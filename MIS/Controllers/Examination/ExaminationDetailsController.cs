﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Extensions;
using MIS_Entity.Entity.Examination;

namespace MIS.Controllers.Examination
{
    public class ExaminationDetailsController : Controller
    {
        private readonly ISMTDbContext _context;

        public ExaminationDetailsController(ISMTDbContext context)
        {
            _context = context;
        }

        // GET: ExaminationDetails
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAllExamDetails()
        {
            var iSMTDbContext = _context.ExaminationDetails.Include(e => e.ApplicationUser).Include(e => e.CourseSubject);
            return View(await iSMTDbContext.ToListAsync());
        }

        // GET: ExaminationDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var examinationDetails = await _context.ExaminationDetails
                .Include(e => e.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (examinationDetails == null)
            {
                return NotFound();
            }

            return View(examinationDetails);
        }

        // GET: ExaminationDetails/Create
        public IActionResult Create()
        {
            var CurrId = Guid.Parse(User.GetUserId());
            var model = new ExaminationDetails()
            {
                ID = Guid.NewGuid(),
                ModifiedBy = CurrId,
                CreatedBy = CurrId,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                ModifiedTimes = 0,
                ExaminationDate = DateTime.Now,
                ExaminationEndDate= DateTime.Now
            };

            ViewBag.SubjectId = _context.CourseSubject.Select(v => new SelectListItem
            {
                Text = v.SubjectName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();

            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            return View(model);
        }

        // POST: ExaminationDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExaminationDetails examinationDetails)
        {
            if (ModelState.IsValid)
            {
                await _context.AddAsync(examinationDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", examinationDetails.CreatedBy);
            return View(examinationDetails);
        }

        // GET: ExaminationDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var examinationDetails = await _context.ExaminationDetails.FindAsync(id);
            if (examinationDetails == null)
            {
                return NotFound();
            }
            ViewBag.SubjectId = _context.CourseSubject.Select(v => new SelectListItem
            {
                Text = v.SubjectName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();

            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", examinationDetails.CreatedBy);
            return View(examinationDetails);
        }

        // POST: ExaminationDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id,ExaminationDetails examinationDetails)
        {
            if (id != examinationDetails.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var CurrId = Guid.Parse(User.GetUserId());
                    examinationDetails.ModifiedBy = CurrId;
                    examinationDetails.ModifiedDate = DateTime.Now;
                    examinationDetails.ModifiedTimes += 1;
                    _context.Update(examinationDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExaminationDetailsExists(examinationDetails.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", examinationDetails.CreatedBy);
            return View(examinationDetails);
        }

        // GET: ExaminationDetails/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var examinationDetails = await _context.ExaminationDetails
                .Include(e => e.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (examinationDetails == null)
            {
                return NotFound();
            }

            return View(examinationDetails);
        }

        // POST: ExaminationDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var examinationDetails = await _context.ExaminationDetails.FindAsync(id);
            _context.ExaminationDetails.Remove(examinationDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExaminationDetailsExists(Guid id)
        {
            return _context.ExaminationDetails.Any(e => e.ID == id);
        }
    }
}
