﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS_Entity.Entity.Examination;

namespace MIS.Controllers.Examination
{
    public class ExaminationGradingController : Controller
    {
        private readonly ISMTDbContext _context;

        public ExaminationGradingController(ISMTDbContext context)
        {
            _context = context;
        }

        // GET: ExaminationGrading
        // GET: ExaminationDetails
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAllExamDetails()
        {
            var iSMTDbContext = _context.ExaminationGrading.Include(e => e.ApplicationUser);
            return View(await iSMTDbContext.ToListAsync());
        }

        // GET: ExaminationGrading/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var examinationGrading = await _context.ExaminationGrading
                .Include(e => e.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (examinationGrading == null)
            {
                return NotFound();
            }

            return View(examinationGrading);
        }

        // GET: ExaminationGrading/Create
        public IActionResult Create()
        {
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: ExaminationGrading/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ExaminationId,StudentId,Remarks,Grade,ID,CreatedBy,ModifiedBy,CreatedDate,ModifiedTimes,ModifiedDate,Deleted,RecordStatus")] ExaminationGrading examinationGrading)
        {
            if (ModelState.IsValid)
            {
                examinationGrading.ID = Guid.NewGuid();
                _context.Add(examinationGrading);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", examinationGrading.CreatedBy);
            return View(examinationGrading);
        }

        // GET: ExaminationGrading/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var examinationGrading = await _context.ExaminationGrading.FindAsync(id);
            if (examinationGrading == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", examinationGrading.CreatedBy);
            return View(examinationGrading);
        }

        // POST: ExaminationGrading/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("ExaminationId,StudentId,Remarks,Grade,ID,CreatedBy,ModifiedBy,CreatedDate,ModifiedTimes,ModifiedDate,Deleted,RecordStatus")] ExaminationGrading examinationGrading)
        {
            if (id != examinationGrading.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(examinationGrading);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExaminationGradingExists(examinationGrading.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", examinationGrading.CreatedBy);
            return View(examinationGrading);
        }

        // GET: ExaminationGrading/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var examinationGrading = await _context.ExaminationGrading
                .Include(e => e.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (examinationGrading == null)
            {
                return NotFound();
            }

            return View(examinationGrading);
        }

        // POST: ExaminationGrading/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var examinationGrading = await _context.ExaminationGrading.FindAsync(id);
            _context.ExaminationGrading.Remove(examinationGrading);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExaminationGradingExists(Guid id)
        {
            return _context.ExaminationGrading.Any(e => e.ID == id);
        }
    }
}
