﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Helper;
using MIS_Entity.Entity.Batches;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Repository.Interfaces;

namespace MIS.Controllers.Batches
{
    [ModuleInfo(ModuleName = "BatchDetails", Url = "/BatchDetails")]
    public class BatchDetailsController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBatchRepository batchRepository;

        public BatchDetailsController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork,
            IBatchRepository batchRepository)
        {
            _context = context;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
            this.batchRepository = batchRepository;
        }

        // GET: BatchDetails
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> GetBatchIndex()
        {
            //var iSMTDbContext = _context.BookDetails.Include(b => b.ApplicationUser);
            //return View(await iSMTDbContext.ToListAsync());

            var batch = batchRepository.GetAll().ToList();
            return View(batch);
        }

        // GET: BatchDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var batchDetails = await _context.BatchDetails
                .Include(b => b.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (batchDetails == null)
            {
                return NotFound();
            }

            return View(batchDetails);
        }

        // GET: BatchDetails/Create
        public IActionResult Create()
        {
            var batch = new BatchDetails();
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            return View(batch);
        }

        // POST: BatchDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BatchName,BatchCode,BatchType,BatchYear")] BatchDetails batchDetails)
        {
            if (ModelState.IsValid)
            {
                batchDetails.ID = Guid.NewGuid();
                batchDetails.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                batchDetails.CreatedDate = DateTime.Now;
                batchDetails.ModifiedTimes = 0;
                batchDetails.RecordStatus = RecordStatus.Active;

                batchRepository.Add(batchDetails);

                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", batchDetails.CreatedBy);
            return View(batchDetails);
        }

        // GET: BatchDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var batchDetails = batchRepository.Get(id);
            if (batchDetails == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", batchDetails.CreatedBy);
            ViewBag.ID = id;
            ViewBag.CreatedBy = batchDetails.CreatedBy;
            ViewBag.CreatedDate = batchDetails.CreatedDate;
            return View(batchDetails);
        }

        // POST: BatchDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("BatchName,BatchCode,BatchType,BatchYear,ID,CreatedBy,CreatedDate")] BatchDetails batchDetails)
        {
            if (id != batchDetails.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    batchDetails.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                    batchDetails.ModifiedDate = DateTime.Now;
                    batchDetails.ModifiedTimes = batchDetails.ModifiedTimes + 1;

                    batchRepository.Update(batchDetails);
                    await unitOfWork.SaveAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BatchDetailsExists(batchDetails.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", batchDetails.CreatedBy);
            return View(batchDetails);
        }

        // GET: BatchDetails/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var batchDetails = await _context.BatchDetails
                .Include(b => b.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (batchDetails == null)
            {
                return NotFound();
            }

            return View(batchDetails);
        }

        // POST: BatchDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var batchDetails = batchRepository.Get(id);
            batchRepository.Remove(batchDetails);
            await unitOfWork.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BatchDetailsExists(Guid id)
        {
            return _context.BatchDetails.Any(e => e.ID == id);
        }
    }
}
