﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS.Services;
using MIS.ViewModel;
using MIS_Entity.DTO;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.FacultyDetails;
using MIS_Entity.Entity.Identity;
using MIS_Entity.Enum;
using MIS_Repository.Interfaces;

namespace MIS.Controllers
{
    [ModuleInfo(ModuleName = "FacultyDetails", Url = "/FacultyDetails")]
    public class FacultyDetailsController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUnitOfWork unitOfWork;
        private readonly IFacultyRepository facultyRepository;
        private readonly IWebHostEnvironment hostEnvironment;
        private readonly IEmailService emailService;
        private readonly IBucketService bucketService;
        private readonly IMapper mapper;
        private readonly FileUploadRequestViewModel _bucketSetting;


        public IFacultyRepository FacultyRepository => facultyRepository;

        public FacultyDetailsController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork,
            IFacultyRepository facultyRepository,
            IWebHostEnvironment _hostEnvironment, IEmailService emailService, IBucketService bucketService,
            IOptions<FileUploadRequestViewModel> bucketSetting, IMapper mapper)
        {
            _context = context;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
            this.facultyRepository = facultyRepository;
            hostEnvironment = _hostEnvironment;
            this.emailService = emailService;
            this.bucketService = bucketService;
            _bucketSetting = bucketSetting.Value;
            this.mapper = mapper;
        }

        // GET: FacultyDetails
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> GetFacultyIndex()
        {
            var faculty = facultyRepository.GetAll().Select(x => new FacultyDetailsViewModel()
            {
                DepartmentID = x.DepartmentID,
                DepartmentName = _context.Department.Where(z => z.ID == x.DepartmentID).Select(z => z.DepartmentName).FirstOrDefault(),
                FacultyImgLocation = x.FacultyImgLocation,
                FacultyName = x.FacultyName,
                FacultyType = x.FacultyType,
                HeadOfDepartment = x.HeadOfDepartment,
                ID = x.ID,
                CreatedDate = x.CreatedDate
            }).ToList();

            return View(faculty);
        }

        // GET: FacultyDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facultyDetails = await _context.FacultyDetails
                .Include(f => f.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (facultyDetails == null)
            {
                return NotFound();
            }

            return View(facultyDetails);
        }

        // GET: FacultyDetails/Create
        public IActionResult Create()
        {
            var faculty = new FacultyDetails();
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            ViewBag.Departments = _context.Department.Select(v => new SelectListItem
            {
                Text = v.DepartmentName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            //string bookTypes[] = 
            ViewBag.FacultyTypes = Enum.GetValues(typeof(FacultyType)).Cast<FacultyType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            return View(faculty);
        }

        // POST: FacultyDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(FacultyDetails facultyDetails)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    facultyDetails.ID = Guid.NewGuid();
                    facultyDetails.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                    facultyDetails.CreatedDate = DateTime.Now;
                    facultyDetails.ModifiedTimes = 0;
                    facultyDetails.RecordStatus = RecordStatus.Active;


                    if (facultyDetails.ImageDetails != null)
                    {
                        var Id = Guid.NewGuid();

                        var fileName = Id + Path.GetExtension(facultyDetails.ImageDetails.FileName);
                        var Files = "";

                        var requestModel = new FileUploadRequestViewModel()
                        {
                            id = Id,
                            AccessKey = _bucketSetting.AccessKey,
                            SecretKey = _bucketSetting.SecretKey,
                            ObjectKey = fileName,
                            BucketName = _bucketSetting.BucketName + "/FacultyImg",
                            FilePath = Files,
                            EndPoint = _bucketSetting.EndPoint,
                            FileStream = facultyDetails.ImageDetails.OpenReadStream(),
                            FileLength = facultyDetails.ImageDetails.Length
                        };

                        // var result = mapper.Map<FileResponseViewModel>(bucketService.UploadBlob(mapper.Map<FileUploadRequestDTO>(requestModel)));
                        var resp = mapper.Map<FileResponseViewModel>(bucketService.UploadFTP(mapper.Map<FileUploadRequestDTO>(requestModel)));
                        if (resp.success)
                        {
                            facultyDetails.FacultyImgLocation = _bucketSetting.URL + "/FacultyImg/" + resp.fileName;
                        }

                    }

                    facultyRepository.Add(facultyDetails);

                    var user = new ApplicationUser() { Id = facultyDetails.ID, UserName = facultyDetails.FacultyEmail, Email = facultyDetails.FacultyEmail, Fullname = facultyDetails.FacultyName, Contact = facultyDetails.FacultyContact, Status = true };
                    var result = await userManager.CreateAsync(user, "Staff@123");
                    if (result.Succeeded)
                    {
                        await userManager.AddToRoleAsync(user, "Faculty");
                        await unitOfWork.SaveAsync();
                        var msg = $"Dear {user.Fullname},<br/> Check your login credential for VLE: <br/><b>username</b>:{user.Email}" +
                              $"<br/><b>password</b>:Staff@123<br/><i>Please change your password after logging in.</i>";
                        try
                        {

                            await this.emailService.Mail_Alert(user.Email, "VLE Registration", msg);

                        }
                        catch
                        {

                        }
                        TempData["Success"] = "Successfully added the record";
                        return RedirectToAction(nameof(Index));

                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError("", error.Description.ToString());
                        }
                    }
                    TempData["Error"] = "The record could not be updated";
                    return RedirectToAction(nameof(Index));
                }
                TempData["Error"] = "The record could not be updated";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                TempData["Error"] = "The record could not be updated";
                return RedirectToAction(nameof(Index));
                // return File(Encoding.UTF8.GetBytes(ex.ToString()), "text/plain", "error.txt");
            }
        }

        // GET: FacultyDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facultyDetails = facultyRepository.Get(id);
            if (facultyDetails == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", facultyDetails.CreatedBy);
            ViewBag.Departments = _context.Department.Select(v => new SelectListItem
            {
                Text = v.DepartmentName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            ViewBag.ID = id;
            ViewBag.CreatedBy = facultyDetails.CreatedBy;
            ViewBag.CreatedDate = facultyDetails.CreatedDate;
            return View(facultyDetails);
        }

        // POST: FacultyDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("FacultyName,DepartmentID,HeadOfDepartment,ID,CreatedBy,CreatedDate")] FacultyDetails facultyDetails)
        {
            if (id != facultyDetails.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    facultyDetails.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                    facultyDetails.ModifiedDate = DateTime.Now;
                    facultyDetails.ModifiedTimes = facultyDetails.ModifiedTimes + 1;

                    facultyRepository.Update(facultyDetails);

                    await unitOfWork.SaveAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FacultyDetailsExists(facultyDetails.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", facultyDetails.CreatedBy);
            return View(facultyDetails);
        }

        // GET: FacultyDetails/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facultyDetails = await _context.FacultyDetails
                .Include(f => f.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (facultyDetails == null)
            {
                return NotFound();
            }

            return View(facultyDetails);
        }

        // POST: FacultyDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var facultyDetails = facultyRepository.Get(id);
            facultyRepository.Remove(facultyDetails);
            await unitOfWork.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FacultyDetailsExists(Guid id)
        {
            return _context.FacultyDetails.Any(e => e.ID == id);
        }
    }
}
