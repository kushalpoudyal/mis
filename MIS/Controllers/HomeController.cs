﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MIS.DBContext;
using MIS.Helper;
using MIS.Models;
using MIS.Utility;
using MIS.ViewModel;
using MIS_Entity.Entity.Identity;
using MIS_Repository.Interfaces;

namespace MIS.Controllers
{
    //[ModuleInfo(ModuleName = "Home", Url = "/Home")]
    //[CRUDAuthorize(ModuleName = "Home", Action = CRUDAction.View)]
    public class HomeController : BaseAdminController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAdminRepository adminRepository;
        private readonly IMapper mapper;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly ISMTDbContext _context;
        public HomeController(ILogger<HomeController> logger, IAdminRepository adminRepository, IMapper mapper)
        {
            _logger = logger;
            this.adminRepository = adminRepository;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            
            var model = mapper.Map<AdminDashboardViewModel>(adminRepository.GetAdminDashboardInfo());
            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
