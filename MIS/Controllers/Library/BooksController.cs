﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GroupDocs.Viewer;
using GroupDocs.Viewer.Options;
using GroupDocs.Viewer.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS.ViewModel;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Repository;
using MIS_Repository.Interfaces;

namespace MIS.Controllers.Library
{
    [Authorize]
    [ModuleInfo(ModuleName = "Books", Url = "/Books")]
    public class BooksController : Controller
    {

        private string projectRootPath;
        private string outputPath;
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUnitOfWork unitOfWork;
        private readonly ILibraryRepository libraryRepository;
        private readonly IWebHostEnvironment hostEnvironment;
        public BooksController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork,
            ILibraryRepository libraryRepository,
            IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
            this.libraryRepository = libraryRepository;
            this.hostEnvironment = hostEnvironment;
            projectRootPath = hostEnvironment.ContentRootPath;
            outputPath = Path.Combine(projectRootPath, "wwwroot/Content");
        }

        // GET: Library
        public async Task<IActionResult> Index()
        {
            //var iSMTDbContext = _context.BookDetails.Include(b => b.ApplicationUser);
            //return View(await iSMTDbContext.ToListAsync());

            //var library = libraryRepository.GetAll().ToList();
            //return View(library);
            return View();
        }
        [HttpGet]
        // GET: Library
        public async Task<IActionResult> DocView(Guid BookId)
        {
            ViewBag.BookId = BookId;
            var entity = await _context.BookDetails.Where(x => x.ID == BookId).FirstOrDefaultAsync();
            var model = new DocumentViewerVM
            {
                BookName = entity.Name,
                IsDownloadable = entity.IsDownloadable,
                LocationKey =   entity.BookLocation,
                ImageLocation = entity.ImageLocation

            };
            await ReadBook(entity).ConfigureAwait(false);
            return View(model);
        }
        [HttpGet]
        public async Task<IActionResult> GetReadBookDetails(Guid BookId)
        {

            var data = await _context.BookActivity.Where(x => x.BookId == BookId).ToListAsync();
            return View(data);
        }
        private async Task ReadBook(BookDetails entity)
        {

            _context.BookActivity.Add(new MIS_Entity.Entity.Library.BookActivity
            {
                BookId = entity.ID,
                BookName = entity.Name,
                Username = User.Identity.Name,
                UserId = Guid.Parse(User.GetUserId())
            });
            await _context.SaveChangesAsync();
        }
        public async Task<IActionResult> GetBookIndex()
        {
            //var iSMTDbContext = _context.BookDetails.Include(b => b.ApplicationUser);
            //return View(await iSMTDbContext.ToListAsync());
            var courseID = Guid.Parse(User.GetActiveUserCourseId());
            var library = await libraryRepository.GetNonPhysicalBooksByCourseId(courseID).ToListAsync();
            return View(library);
        }

        public async Task<IActionResult> SearchBook(string str)
        {
            var dataList = await libraryRepository.GetbyKeyUp(str);
            return View(dataList);
        }

        // GET: Library/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookDetails = await _context.BookDetails
                .Include(b => b.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (bookDetails == null)
            {
                return NotFound();
            }

            return View(bookDetails);
        }



        private bool BookDetailsExists(Guid id)
        {
            return _context.BookDetails.Any(e => e.ID == id);
        }
    }
}
