﻿using System;

namespace MIS.ViewModel
{
    public class FileResponseViewModel
    {
        public Guid id { get; set; }

        public string locationKey { get; set; }

        public string mediaExt { get; set; }
        public long mediaSize { get; set; }

        public string Message { get; set; }

        public string fileName { get; set; }

        public bool success { get; set; }

    }
}