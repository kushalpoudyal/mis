﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MIS.DBContext;
using MIS.Helper;
using MIS.Services;
using MIS.ViewModel;
using MIS_Entity.DTO;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Repository;
using MIS_Repository.Interfaces;

namespace MIS.Controllers.Library
{
    [Authorize]
    [ModuleInfo(ModuleName = "Library", Url = "/Library")]
    public class LibraryController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUnitOfWork unitOfWork;
        private readonly ILibraryRepository libraryRepository;
        private readonly IWebHostEnvironment hostEnvironment;
        private readonly IBucketService bucketService;
        private readonly FileUploadRequestViewModel _bucketSetting;
        private readonly IMapper mapper;

        public LibraryController(
            ISMTDbContext context,
            IOptions<FileUploadRequestViewModel> bucketSetting,
            UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork,
            ILibraryRepository libraryRepository,
            IWebHostEnvironment hostEnvironment, IBucketService bucketService
, IMapper mapper)
        {
            _context = context;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
            this.libraryRepository = libraryRepository;
            this.hostEnvironment = hostEnvironment;
            this.bucketService = bucketService;
            this._bucketSetting = bucketSetting.Value;
            this.mapper = mapper;
        }

        // GET: Library
        public async Task<IActionResult> Index()
        {
            //var iSMTDbContext = _context.BookDetails.Include(b => b.ApplicationUser);
            //return View(await iSMTDbContext.ToListAsync());

            //var library = libraryRepository.GetAll().ToList();
            //return View(library);
            return View();
        }

        public async Task<IActionResult> GetLibraryIndex()
        {
            //var iSMTDbContext = _context.BookDetails.Include(b => b.ApplicationUser);
            //return View(await iSMTDbContext.ToListAsync());

            var library = libraryRepository.GetAll().ToList();
            return View(library);
        }

        // GET: Library/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookDetails = await _context.BookDetails
                .Where(m => m.ID == id).Select(x => new BookDetailsViewModel
                {
                    Id = x.ID,
                    Author = x.Author,
                    Name = x.Name,
                    BookLocationKey = x.BookLocation,
                    ImageLocationKey = x.ImageLocation,
                    Course = _context.CourseDetails.Where(z => z.ID == x.CourseId).Select(x => x.CourseName).FirstOrDefault(),
                    Genre = _context.BookGenre.Where(z => z.ID.ToString() == x.Genre).Select(x => x.Name).FirstOrDefault(),

                }).FirstOrDefaultAsync();
            if (bookDetails == null)
            {
                return NotFound();
            }

            return View(bookDetails);
        }

        // GET: Library/Create
        public IActionResult Create()
        {
            var bookDetails = new BookDetails();
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            //string bookTypes[] = 
            ViewBag.BookTypes = Enum.GetValues(typeof(BookType)).Cast<BookType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewBag.BookGenre = _context.BookGenre.Select(v => new SelectListItem
            {
                Text = v.Name.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            ViewBag.Courses = _context.CourseDetails.Select(v => new SelectListItem
            {
                Text = v.CourseName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            return View(bookDetails);
        }

        // POST: Library/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Create(BookDetails bookDetails)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bookDetails.ID = Guid.NewGuid();
                    bookDetails.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                    bookDetails.CreatedDate = DateTime.Now;
                    bookDetails.ModifiedDate = DateTime.Now;
                    bookDetails.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                    bookDetails.ModifiedTimes = 0;
                    bookDetails.RecordStatus = RecordStatus.Active;

                    //var basePath = Path.Combine(hostEnvironment.WebRootPath, "assets\\images");
                    if (bookDetails.ImageDetails != null)
                    {
                        var Id = Guid.NewGuid();

                        var fileName = Id + Path.GetExtension(bookDetails.ImageDetails.FileName);
                        var Files = Path.GetFullPath(bookDetails.ImageDetails.FileName);

                        var requestModel = new FileUploadRequestViewModel()
                        {
                            id = Id,
                            AccessKey = _bucketSetting.AccessKey,
                            SecretKey = _bucketSetting.SecretKey,
                            ObjectKey = fileName,
                            BucketName = _bucketSetting.BucketName + "/BooksImg",
                            FilePath = Files,
                            EndPoint = _bucketSetting.EndPoint,
                            FileStream = bookDetails.ImageDetails.OpenReadStream(),
                            FileLength = bookDetails.ImageDetails.Length,

                        };

                        // var result = mapper.Map<FileResponseViewModel>(bucketService.UploadBlob(mapper.Map<FileUploadRequestDTO>(requestModel)));
                        var result = mapper.Map<FileResponseViewModel>(bucketService.UploadFTP(mapper.Map<FileUploadRequestDTO>(requestModel)));
                        if (result.success)
                        {
                            bookDetails.ImageLocation = _bucketSetting.URL + "/BooksImg/" + result.fileName;
                        }
                    }

                    if (bookDetails.BookFile != null)
                    {
                        var file = bookDetails.BookFile;

                        var Id = Guid.NewGuid();

                        var fileName = Id + Path.GetExtension(file.FileName);
                        var Files = "";
                        var requestModel = new FileUploadRequestViewModel()
                        {
                            id = Id,
                            AccessKey = _bucketSetting.AccessKey,
                            SecretKey = _bucketSetting.SecretKey,
                            ObjectKey = fileName,
                            BucketName = _bucketSetting.BucketName + "/eBooks",
                            FilePath = Files,
                            EndPoint = _bucketSetting.EndPoint,
                            FileStream = bookDetails.BookFile.OpenReadStream(),
                            FileLength = bookDetails.ImageDetails.Length
                        };

                        //var result = mapper.Map<FileResponseViewModel>(bucketService.UploadBlob(mapper.Map<FileUploadRequestDTO>(requestModel)));
                        var result = mapper.Map<FileResponseViewModel>(bucketService.UploadFTP(mapper.Map<FileUploadRequestDTO>(requestModel)));
                        if (result.success)
                        {
                            bookDetails.BookLocation = _bucketSetting.URL + "/eBooks/" + result.fileName;
                        }
                    }

                    libraryRepository.Add(bookDetails);

                    await unitOfWork.SaveAsync();

                    return RedirectToAction(nameof(Index));
                }
                ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", bookDetails.CreatedBy);
                return View(bookDetails);
            }
            catch (Exception ex)
            {
                return File(Encoding.UTF8.GetBytes(ex.ToString()), "text/plain", "error.txt");
            }
        }
        public async Task<IActionResult> UploadBook(IFormFile bookFile, Guid BookId)
        {
            var response = new FileUploadAjaxModel();

            try
            {
                if (bookFile != null)
                {

                    var file = bookFile;
                    var Id = Guid.NewGuid();

                    var fileName = Id + Path.GetExtension(file.FileName);
                    var Files = "";

                    var requestModel = new FileUploadRequestViewModel()
                    {
                        id = Id,
                        AccessKey = _bucketSetting.AccessKey,
                        SecretKey = _bucketSetting.SecretKey,
                        ObjectKey = fileName,
                        BucketName = _bucketSetting.BucketName + "/eBooks",
                        FilePath = Files,
                        EndPoint = _bucketSetting.EndPoint,
                        FileStream = bookFile.OpenReadStream()
                    };

                    // var result = mapper.Map<FileResponseViewModel>(bucketService.UploadBlob(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    var result = mapper.Map<FileResponseViewModel>(bucketService.UploadFTP(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    if (result.success)
                    {
                        var getBook = await _context.BookDetails.Where(x => x.ID == BookId).FirstOrDefaultAsync();
                        getBook.BookLocation = _bucketSetting.URL + "/eBooks/" + result.fileName;
                        _context.BookDetails.Update(getBook);
                        await _context.SaveChangesAsync();

                        response.Status = true;
                        response.Message = "Successfully Uploaded";
                        response.FileFormat = bookFile.ContentType;
                        response.FilePath = "/eBooks/" + result.fileName;

                        return Json(response);
                    }
                    else
                    {

                        response.Status = false;
                        response.Message = "Error on Uploading";

                        return Json(response);
                    }
                }
                else
                {

                    response.Status = false;
                    response.Message = "Error on Uploading";

                    return Json(response);
                }

            }
            catch
            {
                response.Status = false;
                response.Message = "Error on Uploading";

                return Json(response);
            }


        }
        // GET: Library/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var bookDetails = await _context.BookDetails.FindAsync(id);
            var bookDetails = libraryRepository.Get(id);
            //bookDetails.ID = (Guid)id;
            ViewBag.BookTypes = Enum.GetValues(typeof(BookType)).Cast<BookType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewBag.BookGenre = _context.BookGenre.Select(v => new SelectListItem
            {
                Text = v.Name.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            ViewBag.Courses = _context.CourseDetails.Select(v => new SelectListItem
            {
                Text = v.CourseName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();


            if (bookDetails == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", bookDetails.CreatedBy);
            ViewBag.ID = id;
            ViewBag.CreatedBy = bookDetails.CreatedBy;
            ViewBag.CreatedDate = bookDetails.CreatedDate;
            ViewBag.ImageLocation = bookDetails.ImageLocation;
            return View(bookDetails);
        }

        // POST: Library/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, BookDetails bookDetails)
        {
            if (id != bookDetails.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //_context.Update(bookDetails);
                    //await _context.SaveChangesAsync();
                    if (bookDetails.ImageDetails != null)
                    {
                        //var basePath = Path.Combine(hostEnvironment.WebRootPath, "assets\\images"); 
                        var basePath = Path.Combine(hostEnvironment.WebRootPath, "assets\\images");
                        var basePath1 = "\\assets\\images";
                        var filePath = Path.Combine(basePath, bookDetails.ImageDetails.FileName);
                        var filePath1 = Path.Combine(basePath1, bookDetails.ImageDetails.FileName);
                        bookDetails.ImageLocation = filePath1;
                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await bookDetails.ImageDetails.CopyToAsync(stream);
                        }
                    }



                    bookDetails.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                    bookDetails.ModifiedDate = DateTime.Now;
                    bookDetails.ModifiedTimes = bookDetails.ModifiedTimes + 1;

                    libraryRepository.Update(bookDetails);

                    await unitOfWork.SaveAsync();

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookDetailsExists(bookDetails.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", bookDetails.CreatedBy);
            return View(bookDetails);
        }

        // GET: Library/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookDetails = await _context.BookDetails
                .Include(b => b.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);

            if (bookDetails == null)
            {
                return NotFound();
            }

            return View(bookDetails);
        }

        // POST: Library/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var bookDetails = libraryRepository.Get(id);
            libraryRepository.Remove(bookDetails);

            await unitOfWork.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookDetailsExists(Guid id)
        {
            return _context.BookDetails.Any(e => e.ID == id);
        }
    }
}
