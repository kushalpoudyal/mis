﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Helper;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Identity;
using MIS_Repository.Interfaces;

namespace MIS.Controllers.Library
{
    [ModuleInfo(ModuleName = "BookGenres", Url = "/BookGenres")]
    public class BookGenresController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IBookGenreRepository bookGenreRepository;        
        private readonly IUnitOfWork unitOfWork;

        public BookGenresController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            IBookGenreRepository bookGenreRepository,
            IUnitOfWork unitOfWork)
        {
            _context = context;
            this.userManager = userManager;
            this.bookGenreRepository = bookGenreRepository;
            this.unitOfWork = unitOfWork;
        }

        // GET: BookGenres
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> GetBookGenresIndex()
        {

            var bookGenres = bookGenreRepository.GetAll().ToList();
            return View(bookGenres);
        }

        // GET: BookGenres/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookGenre = await _context.BookGenre
                .FirstOrDefaultAsync(m => m.ID == id);
            if (bookGenre == null)
            {
                return NotFound();
            }

            return View(bookGenre);
        }

        // GET: BookGenres/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BookGenres/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Description")] BookGenre bookGenre)
        {
            if (ModelState.IsValid)
            {
                bookGenreRepository.Add(bookGenre);
                await unitOfWork.SaveAsync();

                return RedirectToAction(nameof(Index));
            }
            return View(bookGenre);
        }

        // GET: BookGenres/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            
            if (id == null)
            {
                return NotFound();
            }

            var bookGenre = bookGenreRepository.Get(id);
            if (bookGenre == null)
            {
                return NotFound();
            }
            ViewBag.ID = id;
                        
            return View(bookGenre);
        }

        // POST: BookGenres/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("ID,Name,Description")] BookGenre bookGenre)
        {
            if (id != bookGenre.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    bookGenreRepository.Update(bookGenre);
                    await unitOfWork.SaveAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookGenreExists(bookGenre.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(bookGenre);
        }

        // GET: BookGenres/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookGenre = await _context.BookGenre
                .FirstOrDefaultAsync(m => m.ID == id);
            if (bookGenre == null)
            {
                return NotFound();
            }

            return View(bookGenre);
        }

        // POST: BookGenres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var bookGenre = bookGenreRepository.Get(id);
            bookGenreRepository.Remove(bookGenre);
            await unitOfWork.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookGenreExists(Guid id)
        {
            return _context.BookGenre.Any(e => e.ID == id);
        }
    }
}
