﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Helper;
using MIS_Entity.Entity.Department;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Repository.Interfaces;

namespace MIS.Controllers.Departments
{
    [Authorize]
    [ModuleInfo(ModuleName = "Departments", Url = "/Departments")]
    public class DepartmentsController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IDepartmentRepository departmentRepository;
        private readonly IUnitOfWork unitOfWork;
        public DepartmentsController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            IDepartmentRepository departmentRepository,
            IUnitOfWork unitOfWork
            )
        {
            _context = context;
            this.userManager = userManager;
            this.departmentRepository = departmentRepository;
            this.unitOfWork = unitOfWork;
        }

        // GET: Departments
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> GetDepartmentIndex()
        {
            var department = departmentRepository.GetAll().ToList();
            return View(department);
        }

        // GET: Departments/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = await _context.Department
                .Include(d => d.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (department == null)
            {
                return NotFound();
            }

            return View(department);
        }

        // GET: Departments/Create
        public IActionResult Create()
        {
            var department = new Department();
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            return View(department);
        }

        // POST: Departments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DepartmentName,DepartmentDesc,DepartmentBlock")] Department department)
        {
            if (ModelState.IsValid)
            {
                department.ID = Guid.NewGuid();
                department.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                department.CreatedDate = DateTime.Now;
                department.ModifiedTimes = 0;
                department.RecordStatus = RecordStatus.Active;

                departmentRepository.Add(department);
                await unitOfWork.SaveAsync();

                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", department.CreatedBy);
            return View(department);
        }

        // GET: Departments/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = departmentRepository.Get(id);
            if (department == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", department.CreatedBy);
            ViewBag.ID = id;
            ViewBag.CreatedBy = department.CreatedBy;
            ViewBag.CreatedDate = department.CreatedDate;
            return View(department);
        }

        // POST: Departments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("DepartmentName,DepartmentDesc,DepartmentBlock,ID,CreatedBy,CreatedDate")] Department department)
        {
            if (id != department.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    department.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                    department.ModifiedDate = DateTime.Now;
                    department.ModifiedTimes = department.ModifiedTimes + 1;

                    departmentRepository.Update(department);
                    await unitOfWork.SaveAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepartmentExists(department.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", department.CreatedBy);
            return View(department);
        }

        // GET: Departments/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = await _context.Department
                .Include(d => d.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (department == null)
            {
                return NotFound();
            }

            return View(department);
        }

        // POST: Departments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var department = departmentRepository.Get(id);
            departmentRepository.Remove(department);
            await unitOfWork.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DepartmentExists(Guid id)
        {
            return _context.Department.Any(e => e.ID == id);
        }
    }
}
