﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Examination;

namespace MIS.Controllers.Students
{
    [Authorize(Roles = "Admin")]
    [ModuleInfo(ModuleName = "Recommendations", Url = "/Recommendations")]
    public class RecommendationsController : Controller
    {
        private readonly ISMTDbContext _context;

        public RecommendationsController(ISMTDbContext context)
        {
            _context = context;
        }

        // GET: NoticeDetails
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> GetAllRecommendation()
        {
            var iSMTDbContext = _context.Recommendations.Include(n => n.ApplicationUser);
            return View(await iSMTDbContext.ToListAsync());
        }

        // GET: Recommendations/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recommendations = await _context.Recommendations
                .Include(r => r.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (recommendations == null)
            {
                return NotFound();
            }

            return View(recommendations);
        }

        // GET: Recommendations/Create
        public IActionResult Create(Guid? StudentId)
        {
            var CurrId = Guid.Parse(User.GetUserId());
            var model = new Recommendations()
            {
                ID = Guid.NewGuid(),
                ModifiedBy = CurrId,
                CreatedBy = CurrId,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                ModifiedTimes = 0
            };
            if (StudentId != null)
            {
                ViewBag.StudentId = _context.StudentDetails.Where(x => x.ID == StudentId).Select(v => new SelectListItem
                {
                    Text = v.StudentName.ToString(),
                    Value = ((Guid)v.ID).ToString()
                }).ToList();
            }
            else
            {
                ViewBag.StudentId = _context.StudentDetails.Select(v => new SelectListItem
                {
                    Text = v.StudentName.ToString(),
                    Value = ((Guid)v.ID).ToString()
                }).ToList();
            }

            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            return View(model);
        }

        // POST: Recommendations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Recommendations recommendations)
        {
            if (ModelState.IsValid)
            {
                recommendations.ID = Guid.NewGuid();
                _context.Add(recommendations);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", recommendations.CreatedBy);
            return View(recommendations);
        }

        // GET: Recommendations/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recommendations = await _context.Recommendations.FindAsync(id);
            if (recommendations == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", recommendations.CreatedBy);
            return View(recommendations);
        }

        // POST: Recommendations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Title,Description,StudentId,ID,CreatedBy,ModifiedBy,CreatedDate,ModifiedTimes,ModifiedDate,Deleted,RecordStatus")] Recommendations recommendations)
        {
            if (id != recommendations.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(recommendations);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RecommendationsExists(recommendations.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", recommendations.CreatedBy);
            return View(recommendations);
        }

        // GET: Recommendations/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recommendations = await _context.Recommendations
                .Include(r => r.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (recommendations == null)
            {
                return NotFound();
            }

            return View(recommendations);
        }

        // POST: Recommendations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var recommendations = await _context.Recommendations.FindAsync(id);
            _context.Recommendations.Remove(recommendations);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RecommendationsExists(Guid id)
        {
            return _context.Recommendations.Any(e => e.ID == id);
        }
    }
}
