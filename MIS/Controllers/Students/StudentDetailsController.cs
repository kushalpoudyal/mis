﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.Web.CodeGeneration;
using MIS.DBContext;
using MIS.Helper;
using MIS.Models;
using MIS.Services;
using MIS.ViewModel;
using MIS_Data.Migrations;
using MIS_Entity.DTO;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Entity.Entity.Student;
using MIS_Repository.Interfaces;
using Org.BouncyCastle.Math.EC.Rfc7748;
using QRCoder;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.Controllers
{
    [ModuleInfo(ModuleName = "StudentDetails", Url = "/StudentDetails")]
    public class StudentDetailsController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly IStudentRepository _studentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IMapper mapper;
        private readonly IEmailService emailService;
        private readonly IBucketService bucketService;
        private readonly FileUploadRequestViewModel _bucketSetting;

        public StudentDetailsController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork,
            IWebHostEnvironment webHostEnvironment,
            IStudentRepository studentRepository,
            IMapper mapper,
            IEmailService emailService, IBucketService bucketService,
            IOptions<FileUploadRequestViewModel> bucketSetting)
        {
            _context = context;
            this.userManager = userManager;
            this._unitOfWork = unitOfWork;
            _webHostEnvironment = webHostEnvironment;
            this._studentRepository = studentRepository;
            this.mapper = mapper;
            this.emailService = emailService;
            this.bucketService = bucketService;
            _bucketSetting = bucketSetting.Value;
        }

        // GET: StudentDetails/Create
        public IActionResult Create()
        {
            var student = new StudentDetails();
            //ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            InitCommon(student);
            student.DOB = DateTime.Now.AddYears(-16);
            student.Id_Issued_Date = DateTime.Now;
            return View(student);
        }

        // POST: StudentDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(StudentDetails studentDetails)
        {
            var response = new ResponseViewModel();
            if (ModelState.IsValid)
            {
                studentDetails.ID = Guid.NewGuid();
                studentDetails.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                studentDetails.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                studentDetails.CreatedDate = DateTime.Now;
                studentDetails.ModifiedDate = DateTime.Now;
                studentDetails.ModifiedTimes = 0;
                studentDetails.RecordStatus = RecordStatus.Active;
                if (studentDetails.ProfilePic != null && studentDetails.ProfilePic.Length > 0)
                {
                    var Id = Guid.NewGuid();

                    var fileName = Id + Path.GetExtension(studentDetails.ProfilePic.FileName);
                    var Files = "";

                    var requestModel = new FileUploadRequestViewModel()
                    {
                        id = Id,
                        AccessKey = _bucketSetting.AccessKey,
                        SecretKey = _bucketSetting.SecretKey,
                        ObjectKey = fileName,
                        BucketName = _bucketSetting.BucketName + "/FacultyImg",
                        FilePath = Files,
                        EndPoint = _bucketSetting.EndPoint,
                        FileStream = studentDetails.ProfilePic.OpenReadStream(),
                        FileLength = studentDetails.ProfilePic.Length
                    };

                    // var result = mapper.Map<FileResponseViewModel>(bucketService.UploadBlob(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    var resp = mapper.Map<FileResponseViewModel>(bucketService.UploadFTP(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    if (resp.success)
                    {
                        studentDetails.ProfileImgLocation = _bucketSetting.URL + "/FacultyImg/" + resp.fileName;
                    }

                }

                var user = new ApplicationUser()
                {
                    Id = studentDetails.ID,
                    UserName = studentDetails.StudentEmail,
                    Email = studentDetails.StudentEmail,
                    Fullname = studentDetails.StudentName,
                    Contact = studentDetails.MobileNumber,
                    Country = studentDetails.PermanentCountry,
                    Status = true
                };
                user.StudentDetails.Add(studentDetails);
                var result = await userManager.CreateAsync(user, "Student@123");

                if (result.Succeeded)
                {

                    //_studentRepository.Add(studentDetails);
                    //await _unitOfWork.SaveAsync();
                    await changeCourseDetails(new StudentCourseTimeline
                    {
                        StudentId = studentDetails.ID,
                        GivenById = studentDetails.CreatedBy,
                        FromDate = DateTime.UtcNow,
                        GivenOnDate = DateTime.UtcNow,
                        CourseId = studentDetails.CourseId,
                        WasEscalated = false
                    }, false);
                    await userManager.AddToRoleAsync(user, "Student");
                    var msg = $"Dear {user.Fullname},<br/> Check your login credential for VLE: <br/><b>username</b>:{user.Email}" +
                         $"<br/><b>password</b>:Student@123<br/><i>Please change your password after logging in.</i>";
                    try
                    {

                        await this.emailService.Mail_Alert(user.Email, "VLE Registration", msg);

                    }
                    catch
                    {

                    }
                    response.Success = true;
                    response.Message = "Successfully Added Student Record";
                    //return Ok(response);
                    TempData["Success"] = response.Message;
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    response.Success = false;
                    foreach (var error in result.Errors)
                    {
                        response.Message += error.Description.ToString();
                    }
                    // return Ok(response);
                    TempData["Error"] = response.Message;
                    return RedirectToAction(nameof(Index));
                }




            }
            response.Success = false;
            response.Message = "Please input all the fields correctly.";
            //return Ok(response);
            TempData["Error"] = response.Message;
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> CreateObservation(StudentObservation studentDetails, bool HasSuspended)
        {
            var response = new ResponseViewModel();
            try
            {

                studentDetails.GivenById = Guid.Parse(userManager.GetUserId(User));
                studentDetails.GivenOn = DateTime.Now;
                if (HasSuspended)
                {
                    await Suspend(new StudentSuspension
                    {
                        Remarks = studentDetails.Remarks,
                        StudentId = studentDetails.StudentId,
                        GivenById = studentDetails.GivenById,
                        GivenOnDate = studentDetails.GivenOn
                    });
                }
                _context.Add(studentDetails);
                await _context.SaveChangesAsync();

                response.Success = true;
                response.Message = "Observation recorded";
                return Ok(response);

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();
                return Ok(response);

            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteObservation(int id)
        {
            var response = new ResponseViewModel();
            try
            {

                var _data = _context.StudentObservation.Where(x => x.Id == id).FirstOrDefault();
                if (_data.GivenById.ToString() != userManager.GetUserId(User))
                {
                    response.Success = false;
                    response.Message = "Unauthorized access";
                    return Ok(response);
                }
                _context.StudentObservation.Remove(_data);
                await _context.SaveChangesAsync();

                response.Success = true;
                response.Message = "Observation Deleted";
                return Ok(response);

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();
                return Ok(response);

            }
        }

        public async Task<IActionResult> GetStudentObservation(Guid StudentId)
        {
            var data = await _context.StudentObservation.Where(x => x.StudentId == StudentId).Select(x => new StudentObservationViewModel
            {
                StudentId = StudentId,
                Criticality = 0,
                GivenById = x.GivenById,
                GivenByName = _context.Users.Where(z => z.Id == x.GivenById).Select(z => z.Fullname).FirstOrDefault(),
                GivenOn = x.GivenOn,
                Remarks = x.Remarks
            }).ToListAsync();
            return View(data);
        }
        public async Task<IActionResult> GetStudentFinancialHistory(string StudentId)
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateSuspension(StudentSuspension studentDetails)
        {

            var response = await Suspend(studentDetails);
            return Ok(response);
        }

        private async Task<ResponseViewModel> Suspend(StudentSuspension studentDetails)
        {
            var response = new ResponseViewModel();
            try
            {
                studentDetails.GivenById = Guid.Parse(userManager.GetUserId(User));
                studentDetails.GivenOnDate = DateTime.Now;

                _context.Add(studentDetails);

                var studentEmail = await _context.Users.Where(x => x.Id == studentDetails.StudentId).FirstOrDefaultAsync();
                studentEmail.RecordStatus = RecordStatus.Suspended;
                studentEmail.ModifiedDate = DateTime.UtcNow;

                _context.Users.Update(studentEmail);

                await _context.SaveChangesAsync();

                var msg = $"Dear Concerned,<br/> " +
                    $"Your VLE account has been suspended, please contact the Administrator for more details. ";
                await this.emailService.Mail_Alert(studentEmail.Email, "VLE Account Details", msg);

                response.Success = true;
                response.Message = "Observation recorded";
                return response;

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();
                return response;

            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RevokeSuspension(int id)
        {
            var response = new ResponseViewModel();
            try
            {

                var _data = _context.StudentSuspension.Where(x => x.Id == id).FirstOrDefault();
                if (_data.GivenById.ToString() != userManager.GetUserId(User))
                {
                    response.Success = false;
                    response.Message = "Unauthorized access";
                    return Ok(response);
                }



                _context.StudentSuspension.Remove(_data);
                await _context.SaveChangesAsync();

                response.Success = true;
                response.Message = "Observation Deleted";
                return Ok(response);

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();
                return Ok(response);

            }
        }
        public async Task<IActionResult> GetStudentSuspension(Guid StudentId)
        {
            var data = await _context.StudentSuspension.Where(x => x.StudentId == StudentId).Select(x => new StudentObservationViewModel
            {
                StudentId = StudentId,
                Criticality = 0,
                GivenById = x.GivenById,
                GivenByName = _context.StudentDetails.Where(z => z.ID == StudentId).Select(z => z.StudentName).FirstOrDefault(),
                GivenOn = x.GivenOnDate,
                Remarks = x.Remarks
            }).ToListAsync();
            return View(data);
        }

        public async Task<IActionResult> GetCourseTimeline(Guid StudentId)
        {
            var data = await _context.StudentCourseTimeline.Where(x => x.StudentId == StudentId).Select(x => new StudentCourseTimelineVM
            {
                StudentId = x.StudentId,
                CourseCode = _context.CourseDetails.Where(z => z.ID == x.CourseId).Select(z => z.CourseCode).FirstOrDefault(),
                CourseName = _context.CourseDetails.Where(z => z.ID == x.CourseId).Select(z => z.CourseName).FirstOrDefault(),
                CourseId = x.CourseId,
                FromDate = x.FromDate,
                GivenById = x.GivenById,
                GivenOnDate = x.GivenOnDate,
                Id = x.Id,
                WasEscalated = x.WasEscalated
            }).ToListAsync();
            return View(data);
        }
        [HttpPost]
        public async Task<IActionResult> ChangeCourseStudent(StudentCourseTimeline course)
        {
            var response = await changeCourseDetails(course, true);
            return Ok(response);
        }
        private async Task<ResponseViewModel> changeCourseDetails(StudentCourseTimeline course, bool WasEscalated)
        {
            var response = new ResponseViewModel();
            try
            {
                course.GivenOnDate = DateTime.UtcNow;
                course.GivenById = Guid.Parse(userManager.GetUserId(User));
                course.WasEscalated = WasEscalated;

                _context.StudentCourseTimeline.Add(course);

                var student = _studentRepository.Get(course.StudentId);
                student.CourseId = course.CourseId;
                student.ModifiedDate = DateTime.UtcNow;

                _studentRepository.Update(student);


                await _context.SaveChangesAsync();

                response.Success = true;
                response.Message = "Course Escalated";
                return response;

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();
                return response;

            }
        }

        // GET: StudentDetails/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentDetails = await _context.StudentDetails
                .Include(f => f.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (studentDetails == null)
            {
                return NotFound();
            }

            return View(studentDetails);
        }

        // POST: StudentDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var studentDetails = _studentRepository.Get(id);
            _studentRepository.Remove(studentDetails);
            await _unitOfWork.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: StudentDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            var studentURL = HttpContext.Request.GetDisplayUrl();

            if (id == null)
            {
                return NotFound();
            }

            var studentDetails = await _context.StudentDetails
                .Include(f => f.ApplicationUser).Include(x => x.CourseDetails).Include(x => x.BatchDetails)
                .FirstOrDefaultAsync(m => m.ID == id);

            var studentProfile = mapper.Map<StudentProfileViewModel>(studentDetails);
            studentProfile.CourseName = studentDetails.CourseDetails.CourseName;
            studentProfile.BatchName = studentDetails.BatchDetails.BatchName;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrData = qrGenerator.CreateQrCode(studentURL, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrData);
            Bitmap qrCodeImage = qrCode.GetGraphic(15);


            var QrByte = BitmapToBytes(qrCodeImage);

            studentProfile.QrImage = QrByte;// Convert.ToBase64String(QrByte);
            if (studentDetails == null)
            {
                return NotFound();
            }

            return View(studentProfile);
        }
        private static Byte[] BitmapToBytes(Bitmap qrCodeImage)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                qrCodeImage.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        // GET: StudentDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentDetails = _studentRepository.Get(id);
            if (studentDetails == null)
            {
                return NotFound();
            }

            InitCommon(studentDetails);
            ViewBag.ID = id;
            ViewBag.CreatedBy = studentDetails.CreatedBy;
            ViewBag.CreatedDate = studentDetails.CreatedDate;
            return View(studentDetails);
        }

        // POST: StudentDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, StudentDetails studentDetails)
        {
            if (id != studentDetails.ID)
            {
                return NotFound();
            }


            try
            {
                var olddata = await _context.StudentDetails
                               .Include(f => f.ApplicationUser)
                               .FirstOrDefaultAsync(m => m.ID == id);
                //olddata.UserId = studentDetails.UserId;
                olddata.BatchId = studentDetails.BatchId;
                olddata.CourseId = studentDetails.CourseId;
                olddata.StudentName = studentDetails.StudentName;
                olddata.DOB = studentDetails.DOB;
                olddata.BloodGroup = studentDetails.BloodGroup;
                olddata.Religion = studentDetails.Religion;
                olddata.MobileNumber = studentDetails.MobileNumber;
                olddata.AlternateNumber = studentDetails.AlternateNumber;
                olddata.Fathername = studentDetails.Fathername;
                olddata.Mothername = studentDetails.Mothername;
                olddata.StudentEmail = studentDetails.StudentEmail;
                olddata.Nationality = studentDetails.Nationality;
                olddata.PermanentCountry = studentDetails.PermanentCountry;
                olddata.PermanentState = studentDetails.PermanentState;
                olddata.PermanentCity = studentDetails.PermanentCity;
                olddata.PermanentLocality = studentDetails.PermanentLocality;
                olddata.TemporaryCountry = studentDetails.TemporaryCountry;
                olddata.TemporaryState = studentDetails.TemporaryState;
                olddata.TemporaryCity = studentDetails.TemporaryCity;
                olddata.TemporaryLocality = studentDetails.TemporaryLocality;
                olddata.Identification = studentDetails.Identification;
                olddata.Id_Number = studentDetails.Id_Number;
                olddata.Id_Issued_Date = studentDetails.Id_Issued_Date;
                olddata.Id_Issued_Place = studentDetails.Id_Issued_Place;
                olddata.Id_Issued_Country = studentDetails.Id_Issued_Country;
                if (studentDetails.ProfilePic != null && studentDetails.ProfilePic.Length > 0)
                {
                    var Id = Guid.NewGuid();

                    var fileName = Id + Path.GetExtension(studentDetails.ProfilePic.FileName);
                    var Files = "";

                    var requestModel = new FileUploadRequestViewModel()
                    {
                        id = Id,
                        AccessKey = _bucketSetting.AccessKey,
                        SecretKey = _bucketSetting.SecretKey,
                        ObjectKey = fileName,
                        BucketName = _bucketSetting.BucketName,
                        FilePath = Files,
                        EndPoint = _bucketSetting.EndPoint,
                        FileStream = studentDetails.ProfilePic.OpenReadStream()
                    };

                    var resp = mapper.Map<FileResponseViewModel>(bucketService.UploadBlob(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    if (resp.success)
                    {
                        olddata.ProfileImgLocation = resp.locationKey;
                    }
                }
                olddata.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                olddata.ModifiedDate = DateTime.Now;
                olddata.ModifiedTimes++;

                _studentRepository.Update(olddata);

                await _unitOfWork.SaveAsync();
                TempData["Success"] = "Successfully updated student record";
                return RedirectToAction(nameof(Index));

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentDetailsExists(studentDetails.ID))
                {
                    return NotFound();
                }
                else
                {
                    //throw;
                }
            }
            TempData["Error"] = "Could not update the entry";
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> GetStudentIndex()
        {
            var student = _studentRepository.GetAll().Include(x => x.BatchDetails).Include(x => x.CourseDetails).ToList();
            return View(student);
        }

        // GET: StudentDetails
        public async Task<IActionResult> Index()
        {
            return View();
        }

        private void InitCommon(StudentDetails student)
        {
            ViewBag.BatchId = _context.BatchDetails.Select(v => new SelectListItem
            {
                Text = v.BatchName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            ViewBag.CourseId = _context.CourseDetails.Select(v => new SelectListItem
            {
                Text = v.CourseName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            //ViewBag.BatchId = new SelectList(_context.BatchDetails
            ////    .Select(v => new
            ////{
            ////    Text = v.BatchName.ToString(),
            ////    Value = ((Guid)v.ID).ToString(),
            ////})
            //    .ToList(), "ID", "BatchName", student?.BatchId);
            //ViewBag.CourseId = new SelectList(_context.CourseDetails.Select(v => new
            //{
            //    Text = v.CourseName.ToString(),
            //    Value = ((Guid)v.ID).ToString()
            //}).ToList(), "Value", "Text", student?.CourseId);
        }

        private bool StudentDetailsExists(Guid id)
        {
            return _context.StudentDetails.Any(e => e.ID == id);
        }
    }
}