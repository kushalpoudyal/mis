﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS.ViewModel;
using MIS_Entity.Entity.Identity;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.Controllers
{
    [ModuleInfo(ModuleName = "Role", Url = "/Role")]
    public class RoleController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ILogger<AccountController> _logger;
        private readonly RoleManager<ApplicationRole> roleManager;

        public RoleController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger, RoleManager<ApplicationRole> roleManager)
        {
            _context = context;
            this.userManager = userManager;
            this.signInManager = signInManager;
            _logger = logger;
            this.roleManager = roleManager;
        }
        public IActionResult Index()
        {
            return View(roleManager.Roles.ToList());
        }

        public IActionResult CreateRole()
        {
            var role = new RoleViewModel();


            var controllerList = AttributeInfo.getControllerList().OrderBy(x => x.ModuleName);
            foreach (var item in controllerList)
            {
                var model = new RoleDetails()
                {
                    ModuleName = item.ModuleName
                };
                role.roleDetailsList.Add(model);
            }
            return View(role);
        }
        public IActionResult EditRole(Guid id)
        {
            var role = new RoleViewModel();
            var roleList = _context.RoleDetails.Where(x => x.RoleId == id).ToList();
            var roleName = _context.Roles.Where(x => x.Id == id).Select(x => x.Name).FirstOrDefault();

            role.roleDetailsList.AddRange(roleList);
            role.Name = roleName;
            role.Id = id;
            var moduleNameList = role.roleDetailsList.Select(x => x.ModuleName);
            var __controllerList = AttributeInfo.getControllerList().Where(x => !moduleNameList.Contains(x.ModuleName)).OrderBy(x => x.ModuleName);
            foreach (var item in __controllerList)
            {
                var model = new RoleDetails()
                {
                    ModuleName = item.ModuleName
                };
                role.roleDetailsList.Add(model);
            }
            return View(role);
        }
        [HttpPost]
        public async Task<ActionResult> CreateRole(RoleViewModel roleViewModel)
        {
            if (ModelState.IsValid)
            {
                var role = new ApplicationRole(roleViewModel.Name);
                var roleresult = await roleManager.CreateAsync(role);
                foreach (var item in roleViewModel.roleDetailsList)
                {
                    var model = new RoleDetails()
                    {
                        ID = Guid.NewGuid(),
                        ModuleName = item.ModuleName,
                        IsView = item.IsView,
                        IsCreate = item.IsCreate,
                        IsDelete = item.IsDelete,
                        IsEdit = item.IsEdit,
                        IsAuthorize = item.IsAuthorize,
                        IsDownload = item.IsDownload,
                        RoleId = role.Id,
                        CreatedBy = Guid.Parse(User.GetUserId()),
                        ModifiedBy = Guid.Parse(User.GetUserId()),
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now
                    };

                    this._context.RoleDetails.Add(model);
                }

                await _context.SaveChangesAsync();
                if (!roleresult.Succeeded)
                {

                    foreach (var item in roleresult.Errors)
                    {
                        ModelState.AddModelError("", item.Description);
                    }
                    return View(roleViewModel);
                }
                return RedirectToAction("Index");
            }
            return View(roleViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> EditRole(RoleViewModel roleViewModel)
        {
            if (ModelState.IsValid)
            {
                var role = await roleManager.FindByIdAsync(roleViewModel.Id.ToString());
                role.Name = roleViewModel.Name; 

                var roleresult = await roleManager.UpdateAsync(role).ConfigureAwait(false);
                var roleList = _context.RoleDetails.Where(x => x.RoleId == roleViewModel.Id).ToList();
                
                _context.RoleDetails.RemoveRange(roleList);
               

                foreach (var item in roleViewModel.roleDetailsList)
                {
                    var model = new RoleDetails()
                    {
                        ID = Guid.NewGuid(),
                        ModuleName = item.ModuleName,
                        IsView = item.IsView,
                        IsCreate = item.IsCreate,
                        IsDelete = item.IsDelete,
                        IsEdit = item.IsEdit,
                        IsAuthorize = item.IsAuthorize,
                        IsDownload = item.IsDownload,
                        RoleId = role.Id,
                        CreatedBy = Guid.Parse(User.GetUserId()),
                        ModifiedBy = Guid.Parse(User.GetUserId()),
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now
                    };

                    this._context.RoleDetails.Add(model);
                }

                await _context.SaveChangesAsync();
                if (!roleresult.Succeeded)
                {

                    foreach (var item in roleresult.Errors)
                    {
                        ModelState.AddModelError("", item.Description);
                    }
                    return View(roleViewModel);
                }
                return RedirectToAction("Index");
            }
            return View(roleViewModel);
        }

    }

}
