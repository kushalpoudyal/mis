﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Helper;
using MIS_Entity.Entity.Classes;
using MIS_Entity.Entity.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.Controllers.RoleBased
{

    [Authorize]
    [ModuleInfo(ModuleName = "Common", Url = "/Common")]
    public class CommonController : Controller
    {
        private readonly DateTime LocalTime;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ISMTDbContext _context;
        public CommonController(UserManager<ApplicationUser> userManager, ISMTDbContext context)
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Nepal Standard Time");
            this.LocalTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone); ;
            _context = context;
            this.userManager = userManager;
        }
        public IActionResult Attendance()
        {

            return View();
        }
        public IActionResult AttendancePage()
        {
            TempData["ShowCheckInTime"] = 0;
            TempData["ShowCheckOutTime"] = 0;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CheckIn(string Remarks, Guid? ClassId)
        {
            try
            {
                var CurrID = Guid.Parse(userManager.GetUserId(User));

                var EmployeeCheckInCheckOut = new UserAttendanceDetails();
                EmployeeCheckInCheckOut.Id = Guid.NewGuid();
                EmployeeCheckInCheckOut.CreatedById = CurrID;
                EmployeeCheckInCheckOut.UserID = CurrID;
                EmployeeCheckInCheckOut.Remarks = Remarks;
                EmployeeCheckInCheckOut.CheckIn = LocalTime;
                EmployeeCheckInCheckOut.CheckOut = null;
                EmployeeCheckInCheckOut.CreatedOn = LocalTime;
                EmployeeCheckInCheckOut.ClassId = ClassId;
                var anyCheckedInToday = await _context.UserAttendanceDetails.AnyAsync(x => x.UserID == CurrID && x.CreatedOn.Date == LocalTime.Date && x.ClassId == ClassId && x.CheckIn != null);

                if (!anyCheckedInToday)
                {

                    _context.Add(EmployeeCheckInCheckOut);
                    await _context.CommitAsync();
                    TempData["ShowCheckInTime"] = 1;
                    this.Notify("Checked In Successfully", "Notice");
                    return Ok();
                }
                else
                {
                    this.Notify("Already checked in.", "Error", "error");
                    return Ok();
                }


            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }



        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CheckOut(string Remarks)
        {
            try
            {
                var CurrID = Guid.Parse(userManager.GetUserId(User));
                var EmployeeCheckInCheckOut = await _context.UserAttendanceDetails.Where(x => x.UserID == CurrID && x.CreatedOn.Date == LocalTime.Date).FirstOrDefaultAsync();
                if (ModelState.IsValid && EmployeeCheckInCheckOut != null)
                {
                    EmployeeCheckInCheckOut.CreatedById = CurrID;
                    EmployeeCheckInCheckOut.UserID = CurrID;
                    EmployeeCheckInCheckOut.Remarks = Remarks;
                    EmployeeCheckInCheckOut.CheckOut = LocalTime;
                    EmployeeCheckInCheckOut.CreatedOn = LocalTime;
                    var anyCheckedOutToday = EmployeeCheckInCheckOut.CheckOut != null;

                    if (!anyCheckedOutToday)
                    {

                        _context.Update(EmployeeCheckInCheckOut);
                        await _context.CommitAsync();
                        TempData["ShowCheckInTime"] = 1;
                        this.Notify("Checked Out Successfully", "Notice");
                        return RedirectToAction(nameof(Index), "Home");
                    }
                    else
                    {
                        this.Notify("Already checked out.", "Error", "error");
                        return RedirectToAction(nameof(Index), "Home");
                    }
                }

            }
            catch (Exception ex)
            {
                this.Notify("Error while checking out ", ExceptionHelper.GetMsg(ex), "error");

            }

            return RedirectToAction(nameof(Index), "Home");

        }
    }
}
