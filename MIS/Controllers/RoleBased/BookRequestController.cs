﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS.ViewModel;
using MIS_Entity.Entity.Identity;
using MIS_Entity.Entity.Library;
using MIS_Entity.Enum;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.Controllers.RoleBased
{
    [ModuleInfo(ModuleName = "BookRequest", Url = "/BookRequest")]
    public class BookRequestController : Controller
    {

        private readonly ISMTDbContext context;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUnitOfWork unitOfWork;
        public BookRequestController(ISMTDbContext context, UserManager<ApplicationUser> userManager, IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.context = context;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAllBookRequest()
        {
            if (User.IsStudent())
            {
                return BadRequest();
            }
            var data = context.BookRequest.Include(x => x.StudentDetails).Include(x => x.BookDetails);

            var model = await data.Select(x => new BookRequestViewModel
            {
                BookName = x.BookDetails.Name,
                AdditionalRemarks = x.AdditionalRemarks,
                BookId = x.BookId,
                Fullname = x.StudentDetails.StudentName,
                RequestedFor = x.RequestedFor,
                RequestedOn = x.RequestedOn,
                RequestedTil = x.RequestedTil,
                RequestStatus = x.RequestStatus,
                UserId = x.UserId
            }).ToListAsync();

            return View("GetBookRequestByStudent", model);
        }

        public IActionResult RequestAction(Guid BookId, Guid UserId)
        {
            var request = new BookRequestActionVM();
            request.BookId = BookId;
            request.UserId = UserId;
            request.RequestStatus = context.BookRequest.Where(x => x.BookId == BookId && x.UserId == UserId).Select(x => x.RequestStatus).FirstOrDefault();
            ViewBag.RequestStatus = Enum.GetValues(typeof(RequestStatus)).Cast<RequestStatus>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();

            request.AvailableStock = context.BookDetails.Where(x => x.ID == BookId).Select(x => x.BookCount).FirstOrDefault() - context.BookRequest.Where(x => x.BookId == x.BookId && x.RequestStatus == RequestStatus.Approved).Count();

            return View(request);
        }
        [HttpPost]
        public async Task<IActionResult> RequestAction(BookRequestActionVM model)
        {
            var data = context.BookRequest.Where(x => x.BookId == model.BookId && x.UserId == model.UserId).FirstOrDefault();
            data.RequestStatus = model.RequestStatus;
            data.ActionTakenOn = DateTime.UtcNow;
            data.ActionTakenBy = Guid.Parse(User.GetUserId());
            data.ActionRemarks = model.ActionRemarks;

            context.BookRequest.Update(data);
            await context.SaveChangesAsync();

            return RedirectToAction("GetAllBookRequest");
        }
        public async Task<IActionResult> GetBookRequestByStudent()
        {
            if (!User.IsStudent())
            {
                return BadRequest();
            }

            var CurrId = Guid.Parse(User.GetUserId());

            var data = context.BookRequest.Include(x => x.StudentDetails).Include(x => x.BookDetails).Where(x => x.UserId == CurrId);

            var model = await data.Select(x => new BookRequestViewModel
            {
                BookName = x.BookDetails.Name,
                AdditionalRemarks = x.AdditionalRemarks,
                BookId = x.BookId,
                Fullname = x.StudentDetails.StudentName,
                RequestedFor = x.RequestedFor,
                RequestedOn = x.RequestedOn,
                RequestedTil = x.RequestedTil,
                RequestStatus = x.RequestStatus,
                UserId = x.UserId
            }).ToListAsync();

            return View(model);
        }
        public IActionResult Create()
        {
            var model = new BookRequest()
            {
                UserId = Guid.Parse(User.GetUserId()),
                RequestedOn = DateTime.Now,
                RequestedFor = DateTime.Now,
                RequestedTil = DateTime.Now,
                RequestStatus = RequestStatus.Pending
            };
            ViewBag.AllBooks = context.BookDetails.Select(v => new SelectListItem
            {
                Text = v.Name,
                Value = ((Guid)v.ID).ToString()
            }).ToList();

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Create(BookRequest model)
        {

            context.BookRequest.Add(model);
            await context.SaveChangesAsync();
            return RedirectToAction("GetBookRequestByStudent");

        }
        public async Task<IActionResult> Edit(Guid BookId)
        {
            var model = await context.BookRequest.Where(x => x.BookId == BookId && x.UserId.ToString() == User.GetUserId()).FirstOrDefaultAsync();
            if (model == null)
            {
                return NotFound();
            }
            ViewBag.AllBooks = context.BookDetails.Select(v => new SelectListItem
            {
                Text = v.Name,
                Value = ((Guid)v.ID).ToString()
            }).ToList();

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(BookRequest model)
        {

            context.BookRequest.Update(model);
            await context.SaveChangesAsync();
            return RedirectToAction("GetBookRequestByStudent");

        }
        [HttpPost]
        public async Task<IActionResult> Delete(Guid BookId)
        {
            var model = await context.BookRequest.Where(x => x.BookId == BookId && x.UserId.ToString() == User.GetUserId()).FirstOrDefaultAsync();
            context.BookRequest.Remove(model);
            await context.SaveChangesAsync();
            return RedirectToAction("GetBookRequestByStudent");

        }



    }
}
