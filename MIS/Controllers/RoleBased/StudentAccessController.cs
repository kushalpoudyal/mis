﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS.Services;
using MIS.ViewModel;
using MIS_Entity.DTO;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Entity.Entity.Library;
using MIS_Entity.Entity.Student;
using MIS_Entity.Enum;
using MIS_Repository.Interfaces;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.Controllers.RoleBased
{
    [Authorize(Roles = "Student,Admin")]
    [ModuleInfo(ModuleName = "StudentAccess", Url = "/StudentAccess")]
    public class StudentAccessController : Controller
    {
        private readonly ISMTDbContext context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUnitOfWork unitOfWork;
        private readonly IWebHostEnvironment hostEnvironment;
        private readonly FileUploadRequestViewModel _bucketSetting;
        private readonly IStudentAssignmentRepository assignmentRepository;
        private readonly IMapper mapper;
        private readonly IBucketService bucketService;
        public StudentAccessController(ISMTDbContext context, UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment, IStudentAssignmentRepository assignmentRepository,
            IOptions<FileUploadRequestViewModel> bucketSetting, IMapper mapper, IBucketService bucketService)
        {
            this.context = context;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
            this.hostEnvironment = hostEnvironment;
            this.assignmentRepository = assignmentRepository;
            _bucketSetting = bucketSetting.Value;
            this.mapper = mapper;
            this.bucketService = bucketService;
        }
        public async Task<IActionResult> Index()
        {
            var model = new StudentDashboardVM();
            var iSMTDbContext = context.NoticeDetails.Include(n => n.ApplicationUser);
            model.Notices = await iSMTDbContext.ToListAsync();


            return View(model);
        }
        public IActionResult LearningAll()
        {
            var CurrId = Guid.Parse(userManager.GetUserId(User));
            var GETCourseId = Guid.Parse(User.GetActiveUserCourseId());
            var CourseBooks = context.BookDetails.ToList();
            return View(CourseBooks);
        }
        public IActionResult Learning()
        {
            var CurrId = Guid.Parse(userManager.GetUserId(User));
            var GETCourseId = Guid.Parse(User.GetActiveUserCourseId());
            var CourseBooks = context.BookDetails.Where(x => x.CourseId == GETCourseId).ToList();
            return View(CourseBooks);
        }

        public IActionResult MyClasses()
        {
            var CurrId = Guid.Parse(userManager.GetUserId(User));
            var GETCourseId = Guid.Parse(User.GetActiveUserCourseId());
            var CourseBooks = unitOfWork.ClassRepository.GetCourseByStudent().Where(x => x.CourseID == GETCourseId && x.ClassStartTime >= DateTime.Now).ToList();
            return View(CourseBooks);
        }

        public IActionResult Examination()
        {
            return View();
        }

        public IActionResult Assignment()
        {
            var curUser = Guid.Parse(userManager.GetUserId(User));
            var GETCourseId = Guid.Parse(User.GetActiveUserCourseId());
            var BatchId = Guid.Parse(User.GetUserBatchId());

            var curCourse = context.StudentDetails.Where(a => a.CourseId == GETCourseId).Select(a => a.CourseId).ToList();
            var curSubjects = context.SubjectByCourse.Where(a => curCourse.Contains(a.CourseId)).Select(a => a.SubjectId).ToList();

            var assignments = context.AssignmentMaterial.Where(a => curSubjects.Contains(a.SubjectId) && a.BatchId == BatchId).ToList();

            var assignIds = assignments.Select(x => x.ID);

            ViewBag.Status = context.AssignmentByStudent.Any(a => assignIds.Contains(a.AssignmentId) && a.CreatedBy == curUser) ? "Submitted" : "Pending";
            return View(assignments);
        }

        public async Task<IActionResult> AssignmentUpload(Guid AssignId)
        {
            var assignments = await context.AssignmentMaterial.Include(x => x.BatchDetails).Where(x => x.ID == AssignId).Select(x => new AssignmentDetailViewModel
            {
                Id = x.ID,
                BatchId = x.BatchId,
                UploadedById = x.UploadedById,
                SubjectId = x.SubjectId,
                BatchName = x.BatchDetails.BatchName,
                DeadlineDate = x.DeadlineDate,
                AssignmentDetail = x.AssignmentDetail,
                FileFormat = x.FileFormat,
                FileLocation = x.FileLocation,
                AssignmentName = x.AssignmentName
            }).FirstOrDefaultAsync();

            return View(assignments);
        }
        [HttpPost]
        public async Task<IActionResult> AssignmentUpload(AssignmentUploadVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var CurrId = Guid.Parse(User.GetUserId());

                    var existAssignment = await context.AssignmentByStudent.Where(x => x.AssignmentId == model.AssignmentId && x.CreatedBy == CurrId).ToListAsync();
                    if (existAssignment.Count > 0)
                    {
                        existAssignment.ForEach(x =>
                        {
                            x.RecordStatus = RecordStatus.Inactive;
                            x.ModifiedDate = DateTime.UtcNow;
                            x.ModifiedTimes = x.ModifiedTimes++;
                        });
                        context.AssignmentByStudent.UpdateRange(existAssignment);
                    }

                    var UploadAssign = new AssignmentByStudent()
                    {
                        ID = Guid.NewGuid(),
                        Remarks = model.Remarks,
                        AssignmentId = model.AssignmentId,
                        CreatedBy = CurrId,
                        CreatedDate = DateTime.Now,
                        ModifiedBy = CurrId,
                        ModifiedDate = DateTime.Now,
                        FileLocation = model.filePath,
                        ModifiedTimes = 0,
                        RecordStatus = RecordStatus.Active,
                        AssignmentStatus = AssignmentStatus.Pending,
                        AssignmentContent = model.Remarks

                    };

                    await context.AssignmentByStudent.AddAsync(UploadAssign);


                    await context.SaveChangesAsync();
                    return Redirect("Assignment");

                }
                else
                {
                    return View(model.AssignmentId);
                }

            }
            catch
            {
                return View(model.AssignmentId);
            }


        }

        public async Task<IActionResult> UploadAssignmentDoc(IFormFile AssignmentFile)
        {
            var response = new FileUploadAjaxModel();

            try
            {
                if (AssignmentFile != null)
                {

                    var file = AssignmentFile;
                    //    var basePath = Path.Combine(hostEnvironment.WebRootPath, "eBooks");
                    //    bool pathExists = System.IO.Directory.Exists(basePath);
                    //    if (!pathExists)
                    //    {
                    //        Directory.CreateDirectory(basePath);
                    //    }
                    //    var basePath1 = "eBooks";
                    //    var filePath = Path.Combine(basePath, bookDetails.BookFile.FileName);
                    //    var filePath1 = Path.Combine(basePath1, bookDetails.BookFile.FileName);
                    //    bookDetails.BookLocation = "\\" + filePath1;
                    //    using (var stream = new FileStream(filePath, FileMode.Create))
                    //    {
                    //        await bookDetails.BookFile.CopyToAsync(stream);
                    //    }
                    var Id = Guid.NewGuid();

                    var fileName = Id + Path.GetExtension(file.FileName);
                    var Files = "";



                    var requestModel = new FileUploadRequestViewModel()
                    {
                        id = Id,
                        AccessKey = _bucketSetting.AccessKey,
                        SecretKey = _bucketSetting.SecretKey,
                        ObjectKey = fileName,
                        BucketName = _bucketSetting.BucketName + "/AssignmentDoc",
                        FilePath = Files,
                        EndPoint = _bucketSetting.EndPoint,
                        FileStream = AssignmentFile.OpenReadStream()
                    };

                    // var result = mapper.Map<FileResponseViewModel>(bucketService.UploadBlob(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    var result = mapper.Map<FileResponseViewModel>(bucketService.UploadFTP(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    if (result.success)
                    {
                        response.Status = true;
                        response.Message = "Successfully Uploaded";
                        response.FileFormat = AssignmentFile.ContentType;
                        response.FilePath = _bucketSetting.URL +  "/AssignmentDoc/" + result.fileName;

                        return Json(response);
                    }
                    else
                    {

                        response.Status = false;
                        response.Message = "Error on Uploading";

                        return Json(response);
                    }



                }
                else
                {

                    response.Status = false;
                    response.Message = "Error on Uploading";

                    return Json(response);
                }

            }
            catch
            {
                response.Status = false;
                response.Message = "Error on Uploading";

                return Json(response);
            }


        }

        public IActionResult CreateAssignment(Guid? id)
        {
            ViewData["CreatedBy"] = new SelectList(context.Users, "Id", "Id");
            //ViewBag.SubjectId = context.CourseSubject.Select(v => new SelectListItem
            //{
            //    Text = v.SubjectName.ToString(),
            //    Value = ((Guid)v.ID).ToString()
            //}).ToList();
            ViewBag.AssignmentId = id;

            //ViewData["SubjectId"] = new SelectList(context.CourseSubject, "ID", "ID");
            //ViewData["UploadedById"] = new SelectList(context.FacultyDetails, "ID", "ID");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAssignment([Bind("AssignmentContent,Remarks,AssignmentFile,AssignmentId")] AssignmentByStudent assignmentByStudent)
        {
            if (ModelState.IsValid)
            {
                assignmentByStudent.ID = Guid.NewGuid();
                assignmentByStudent.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                assignmentByStudent.CreatedDate = DateTime.Now;
                assignmentByStudent.ModifiedTimes = 0;
                assignmentByStudent.RecordStatus = RecordStatus.Active;
                // assignmentMaterial.SubjectId = "068381CC-338A-4DB0-96FE-A8205C235A3C"; 
                //assignmentMaterial.UploadedById = Guid.Parse(userManager.GetUserId(User)); 

                if (assignmentByStudent.AssignmentFile != null)
                {
                    var basePath = Path.Combine(hostEnvironment.WebRootPath, "AssignmentDoc");
                    bool pathExists = System.IO.Directory.Exists(basePath);
                    if (!pathExists)
                    {
                        Directory.CreateDirectory(basePath);
                    }
                    var basePath1 = "AssignmentDoc";
                    var filePath = Path.Combine(basePath, assignmentByStudent.AssignmentFile.FileName);
                    var filePath1 = Path.Combine(basePath1, assignmentByStudent.AssignmentFile.FileName);
                    assignmentByStudent.FileLocation = "\\" + filePath1;
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await assignmentByStudent.AssignmentFile.CopyToAsync(stream);
                    }

                }

                assignmentRepository.Add(assignmentByStudent);

                await unitOfWork.SaveAsync();

                return RedirectToAction("Assignment", "StudentAccess");
            }
            ViewData["CreatedBy"] = new SelectList(context.Users, "Id", "Id", assignmentByStudent.CreatedBy);
            return View(assignmentByStudent);
        }
    }

}
