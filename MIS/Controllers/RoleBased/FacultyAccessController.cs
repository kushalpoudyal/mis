﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIS.DBContext;
using MIS.Extensions;
using MIS.ViewModel;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Repository.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MIS.Helper;
using AutoMapper;
using MIS.Services;
using MIS_Entity.DTO;
using Microsoft.Extensions.Options;

namespace MIS.Controllers.RoleBased
{
    [Authorize(Roles = "Faculty,Admin,Librarian")]
    [ModuleInfo(ModuleName = "FacultyAccess", Url = "/FacultyAccess")]
    public class FacultyAccessController : Controller
    {
        private readonly ISMTDbContext context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUnitOfWork unitOfWork;
        private readonly IAssignmentRepository assignmentRepository;
        private readonly IWebHostEnvironment hostEnvironment;
        private readonly IMapper mapper;
        private readonly IBucketService bucketService;
        private readonly FileUploadRequestViewModel _bucketSetting;
        public FacultyAccessController(ISMTDbContext context, UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork, IAssignmentRepository assignmentRepository, IWebHostEnvironment hostEnvironment, IMapper mapper,
            IBucketService bucketService,
            IOptions<FileUploadRequestViewModel> bucketSetting)
        {
            this.context = context;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
            this.assignmentRepository = assignmentRepository;
            this.hostEnvironment = hostEnvironment;
            this.mapper = mapper;
            this.bucketService = bucketService;
            _bucketSetting = bucketSetting.Value;
        }
        public async Task<IActionResult> Index()
        {
            var model = new FacultyDashboardVM();
            var iSMTDbContext = context.NoticeDetails.Include(n => n.ApplicationUser);
            model.Notices = await iSMTDbContext.ToListAsync();
            return View(model);
        }

        public IActionResult Learning()
        {

            var CourseBooks = context.BookDetails.ToList();
            return View(CourseBooks);
        }

        public IActionResult MyClasses()
        {
            var CurrId = Guid.Parse(userManager.GetUserId(User));
            var GetSubjectId = JsonConvert.DeserializeObject<List<Guid>>(User.GetFacultySubjects());

            var CourseBooks = unitOfWork.ClassRepository.GetCourseByStudent().Where(x => GetSubjectId.Contains(x.SubjectID) && x.ClassStartTime >= DateTime.Now).ToList();
            return View(CourseBooks);
        }
        public IActionResult MyClassAttendance(Guid ClassId)
        {
            var model = new List<ManageAttendanceViewModel>();
            var CurrId = Guid.Parse(userManager.GetUserId(User));
            var GetSubjectId = JsonConvert.DeserializeObject<List<Guid>>(User.GetFacultySubjects());
            model = context.UserAttendanceDetails.Where(x => x.ClassId == ClassId).Include(x => x.ApplicationUser).
              Select(x => new ManageAttendanceViewModel
              {
                  StudentId = x.UserID,
                  StudentName = x.ApplicationUser.Fullname
              }).ToList();
            return View(model);
        }

        public IActionResult Examination()
        {
            return View();
        }

        public IActionResult Assignment()
        {
            var assignments = context.AssignmentMaterial.Where(x => x.CreatedBy == Guid.Parse(User.GetUserId())).ToList();
            if (User.IsAdmin())
            {
                ViewBag.Layout = "~/Views/Shared/_LayoutAdmin.cshtml";

            }
            else
            {
                ViewBag.Layout = "~/Views/Shared/_LayoutFaculty.cshtml";
            }
            return View(assignments);
        }

        public async Task<IActionResult> AssignmentDetail(Guid AssignId)
        {
            var assignments = await context.AssignmentMaterial.Include(x => x.BatchDetails).Where(x => x.ID == AssignId).Select(x => new AssignmentDetailViewModel
            {
                Id = x.ID,
                BatchId = x.BatchId,
                UploadedById = x.UploadedById,
                SubjectId = x.SubjectId,
                BatchName = x.BatchDetails.BatchName,
                DeadlineDate = x.DeadlineDate,
                AssignmentDetail = x.AssignmentDetail,
                FileFormat = x.FileFormat,
                FileLocation = x.FileLocation,
                AssignmentName = x.AssignmentName
            }).FirstOrDefaultAsync();

            var GetSubmittedStudents = await context.AssignmentByStudent.Where(x => x.AssignmentId == AssignId && x.RecordStatus == RecordStatus.Active).Select(x => new AssignmentByStudentVM
            {
                AssignmentId = x.AssignmentId,
                AssignmentContent = x.AssignmentContent,
                AssignmentStatus = x.AssignmentStatus,
                FileLocation = x.FileLocation,
                StudentName = x.ApplicationUser.Fullname,
                Remarks = x.Remarks,
                StudentId = x.CreatedBy,
                CreatedOn = x.CreatedDate,
            }).ToListAsync();
            if (GetSubmittedStudents.Count > 0)
            {
                assignments.AssignmentByStudentVM = GetSubmittedStudents.Where(x=>x.AssignmentStatus == MIS_Entity.Enum.AssignmentStatus.Pending).ToList();
                assignments.AssignmentByStudentRejected = GetSubmittedStudents.Where(x=>x.AssignmentStatus == MIS_Entity.Enum.AssignmentStatus.Rejected).ToList();
                assignments.AssignmentByStudentReviewed = GetSubmittedStudents.Where(x=>x.AssignmentStatus == MIS_Entity.Enum.AssignmentStatus.Approved).ToList();
                assignments.AssignmentByStudentReturned = GetSubmittedStudents.Where(x => x.AssignmentStatus == MIS_Entity.Enum.AssignmentStatus.Returned).ToList(); 
            }


            return View(assignments);
        }

        public IActionResult CreateAssignment()
        {
            ViewData["CreatedBy"] = new SelectList(context.Users, "Id", "Id");
            ViewBag.SubjectId = context.CourseSubject.Select(v => new SelectListItem
            {
                Text = v.SubjectName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            ViewBag.BatchId = context.BatchDetails.Select(v => new SelectListItem
            {
                Text = v.BatchName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();


            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAssignment(AssignmentMaterial assignmentMaterial)
        {
            if (ModelState.IsValid)
            {
                assignmentMaterial.ID = Guid.NewGuid();
                assignmentMaterial.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                assignmentMaterial.CreatedDate = DateTime.Now;
                assignmentMaterial.ModifiedTimes = 0;
                assignmentMaterial.RecordStatus = RecordStatus.Active;
                assignmentMaterial.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                assignmentMaterial.UploadedById = Guid.Parse(userManager.GetUserId(User));

                if (assignmentMaterial.AssignmentFile != null)
                {
                    var file = assignmentMaterial.AssignmentFile;
                    var Id = Guid.NewGuid();

                    var fileName = Id + Path.GetExtension(file.FileName);
                    var Files = "";

                    var requestModel = new FileUploadRequestViewModel()
                    {
                        id = Id,
                        AccessKey = _bucketSetting.AccessKey,
                        SecretKey = _bucketSetting.SecretKey,
                        ObjectKey = fileName,
                        BucketName = _bucketSetting.BucketName + "/AssignmentDoc",
                        FilePath = Files,
                        EndPoint = _bucketSetting.EndPoint,
                        FileStream = assignmentMaterial.AssignmentFile.OpenReadStream()
                    };


                    var result = mapper.Map<FileResponseViewModel>(bucketService.UploadFTP(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    if (result.success)
                    {
                        assignmentMaterial.FileLocation = _bucketSetting.URL + "/AssignmentDoc/" + result.fileName;
                        assignmentMaterial.FileFormat = assignmentMaterial.AssignmentFile.ContentType;
                    }

                }
                assignmentRepository.Add(assignmentMaterial);
                await unitOfWork.SaveAsync();

                return RedirectToAction("Assignment", "FacultyAccess");
            }
            ViewData["CreatedBy"] = new SelectList(context.Users, "Id", "Id", assignmentMaterial.CreatedBy);
            ViewData["SubjectId"] = new SelectList(context.CourseSubject, "ID", "ID", assignmentMaterial.SubjectId);
            ViewData["UploadedById"] = new SelectList(context.FacultyDetails, "ID", "ID", assignmentMaterial.UploadedById);
            return View(assignmentMaterial);
        }
    }
}
