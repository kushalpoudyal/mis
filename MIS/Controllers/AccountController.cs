﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS.Services;
using MIS.ViewModel;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Entity.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MIS.Controllers
{
    [ModuleInfo(ModuleName = "Account", Url = "/Account")]

    public class AccountController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ILogger<AccountController> _logger;
        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly IEmailService emailService;

        public AccountController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger, RoleManager<ApplicationRole> roleManager, IEmailService emailService)
        {
            _context = context;
            this.userManager = userManager;
            this.signInManager = signInManager;
            _logger = logger;
            this.roleManager = roleManager;
            this.emailService = emailService;
        }

        public async Task<IActionResult> Register()
        {
            var model = new RegistrationViewModel()
            {
                Password = "iSMT@010203#",
                ConfirmPassword = "iSMT@010203#",

            };

            var roleList = await roleManager.Roles.ToListAsync();
            ViewBag.RoleList = roleList;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegistrationViewModel model, params string[] role)
        {
            var UserType = string.Empty;
            if (ModelState.IsValid)
            {
                if (role != null)
                {
                    var user = new ApplicationUser()
                    {
                        UserName = model.Email,
                        Email = model.Email,
                        Status = true,
                        Deleted = false,
                        CreatedBy = Guid.Parse(User.GetUserId()),
                        ModifiedBy = Guid.Parse(User.GetUserId()),
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now
                    };
                    var result = await userManager.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        user.Fullname = model.FullName;
                        user.Contact = model.Contact;
                        user.Country = model.Country;
                        user.WorkTitle = model.WorkTitle;



                        _context.Users.Update(user);
                        await _context.SaveChangesAsync();

                        var results = await userManager.AddToRolesAsync(user, role);
                        if (!results.Succeeded)
                        {
                            foreach (var item in results.Errors)
                            {
                                ModelState.AddModelError("", item.Description);
                            }
                            return View(model);
                        }
                        var msg = $"Dear {user.Fullname},<br/> Check your login credential for VLE: <br/><b>username</b>:{user.Email}" +
                            $"<br/><b>password</b>:{model.Password}<br/><i>Please change your password after logging in.</i>";
                        await this.emailService.Mail_Alert(user.Email, "VLE Registration", msg);

                        return Redirect("Index");
                    }
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError("", item.Description);
                    }

                }
                else
                {
                    ModelState.AddModelError("", "Please select atleast one role.");
                }


            }
            var roleList = await roleManager.Roles.ToListAsync();
            ViewBag.RoleList = roleList;
            return View(model);
        }

        public async Task<IActionResult> GetLoginDetails()
        {
            var loginDetails = await _context.LoginActivity.Select(x => new GetLoginViewModel()
            {
                AuditDate = x.AuditDate,
                Email = x.Username,
                LoginLatitude = x.LoginLatitude,
                LoginLongitude = x.LoginLongitude,
                LoginIP = x.LoginIP,
                Success = x.Success,
                UserAgent = x.UserAgent

            }).OrderByDescending(x => x.AuditDate).ToListAsync();
            return View(loginDetails);
        }
        [AllowAnonymous]
        public IActionResult Login()
        {
            //var msg = $"Dear Test,<br/> Check your login credential for VLE: <br/><b>username</b>:kuku@gmail.com" +
            //$"<br/><b>password</b>:123123<br/><i>Please change your password after logging in.</i>";
            //await this.emailService.Mail_Alert("kushalpoudyal94@gmail.com", "VLE Registration", msg);

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

                    if (result.Succeeded)
                    {
                        var usrdetails = await _context.Users.Where(x => x.UserName == model.Email).FirstOrDefaultAsync();

                        var CourseId = await _context.StudentDetails.Where(x => x.ID == usrdetails.Id)
                            .Select(x => new
                            {
                                x.CourseId,
                                x.BatchId
                            }).FirstOrDefaultAsync();

                        var usrRole = await this.userManager.GetRolesAsync(usrdetails);

                        var roleDetail = await this.roleManager.FindByNameAsync(usrRole.FirstOrDefault());
                        if (usrdetails.RecordStatus == RecordStatus.Suspended)
                        {
                            ModelState.AddModelError("", "Account has been suspended. Contact Administrator");
                            return View(model);
                        }
                        var Loginstate = new LoginActivity
                        {
                            UserId = usrdetails.Id,
                            LoginType = usrRole.FirstOrDefault().ToString(),
                            Username = model.Email,
                            Remarks = "Logged In",
                            Success = true,
                            LoginIP = HttpContext.Connection.RemoteIpAddress.ToString(),
                            LoginLatitude = model.LoginLatitude,
                            LoginLongitude = model.LoginLongitude,
                            UserAgent = model.UserAgent
                        };

                        await recordLogin(Loginstate).ConfigureAwait(false);
                        if (usrRole.Contains(Roles.Admin.ToString()))
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        else if (usrRole.Contains(Roles.Student.ToString()))
                        {

                            if (usrdetails.IsPasswordUpdated)
                            {
                                return RedirectToAction("Index", "StudentAccess");
                            }
                            else
                            {
                                return RedirectToAction("ChangePassword", "Account");
                            }
                        }
                        else if (usrRole.Contains("Faculty")|| usrRole.Contains("Librarian"))
                        {
                            if (usrdetails.IsPasswordUpdated)
                            {
                                return RedirectToAction("Index", "FacultyAccess");
                            }
                            else
                            {
                                return RedirectToAction("ChangePassword", "Account");
                            }
                        }
                        else
                        {

                            if (usrdetails.IsPasswordUpdated)
                            {
                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                return RedirectToAction("ChangePassword", "Account");
                            }
                        }
                    }
                    else
                    {
                        var Loginstate = new LoginActivity
                        {
                            UserId = Guid.Empty,
                            LoginType = "Error",
                            Username = model.Email,
                            Remarks = $"Failure, Incorrect User Or Password",
                            Success = false
                        };
                        await recordLogin(Loginstate).ConfigureAwait(false);
                    }

                    ModelState.AddModelError("", "Invalid Login Attempt");
                }

            }
            catch (Exception ex)
            {
                var Loginstate = new LoginActivity
                {
                    UserId = Guid.Empty,
                    LoginType = "Error",
                    Username = model.Email,
                    Remarks = $"Failure, {ex.Message}",
                    Success = false
                };
                await recordLogin(Loginstate).ConfigureAwait(false);
            }
            return View(model);
        }

        private async Task recordLogin(LoginActivity model)
        {
            await _context.LoginActivity.AddAsync(model).ConfigureAwait(false);
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        // GET: Account
        public async Task<IActionResult> Index()
        {
            return View(await _context.Users.ToListAsync());
        }

        // GET: Account/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }

        // GET: Account/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Account/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Fullname,Contact,Country,Organization,WorkTitle,IsPasswordUpdated,Id,UserName,NormalizedUserName,Email,NormalizedEmail,EmailConfirmed,PasswordHash,SecurityStamp,ConcurrencyStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnd,LockoutEnabled,AccessFailedCount")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                applicationUser.Id = Guid.NewGuid();
                _context.Add(applicationUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(applicationUser);
        }
        public async Task<IActionResult> MyProfile(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users.FindAsync(id);
            var role = await _context.UserRoles.Where(x => x.UserId == id).FirstOrDefaultAsync();
            var roleName = await _context.Roles.Where(x => x.Id == role.RoleId).Select(x => x.Name).FirstOrDefaultAsync();

            if (applicationUser == null)
            {
                return NotFound();
            }
            var model = new EditUserViewModel()
            {

                FullName = applicationUser.Fullname,
                Email = applicationUser.Email,
                Contact = applicationUser.Contact,
                CurrentRole = roleName
            };

            var roleList = await roleManager.Roles.ToListAsync();
            ViewBag.RoleList = roleList;
            return View(model);
        }
        // GET: Account/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users.FindAsync(id);
            var role = await _context.UserRoles.Where(x => x.UserId == id).FirstOrDefaultAsync();
            var roleName = await _context.Roles.Where(x => x.Id == role.RoleId).Select(x => x.Name).FirstOrDefaultAsync();

            if (applicationUser == null)
            {
                return NotFound();
            }
            var model = new EditUserViewModel()
            {

                FullName = applicationUser.Fullname,
                Email = applicationUser.Email,
                Contact = applicationUser.Contact,
                CurrentRole = roleName
            };

            var roleList = await roleManager.Roles.ToListAsync();
            ViewBag.RoleList = roleList;
            return View(model);
        }

        // POST: Account/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditUserViewModel applicationUser, params string[] role)
        {

            var UserType = string.Empty;
            if (ModelState.IsValid)
            {
                if (role != null)
                {
                    var user = await userManager.FindByIdAsync(applicationUser.Id.ToString());
                    user.Fullname = applicationUser.FullName;
                    user.Contact = applicationUser.Contact;
                    user.Country = applicationUser.Country;
                    user.WorkTitle = applicationUser.WorkTitle;

                    var result = await userManager.UpdateAsync(user);

                    if (result.Succeeded)
                    {
                        await _context.SaveChangesAsync();
                        var rolesId = _context.UserRoles.Where(x => x.UserId == applicationUser.Id).Select(x => x.RoleId).AsEnumerable();
                        IEnumerable<string> userRole =
                            await _context.Roles.Where(x => rolesId.Contains(x.Id)).Select(x => x.Name).ToListAsync();
                        await userManager.RemoveFromRolesAsync(user, userRole);

                        var results = await userManager.AddToRolesAsync(user, role);
                        if (!results.Succeeded)
                        {
                            foreach (var item in results.Errors)
                            {
                                ModelState.AddModelError("", item.Description);
                            }
                            return View(applicationUser);
                        }

                        return RedirectToAction("Index");
                    }
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError("", item.Description);
                    }

                }
                else
                {
                    ModelState.AddModelError("", "Please select atleast one role.");
                }


            }
            var roleList = await roleManager.Roles.ToListAsync();
            ViewBag.RoleList = roleList;
            return View(applicationUser);
        }

        // GET: Account/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }

        // POST: Account/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var applicationUser = await _context.Users.FindAsync(id);
            _context.Users.Remove(applicationUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ApplicationUserExists(Guid id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
        [AllowAnonymous]

        public IActionResult ChangePassword()
        {
            return View();
        }
        [AllowAnonymous]

        public IActionResult ChangePasswordConfirmation()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", " Please try again!");
                return View(model);

            }

            var userID = User.GetUserId();
            if (string.IsNullOrEmpty(userID))
            {
                return RedirectToAction("Login");

            }
            var UserT = await _context.Users.Where(x => x.Id == Guid.Parse(User.GetUserId())).FirstOrDefaultAsync();
            var result = await this.userManager.ChangePasswordAsync(UserT, model.OldPassword, model.NewPassword);

            if (result.Succeeded)
            {
                UserT.IsPasswordUpdated = true;
                await userManager.UpdateAsync(UserT);
                return View("ChangePasswordConfirmation");
            }
            foreach (var item in result.Errors)
            {
                ModelState.AddModelError("", item.Description.ToString());
            }
            return View(model);

        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword()
        {

            return View();
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {

            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await userManager.FindByEmailAsync(model.Email);
                var super = await userManager.FindByIdAsync(User.GetUserId());

                if (!await userManager.CheckPasswordAsync(super, model.SuperPassword))
                {
                    ModelState.AddModelError("", "Unauthorized attempt to change password.");
                }

                if (user != null)
                {
                    model.Token = await userManager.GeneratePasswordResetTokenAsync(user);
                    // reset the user password
                    var result = await userManager.ResetPasswordAsync(user, model.Token, model.ConfirmPassword);
                    if (result.Succeeded)
                    {
                        var msg = $"Dear {user.Fullname},<br/> Check your login credential for VLE: <br/><b>username</b>:{user.Email}" +
                          $"<br/><b>password</b>:{model.ConfirmPassword}<br/><i>Please change your password after logging in.</i>";
                        await this.emailService.Mail_Alert(user.Email, "VLE Registration", msg);
                        user.IsPasswordUpdated = false;
                        await userManager.UpdateAsync(user);
                        return View("ResetPasswordConfirmation");
                    }
                    // Display validation errors. For example, password reset token already
                    // used to change the password or password complexity rules not met
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(model);
                }

                // To avoid account enumeration and brute force attacks, don't
                // reveal that the user does not exist
                return View("Index");
            }
            // Display validation errors if model state is not valid
            return View(model);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}