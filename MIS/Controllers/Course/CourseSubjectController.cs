﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Helper;
using MIS_Entity.Entity.Course;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Repository.Interfaces;

namespace MIS.Controllers.Course
{
    [ModuleInfo(ModuleName = "CourseSubject", Url = "/CourseSubject")]
    public class CourseSubjectController : Controller
    {
        private readonly ISMTDbContext _context;
        private readonly ICourseSubjectRepository _courseSubjectRepository;
        private readonly IFacultyRepository facultyRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;

        public CourseSubjectController(ISMTDbContext context,
            ICourseSubjectRepository courseSubjectRepository,
             IUnitOfWork unitOfWork,
             UserManager<ApplicationUser> userManager, IFacultyRepository facultyRepository)
        {
            _context = context;
            _courseSubjectRepository = courseSubjectRepository;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            this.facultyRepository = facultyRepository;
        }

        // GET: CourseSubject
        public async Task<IActionResult> Index()
        {
            return View();
        }
        public async Task<IActionResult> GetCourseIndex()
        {
            //var iSMTDbContext = _context.BookDetails.Include(b => b.ApplicationUser);
            //return View(await iSMTDbContext.ToListAsync());

            var course = _courseSubjectRepository.GetAll().ToList();
            return View(course);
        }
        public async Task<IActionResult> GetAllSubjects()
        {
            //var iSMTDbContext = _context.BookDetails.Include(b => b.ApplicationUser);
            //return View(await iSMTDbContext.ToListAsync());

            var course = _courseSubjectRepository.GetAll().ToList();
            ViewBag.Teachers = facultyRepository.GetAll().Select(v => new SelectListItem
            {
                Text = v.FacultyName.ToString(),
                Value = v.ID.ToString()
            }).ToList();
            return View(course);
        }

        // GET: CourseSubject/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseSubject = await _context.CourseSubject
                .Include(c => c.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (courseSubject == null)
            {
                return NotFound();
            }

            return View(courseSubject);
        }

        // GET: CourseSubject/Create
        public IActionResult Create()
        {
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: CourseSubject/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SubjectCode,SubjectName,SubjectDescription,CreditHours,ID,CreatedBy,ModifiedBy,CreatedDate,ModifiedTimes,ModifiedDate,RecordStatus")] CourseSubject courseSubject)
        {
            if (ModelState.IsValid)
            {
                courseSubject.ID = Guid.NewGuid();
                courseSubject.CreatedBy = Guid.Parse(_userManager.GetUserId(User));
                courseSubject.CreatedDate = DateTime.Now;
                courseSubject.ModifiedTimes = 0;
                courseSubject.RecordStatus = RecordStatus.Active;
                _courseSubjectRepository.Add(courseSubject);
                await _unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", courseSubject.CreatedBy);
            return View(courseSubject);
        }

        // GET: CourseDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseDetails = _courseSubjectRepository.Get(id);

            if (courseDetails == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", courseDetails.CreatedBy);
            ViewBag.ID = id;
            ViewBag.CreatedBy = courseDetails.CreatedBy;
            ViewBag.CreatedDate = courseDetails.CreatedDate;
            return View(courseDetails);
        }

        // POST: CourseSubject/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("SubjectCode,SubjectName,SubjectDescription,CreditHours,ID,CreatedBy,ModifiedBy,CreatedDate,ModifiedTimes,ModifiedDate,RecordStatus")] CourseSubject courseSubject)
        {
            if (id != courseSubject.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    courseSubject.ModifiedBy = Guid.Parse(_userManager.GetUserId(User));
                    courseSubject.ModifiedDate = DateTime.Now;
                    courseSubject.ModifiedTimes = courseSubject.ModifiedTimes + 1;
                    _courseSubjectRepository.Update(courseSubject);
                    await _unitOfWork.SaveAsync();


                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CourseSubjectExists(courseSubject.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", courseSubject.CreatedBy);
            return View(courseSubject);
        }

        // GET: CourseSubject/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseSubject = await _context.CourseSubject
                .Include(c => c.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (courseSubject == null)
            {
                return NotFound();
            }

            return View(courseSubject);
        }

        // POST: CourseSubject/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var courseDetails = _courseSubjectRepository.Get(id);
            _courseSubjectRepository.Remove(courseDetails);
            await _unitOfWork.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CourseSubjectExists(Guid id)
        {
            return _context.CourseSubject.Any(e => e.ID == id);
        }
    }
}
