﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Helper;
using MIS.ViewModel;
using MIS_Entity.Entity.Course;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Repository;
using MIS_Repository.Interfaces;

namespace MIS.Controllers.Course
{
    [Authorize]
    [ModuleInfo(ModuleName = "CourseDetails", Url = "/CourseDetails")]
    public class CourseDetailsController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUnitOfWork unitOfWork;
        private readonly ICourseRepository courseRepository;
        private readonly IMapper _mapper;

        public CourseDetailsController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork,
            ICourseRepository courseRepository,
            IMapper mapper)
        {
            _context = context;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
            this.courseRepository = courseRepository;
            _mapper = mapper;
        }

        // GET: CourseDetails
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> GetCourseIndex()
        {
            //var iSMTDbContext = _context.BookDetails.Include(b => b.ApplicationUser);
            //return View(await iSMTDbContext.ToListAsync());

            var course = courseRepository.GetAll().ToList();
            return View(course);
        }

        // GET: CourseDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseDetails = await _context.CourseDetails
                .Include(c => c.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (courseDetails == null)
            {
                return NotFound();
            }


            return View(courseDetails);
        }

        // GET: CourseDetails/Create
        public IActionResult Create()
        {
            var course = new CourseDetailViewModel();
            var selectListItem = new List<SubjectForCourseViewModel>();

            selectListItem.Add(new SubjectForCourseViewModel()
            {
                SubjectId = Guid.NewGuid(),
                FacultyId = Guid.NewGuid()

            });
            course.SubjectCourses = selectListItem;
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            return View(course);
        }

        // POST: CourseDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CourseDetailViewModel courseDel)
        {
            if (ModelState.IsValid)
            {
                CourseDetails courseDetails = _mapper.Map<CourseDetails>(courseDel);
                courseDetails.ID = Guid.NewGuid();
                courseDetails.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                courseDetails.ModifiedBy = courseDetails.CreatedBy;
                courseDetails.CreatedDate = DateTime.Now;
                courseDetails.ModifiedDate = DateTime.Now;
                courseDetails.ModifiedTimes = 0;
                courseDetails.RecordStatus = RecordStatus.Active;

                courseRepository.Add(courseDetails);

                foreach (var item in courseDel.SubjectCourses)
                {
                    var subjCourse = new SubjectByCourse();
                    subjCourse.CourseId = courseDetails.ID;
                    subjCourse.SubjectId = item.SubjectId;
                    subjCourse.FacultyId = item.FacultyId;

                    _context.SubjectByCourse.Add(subjCourse);
                }

                await unitOfWork.SaveAsync();

                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", courseDel.CreatedBy);
            return View(courseDel);
        }

        // GET: CourseDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseDetails = courseRepository.Get(id);
            if (courseDetails == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", courseDetails.CreatedBy);
            ViewBag.ID = id;
            ViewBag.CreatedBy = courseDetails.CreatedBy;
            ViewBag.CreatedDate = courseDetails.CreatedDate;
            return View(courseDetails);
        }

        // POST: CourseDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, CourseDetailViewModel courseDel)
        {
            if (id != courseDel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    CourseDetails courseDetails = _mapper.Map<CourseDetails>(courseDel);
                    courseDetails.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                    courseDetails.ModifiedDate = DateTime.Now;
                    courseDetails.ModifiedTimes = courseDetails.ModifiedTimes + 1;

                    courseRepository.Update(courseDetails);
                    
                    if (courseDel.SubjectCourses != null)
                    {
                        _context.SubjectByCourse.RemoveRange();
                        foreach (var item in courseDel.SubjectCourses)
                        {
                            var subjCourse = new SubjectByCourse();
                            subjCourse.CourseId = courseDetails.ID;
                            subjCourse.SubjectId = item.SubjectId;
                            subjCourse.FacultyId = item.FacultyId;

                            _context.SubjectByCourse.Add(subjCourse);
                        }
                    }

                    await unitOfWork.SaveAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CourseDetailsExists(courseDel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", courseDel.CreatedBy);
            return View(courseDel);
        }

        // GET: CourseDetails/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseDetails = await _context.CourseDetails
                .Include(c => c.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (courseDetails == null)
            {
                return NotFound();
            }

            return View(courseDetails);
        }

        // POST: CourseDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            try
            {
                var courseDetails = courseRepository.Get(id);
                courseRepository.Remove(courseDetails);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private bool CourseDetailsExists(Guid id)
        {
            return _context.CourseDetails.Any(e => e.ID == id);
        }
    }
}
