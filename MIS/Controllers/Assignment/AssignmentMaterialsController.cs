﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS.Services;
using MIS.ViewModel;
using MIS_Data.Migrations;
using MIS_Entity.DTO;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Entity.Entity.Student;
using MIS_Repository.Interfaces;

namespace MIS.Controllers.Assignment
{
    [ModuleInfo(ModuleName = "AssignmentMaterials", Url = "/AssignmentMaterials")]
    public class AssignmentMaterialsController : Controller
    {
        private readonly ISMTDbContext _context;
        private readonly IUnitOfWork unitOfWork;
        private readonly IAssignmentRepository assignmentRepository;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly FileUploadRequestViewModel _bucketSetting;
        private readonly IBucketService bucketService;
        private readonly IMapper mapper;
        private readonly IEmailService emailService;

        public AssignmentMaterialsController(ISMTDbContext context,
            IUnitOfWork unitOfWork,
            IAssignmentRepository assignmentRepository,
            UserManager<ApplicationUser> userManager,
            IOptions<FileUploadRequestViewModel> bucketSetting,
            IBucketService bucketService,
            IEmailService emailService)
        {
            _context = context;
            this.unitOfWork = unitOfWork;
            this.assignmentRepository = assignmentRepository;
            this.userManager = userManager;
            _bucketSetting = bucketSetting.Value;
            this.bucketService = bucketService;
            this.emailService = emailService;
        }

        // GET: AssignmentMaterials
        public async Task<IActionResult> Index()
        {
            var iSMTDbContext = _context.AssignmentMaterial.Include(a => a.ApplicationUser).Include(a => a.CourseSubject);
            return View(await iSMTDbContext.ToListAsync());
        }


        // GET: AssignmentMaterials/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignmentMaterial = await _context.AssignmentMaterial
                .Include(a => a.ApplicationUser)
                .Include(a => a.CourseSubject)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (assignmentMaterial == null)
            {
                return NotFound();
            }

            return View(assignmentMaterial);
        }

        public async Task<IActionResult> AssignmentTimeline(Guid StudentId, Guid AssignmentId)
        {
            var assignmentMaterial = await _context.AssignmentByStudent
    .Include(a => a.ApplicationUser)
    .FirstOrDefaultAsync(m => m.CreatedBy == StudentId && m.AssignmentId == AssignmentId && m.RecordStatus == RecordStatus.Active);

            var assignment = await _context.AssignmentMaterial.FirstOrDefaultAsync(x => x.ID == AssignmentId);

            var assignmentTimline = await _context.AssignmentTimeline.Include(x => x.ApplicationUser)
                .Where(x => x.AssignmentByStudentId == assignmentMaterial.ID)
                .ToListAsync();

            if (assignment == null || assignmentMaterial == null)
            {
                return NotFound();
            }

            var timelineModel = new AssignmentTimelineDetailsViewModel
            {
                AssignmentTimelineViewModel = assignmentTimline.Select(x => new AssignmentTimelineViewModel
                {
                    AssignmentByStudentId = x.AssignmentByStudentId,
                    AssignmentStatus = x.AssignmentStatus,
                    GivenByName = x.ApplicationUser.Fullname,
                    GivenOn = x.GivenOn,
                    GivenById = x.GivenById,
                    Remarks = x.Remarks,
                }).ToList(),
                AssignmentName = assignment.AssignmentName,
                AssignmentId = AssignmentId,
                FileLocation = assignmentMaterial.FileLocation,
                Remarks = assignmentMaterial.Remarks,
                AssignmentStatus = assignmentMaterial.AssignmentStatus,
                SubmittedByName = assignmentMaterial.ApplicationUser.Fullname,
                AssignmentByStudentId = assignmentMaterial.ID


            };



            return View(timelineModel);
        }
        [HttpPost]
        public async Task<IActionResult> AssignmentTimeline(AssignmentTimeline entity)
        {
            var assignmentMaterial = await _context.AssignmentByStudent
                    .Include(a => a.ApplicationUser)
                    .Include(a => a.AssignmentMaterial)
                    .FirstOrDefaultAsync(m => m.ID == entity.AssignmentByStudentId );
            try
            {
                entity.GivenById = Guid.Parse(User.GetUserId());
                _context.AssignmentTimeline.Add(entity);



                assignmentMaterial.AssignmentStatus = entity.AssignmentStatus;
                assignmentMaterial.ModifiedDate = DateTime.Now;

                _context.AssignmentByStudent.Update(assignmentMaterial);

                await _context.SaveChangesAsync();

                var msg = $"Dear {assignmentMaterial.ApplicationUser.Fullname},<br/> Your Submitted assignment for {assignmentMaterial.AssignmentMaterial.AssignmentName} has been <b>{entity.AssignmentStatus.ToString()}</b> on {assignmentMaterial.ModifiedDate}.";
                await this.emailService.Mail_Alert(assignmentMaterial.ApplicationUser.Email, "Assignment Notification - " + entity.AssignmentStatus.ToString(), msg);

                TempData["Success"] = "Successfully updated assignment record";
                return RedirectToAction("AssignmentTimeline", new
                { StudentId = assignmentMaterial.CreatedBy, AssignmentId = assignmentMaterial.AssignmentId });
            }
            catch (Exception ex)
            {

                TempData["Error"] = "Could not update the record";
                return RedirectToAction("AssignmentTimeline", new
                { StudentId = assignmentMaterial.CreatedBy, AssignmentId = assignmentMaterial.AssignmentId });
            }


        }

        // GET: AssignmentMaterials/Create
        public IActionResult Create()
        {
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            ViewData["SubjectId"] = new SelectList(_context.CourseSubject, "ID", "ID");
            ViewData["UploadedById"] = new SelectList(_context.FacultyDetails, "ID", "ID");
            return View();
        }

        // POST: AssignmentMaterials/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AssignmentName,AssignmentDetail,SubjectId,UploadedById")] AssignmentMaterial assignmentMaterial)
        {
            if (ModelState.IsValid)
            {
                assignmentMaterial.ID = Guid.NewGuid();
                assignmentMaterial.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                assignmentMaterial.CreatedDate = DateTime.Now;
                assignmentMaterial.ModifiedTimes = 0;
                assignmentMaterial.RecordStatus = RecordStatus.Active;
                // assignmentMaterial.SubjectId = "068381CC-338A-4DB0-96FE-A8205C235A3C"; 
                //assignmentMaterial.UploadedById = Guid.Parse(userManager.GetUserId(User));
                assignmentMaterial.DeadlineDate = DateTime.Now;
                assignmentMaterial.FileFormat = null;
                assignmentMaterial.FileLocation = null;
                if (assignmentMaterial.AssignmentFile != null)
                {
                    var file = assignmentMaterial.AssignmentFile;

                    var Id = Guid.NewGuid();

                    var fileName = Id + Path.GetExtension(file.FileName);
                    var Files = "";
                    var requestModel = new FileUploadRequestViewModel()
                    {
                        id = Id,
                        AccessKey = _bucketSetting.AccessKey,
                        SecretKey = _bucketSetting.SecretKey,
                        ObjectKey = fileName,
                        BucketName = _bucketSetting.BucketName + "/AssignmentDocs",
                        FilePath = Files,
                        EndPoint = _bucketSetting.EndPoint,
                        FileStream = assignmentMaterial.AssignmentFile.OpenReadStream(),
                        FileLength = file.Length,
                    };

                    //var result = mapper.Map<FileResponseViewModel>(bucketService.UploadBlob(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    var result = mapper.Map<FileResponseViewModel>(bucketService.UploadFTP(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    if (result.success)
                    {
                        assignmentMaterial.FileLocation = _bucketSetting.URL + "/AssignmentDocs/" + result.fileName;
                    }
                }

                assignmentRepository.Add(assignmentMaterial);

                await unitOfWork.SaveAsync();

                return RedirectToAction("Assignment", "FacultyAccess");
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", assignmentMaterial.CreatedBy);
            ViewData["SubjectId"] = new SelectList(_context.CourseSubject, "ID", "ID", assignmentMaterial.SubjectId);
            ViewData["UploadedById"] = new SelectList(_context.FacultyDetails, "ID", "ID", assignmentMaterial.UploadedById);
            return View(assignmentMaterial);
        }

        // GET: AssignmentMaterials/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignmentMaterial = await _context.AssignmentMaterial.FindAsync(id);
            if (assignmentMaterial == null)
            {
                return NotFound();
            }

            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            ViewBag.SubjectId = _context.CourseSubject.Select(v => new SelectListItem
            {
                Text = v.SubjectName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            ViewBag.BatchId = _context.BatchDetails.Select(v => new SelectListItem
            {
                Text = v.BatchName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();


            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", assignmentMaterial.CreatedBy); 
            ViewData["UploadedById"] = new SelectList(_context.FacultyDetails, "ID", "ID", assignmentMaterial.UploadedById);
            return View(assignmentMaterial);
        }

        // POST: AssignmentMaterials/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AssignmentMaterial assignmentMaterial)
        {
            if (ModelState.IsValid)
            {
                assignmentMaterial.ID = Guid.NewGuid();
                assignmentMaterial.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                assignmentMaterial.CreatedDate = DateTime.Now;
                assignmentMaterial.ModifiedTimes = 0;
                assignmentMaterial.RecordStatus = RecordStatus.Active;
                assignmentMaterial.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                assignmentMaterial.UploadedById = Guid.Parse(userManager.GetUserId(User));

                if (assignmentMaterial.AssignmentFile != null)
                {
                    var file = assignmentMaterial.AssignmentFile;
                    var Id = Guid.NewGuid();

                    var fileName = Id + Path.GetExtension(file.FileName);
                    var Files = "";

                    var requestModel = new FileUploadRequestViewModel()
                    {
                        id = Id,
                        AccessKey = _bucketSetting.AccessKey,
                        SecretKey = _bucketSetting.SecretKey,
                        ObjectKey = fileName,
                        BucketName = _bucketSetting.BucketName + "/AssignmentDoc",
                        FilePath = Files,
                        EndPoint = _bucketSetting.EndPoint,
                        FileStream = assignmentMaterial.AssignmentFile.OpenReadStream()
                    };


                    var result = mapper.Map<FileResponseViewModel>(bucketService.UploadFTP(mapper.Map<FileUploadRequestDTO>(requestModel)));
                    if (result.success)
                    {
                        assignmentMaterial.FileLocation = _bucketSetting.URL + "/AssignmentDoc/" + result.fileName; 
                        assignmentMaterial.FileFormat = assignmentMaterial.AssignmentFile.ContentType;
                    }

                }
                assignmentRepository.Add(assignmentMaterial);
                await unitOfWork.SaveAsync();

                return RedirectToAction("Assignment", "FacultyAccess");
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", assignmentMaterial.CreatedBy); 
            ViewData["UploadedById"] = new SelectList(_context.FacultyDetails, "ID", "ID", assignmentMaterial.UploadedById);
            return View(assignmentMaterial);
        }

        // GET: AssignmentMaterials/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignmentMaterial = await _context.AssignmentMaterial
                .Include(a => a.ApplicationUser)
                .Include(a => a.CourseSubject)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (assignmentMaterial == null)
            {
                return NotFound();
            }

            return View(assignmentMaterial);
        }

        // POST: AssignmentMaterials/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var assignmentMaterial = await _context.AssignmentMaterial.FindAsync(id);
            _context.AssignmentMaterial.Remove(assignmentMaterial);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AssignmentMaterialExists(Guid id)
        {
            return _context.AssignmentMaterial.Any(e => e.ID == id);
        }
    }
}
