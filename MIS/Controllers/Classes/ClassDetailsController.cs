﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Helper;
using MIS_Entity.Entity.Classes;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using MIS_Entity.Enum;
using MIS_Repository.Interfaces;

namespace MIS.Controllers.Classes
{
    [ModuleInfo(ModuleName = "ClassDetails", Url = "/ClassDetails")]
    public class ClassDetailsController : BaseAdminController
    {
        private readonly ISMTDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IClassRepository classRepository;
        private readonly ICourseRepository courseRepository;
        private readonly IUnitOfWork unitOfWork;

        public ClassDetailsController(ISMTDbContext context,
            UserManager<ApplicationUser> userManager,
            IClassRepository classRepository,
            ICourseRepository courseRepository,
            IUnitOfWork unitOfWork)
        {
            _context = context;
            this.userManager = userManager;
            this.classRepository = classRepository;
            this.courseRepository = courseRepository;
            this.unitOfWork = unitOfWork;
        }

        // GET: ClassDetails
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> GetClassesIndex(ClassDetails classDetails)
        {

            var classes = await classRepository.GetCourseBatchName().ToListAsync();

            return View(classes);
        }

        // GET: ClassDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classDetails = await _context.ClassDetails
                .Include(c => c.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (classDetails == null)
            {
                return NotFound();
            }

            return View(classDetails);
        }

        // GET: ClassDetails/Create
        public IActionResult Create()
        {
            var classes = new ClassDetails();
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            ViewBag.WeekDays = Enum.GetValues(typeof(WeekDay)).Cast<WeekDay>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewBag.Course = _context.CourseDetails.Select(v => new SelectListItem
            {
                Text = v.CourseName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();

            ViewBag.Batch = _context.BatchDetails.Select(v => new SelectListItem
            {
                Text = v.BatchName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            classes.ClassStartTime = DateTime.Now;
            classes.ClassEndTime = DateTime.Now.AddHours(1);
            return View(classes);
        }
        public IActionResult GetSubjectByCourse(Guid Id)
        {
            var subjectID = _context.SubjectByCourse.Where(x => x.CourseId == Id).Select(x => x.SubjectId).ToList();
            var data = _context.CourseSubject.Where(x => subjectID.Contains(x.ID)).Select(v => new SelectListItem
            {
                Text = v.SubjectName.ToString(),
                Value = ((Guid)v.ID).ToString()
            }).ToList();
            return Ok(data);
        }

        // POST: ClassDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClassDetails classDetails)
        {
            if (ModelState.IsValid)
            {
                classDetails.ID = Guid.NewGuid();
                classDetails.CreatedBy = Guid.Parse(userManager.GetUserId(User));
                classDetails.CreatedDate = DateTime.Now;
                classDetails.ModifiedTimes = 0;
                classDetails.RecordStatus = RecordStatus.Active;

                classRepository.Add(classDetails);

                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", classDetails.CreatedBy);
            return View(classDetails);
        }

        // GET: ClassDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classDetails = classRepository.Get(id);
            if (classDetails == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", classDetails.CreatedBy);
            ViewBag.ID = id;
            ViewBag.CreatedBy = classDetails.CreatedBy;
            ViewBag.CreatedDate = classDetails.CreatedDate;
            return View(classDetails);
        }

        // POST: ClassDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("CourseID,BatchID,BatchYear,WeekDays,ClassStartTime,ClassEndTime,ID,CreatedDate,CreatedBy")] ClassDetails classDetails)
        {
            if (id != classDetails.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    classDetails.ModifiedBy = Guid.Parse(userManager.GetUserId(User));
                    classDetails.ModifiedDate = DateTime.Now;
                    classDetails.ModifiedTimes = classDetails.ModifiedTimes + 1;

                    classRepository.Update(classDetails);
                    await unitOfWork.SaveAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClassDetailsExists(classDetails.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", classDetails.CreatedBy);
            return View(classDetails);
        }

        // GET: ClassDetails/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classDetails = await _context.ClassDetails
                .Include(c => c.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (classDetails == null)
            {
                return NotFound();
            }

            return View(classDetails);
        }

        // POST: ClassDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var classDetails = classRepository.Get(id);
            classRepository.Remove(classDetails);
            await unitOfWork.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClassDetailsExists(Guid id)
        {
            return _context.ClassDetails.Any(e => e.ID == id);
        }
    }
}
