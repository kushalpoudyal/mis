﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS_Entity.Entity;
using MIS_Entity.Enum;

namespace MIS.Controllers.Notice
{
    [Authorize]
    [ModuleInfo(ModuleName = "Recommendations", Url = "/Recommendations")]
    public class RecommendationsController : Controller
    {
        private readonly ISMTDbContext _context;

        public RecommendationsController(ISMTDbContext context)
        {
            _context = context;
        }

        // GET: Recommendations
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> GetAllNotices()
        {
            var iSMTDbContext = _context.Recommendations.Include(n => n.ApplicationUser);
            return View(await iSMTDbContext.ToListAsync());
        }
        // GET: Recommendations/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Recommendations = await _context.Recommendations
                .Include(n => n.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (Recommendations == null)
            {
                return NotFound();
            }

            return View(Recommendations);
        }
        [Authorize(Roles = "Admin")]
        // GET: Recommendations/Create  
        public IActionResult Create(Guid? StudentId)
        {
            var CurrId = Guid.Parse(User.GetUserId());
            var model = new Recommendations()
            {
                ID = Guid.NewGuid(),
                ModifiedBy = CurrId,
                CreatedBy = CurrId,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                ModifiedTimes = 0
            };
            if (StudentId != null)
            {
                ViewBag.StudentId = _context.StudentDetails.Where(x => x.ID == StudentId).Select(v => new SelectListItem
                {
                    Text = v.StudentName.ToString(),
                    Value = ((Guid)v.ID).ToString()
                }).ToList();
            }
            else
            {
                ViewBag.StudentId = _context.StudentDetails.Select(v => new SelectListItem
                {
                    Text = v.StudentName.ToString(),
                    Value = ((Guid)v.ID).ToString()
                }).ToList();
            }

            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            return View(model);
        }

        // POST: Recommendations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Recommendations Recommendations)
        {
            if (ModelState.IsValid)
            {
                Recommendations.ID = Guid.NewGuid();
                Recommendations.CreatedBy = Guid.Parse(User.GetUserId());
                Recommendations.CreatedDate = DateTime.UtcNow;
                Recommendations.ModifiedBy = Guid.Parse(User.GetUserId());
                Recommendations.ModifiedDate = DateTime.UtcNow;
                Recommendations.ModifiedTimes = 0;
                Recommendations.RecordStatus = MIS_Entity.Entity.Enum.RecordStatus.Active;

                _context.Add(Recommendations);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", Recommendations.CreatedBy);
            return View(Recommendations);
        }
        [Authorize(Roles = "Admin")]
        // GET: Recommendations/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Recommendations = await _context.Recommendations.FindAsync(id);
            ViewBag.Priority = Enum.GetValues(typeof(Priority)).Cast<Priority>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            if (Recommendations == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", Recommendations.CreatedBy);
            return View(Recommendations);
        }

        // POST: Recommendations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Title,Description,Priority,ExpiryDate,ID,CreatedBy,ModifiedBy,CreatedDate,ModifiedTimes,ModifiedDate,RecordStatus")] Recommendations Recommendations)
        {
            if (id != Recommendations.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    Recommendations.ModifiedBy = Guid.Parse(User.GetUserId());
                    Recommendations.ModifiedDate = DateTime.UtcNow;
                    Recommendations.ModifiedTimes = Recommendations.ModifiedTimes + 1;
                    _context.Update(Recommendations);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RecommendationsExists(Recommendations.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", Recommendations.CreatedBy);
            return View(Recommendations);
        }
        [Authorize(Roles = "Admin")]
        // GET: Recommendations/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Recommendations = await _context.Recommendations
                .Include(n => n.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (Recommendations == null)
            {
                return NotFound();
            }

            return View(Recommendations);
        }

        // POST: Recommendations/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var Recommendations = await _context.Recommendations.FindAsync(id);
            _context.Recommendations.Remove(Recommendations);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RecommendationsExists(Guid id)
        {
            return _context.Recommendations.Any(e => e.ID == id);
        }
    }
}
