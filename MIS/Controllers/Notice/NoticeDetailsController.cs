﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Extensions;
using MIS.Helper;
using MIS_Entity.Entity;
using MIS_Entity.Enum;

namespace MIS.Controllers.Notice
{
    [Authorize]
    [ModuleInfo(ModuleName = "NoticeDetails", Url = "/NoticeDetails")]
    public class NoticeDetailsController : Controller
    {
        private readonly ISMTDbContext _context;

        public NoticeDetailsController(ISMTDbContext context)
        {
            _context = context;
        }

        // GET: NoticeDetails
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> GetAllNotices()
        {
            var iSMTDbContext = _context.NoticeDetails.Include(n => n.ApplicationUser);
            return View(await iSMTDbContext.ToListAsync());
        }
        // GET: NoticeDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var noticeDetails = await _context.NoticeDetails
                .Include(n => n.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (noticeDetails == null)
            {
                return NotFound();
            }

            return View(noticeDetails);
        }
        [Authorize(Roles = "Admin")]
        // GET: NoticeDetails/Create
        public IActionResult Create()
        {
            ViewBag.Priority = Enum.GetValues(typeof(Priority)).Cast<Priority>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: NoticeDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(NoticeDetails noticeDetails)
        {
            if (ModelState.IsValid)
            {
                noticeDetails.ID = Guid.NewGuid();
                noticeDetails.CreatedBy = Guid.Parse(User.GetUserId());
                noticeDetails.CreatedDate = DateTime.UtcNow;
                noticeDetails.ModifiedBy = Guid.Parse(User.GetUserId());
                noticeDetails.ModifiedDate = DateTime.UtcNow;
                noticeDetails.ModifiedTimes = 0;
                noticeDetails.RecordStatus = MIS_Entity.Entity.Enum.RecordStatus.Active;

                _context.Add(noticeDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", noticeDetails.CreatedBy);
            return View(noticeDetails);
        }
        [Authorize(Roles = "Admin")]
        // GET: NoticeDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var noticeDetails = await _context.NoticeDetails.FindAsync(id);
            ViewBag.Priority = Enum.GetValues(typeof(Priority)).Cast<Priority>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            if (noticeDetails == null)
            {
                return NotFound();
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", noticeDetails.CreatedBy);
            return View(noticeDetails);
        }

        // POST: NoticeDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Title,Description,Priority,ExpiryDate,ID,CreatedBy,ModifiedBy,CreatedDate,ModifiedTimes,ModifiedDate,RecordStatus")] NoticeDetails noticeDetails)
        {
            if (id != noticeDetails.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    noticeDetails.ModifiedBy = Guid.Parse(User.GetUserId());
                    noticeDetails.ModifiedDate = DateTime.UtcNow;
                    noticeDetails.ModifiedTimes = noticeDetails.ModifiedTimes + 1;
                    _context.Update(noticeDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NoticeDetailsExists(noticeDetails.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatedBy"] = new SelectList(_context.Users, "Id", "Id", noticeDetails.CreatedBy);
            return View(noticeDetails);
        }
        [Authorize(Roles = "Admin")]
        // GET: NoticeDetails/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var noticeDetails = await _context.NoticeDetails
                .Include(n => n.ApplicationUser)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (noticeDetails == null)
            {
                return NotFound();
            }

            return View(noticeDetails);
        }

        // POST: NoticeDetails/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var noticeDetails = await _context.NoticeDetails.FindAsync(id);
            _context.NoticeDetails.Remove(noticeDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NoticeDetailsExists(Guid id)
        {
            return _context.NoticeDetails.Any(e => e.ID == id);
        }
    }
}
