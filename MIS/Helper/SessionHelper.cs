﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS_Entity.Entity.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MIS.Helper
{
    public class SessionHelper
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly ISMTDbContext _context;
        public SessionHelper(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            ISMTDbContext _context)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this._context = _context;
        }
        public async Task<List<RoleDetails>> GetRoles(ClaimsPrincipal UserC)
        {
            var usrDetails = await this.userManager.GetUserAsync(UserC);
            var usrRole = await this.userManager.GetRolesAsync(usrDetails);

            var roleDetail = await this.roleManager.FindByNameAsync(usrRole.FirstOrDefault());
            var details = await _context.RoleDetails.Where(x => x.IsView && x.RoleId==roleDetail.Id).ToListAsync();
            return details;
        }
    }
}
