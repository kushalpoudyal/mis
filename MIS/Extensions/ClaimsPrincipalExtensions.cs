﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using MIS_Entity.Entity.Enum;

namespace MIS.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetUserId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            return GetClaimValueByClaimType(principal, ClaimTypes.NameIdentifier);
        }

        private static string GetClaimValueByClaimType(ClaimsPrincipal principal, string claimType)
        {
            return principal.FindFirst(claimType)?.Value;
        }

        public static string GetStudentId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "StudentId");
        }
        public static string GetRoleName(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "Role");
        }
        public static string GetCourseId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "StudentId");
        }
        public static string GetActiveUserCourseId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "CourseId");
        }
        public static string GetUserBatchId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "BatchId");
        }

        public static string GetActiveUserAssignmentId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "AssignmentId");
        }
        public static string GetFullname(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "Name");
        }
       
        public static string GetEmail(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, ClaimTypes.Email);
        }
        public static string GetProfileImagePath(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "ProfileImagePath");
        }
        public static string GetFacultySubjects(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return GetClaimValueByClaimType(principal, "SubjectIDs");
        }
        public static bool IsStudent(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return principal.IsInRole(Roles.Student.ToString());
        }
        public static bool IsStaff(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return principal.IsInRole(Roles.Faculty.ToString());
        }
        public static bool IsAdmin(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return principal.IsInRole("Admin") || principal.IsInRole("SuperAdmin");
        }
        public static bool IsSuperAdmin(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));
            return principal.IsInRole("SuperAdmin");
        }
    }
}
