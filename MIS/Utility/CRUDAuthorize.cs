﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using MIS.Extensions;
using MIS.Helper;
using MIS_Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
namespace MIS.Utility
{

    public class CRUDAuthorize : ActionFilterAttribute
    {

        public string ModuleName { get; set; }
        public CRUDAction Action { get; set; }


        public override async void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var User = filterContext.HttpContext.User;

            var iroleservice = (IRoleDetailsRepository)filterContext.HttpContext.RequestServices.GetService(typeof(IRoleDetailsRepository));
            var _data = (ClaimsIdentity)User.Identity;

            if (!User.Identity.IsAuthenticated)
            {

                base.OnActionExecuting(filterContext);

            }
            else
            {

                if (User.IsAdmin() || User.IsSuperAdmin() || User.IsStudent() || User.IsStaff())
                {
                    return;
                }

                var jsonValue = await iroleservice.GetUserRoleDetails(Guid.Parse(User.GetUserId()));

                switch (Action)
                {
                    case CRUDAction.View:
                        if (ModuleName != string.Empty)
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsView).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        else
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsView).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        break;
                    case CRUDAction.Create:
                        if (ModuleName != string.Empty)
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsCreate).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        else
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsCreate).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        break;
                    case CRUDAction.Edit:
                        if (ModuleName != string.Empty)
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsEdit).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        else
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsEdit).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        break;
                    case CRUDAction.Delete:
                        if (ModuleName != string.Empty)
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsDelete).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        else
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsDelete).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        break;
                    case CRUDAction.Authorized:
                        if (ModuleName != string.Empty)
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsAuthorize).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        else
                        {
                            if (!(jsonValue.Where(x => x.ModuleName == ModuleName && x.IsDelete).Count() > 0))
                            {
                                base.OnActionExecuting(filterContext); break;
                            }
                        }
                        break;
                    default:
                        base.OnActionExecuting(filterContext); break;

                }

            }
        }


    }
}
