﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using MIS.Extensions;
using MIS.Helper;
using MIS_Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MIS.Utility
{
    public static class AuthorizeViewHelper
    {
        public async static Task<bool> IsAuthorize(string ModuleName, string AC, HttpContext context)
        {
            var action = AC.ToEnum<CRUDAction>();
            var _data = (ClaimsIdentity)context.User.Identity;
            var claim = _data.Claims.Where(c => c.Type.Contains("userdata")).FirstOrDefault();
            var adminRole = _data.Claims.Where(c => c.Type.Contains("role")).FirstOrDefault();
            var UserId = Guid.Parse(context.User.GetUserId());
            if (adminRole.Value.ToString() == "SuperAdmin")
            {
                return true;
            }

            //List<RoleDetails> jsonValue = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RoleDetails>>(claim.Value);
            //var ModuleInfo = AttributeInfo.GetModuleInfo((Type)HttpContext.Current.Request.RequestContext.RouteData.Values["controller"]);
            try
            {
                var iroleservice = (IRoleDetailsRepository)context.RequestServices.GetService(typeof(IRoleDetailsRepository));
                var jsonValue = await iroleservice.GetUserRoleDetails(UserId);


                switch (action)
                {
                    case CRUDAction.View:
                        if (ModuleName != string.Empty)
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsView).Count() > 0))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsView).Count() > 0))
                            {
                                return true;
                            }
                        }
                        break;
                    case CRUDAction.Create:
                        if (ModuleName != string.Empty)
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsCreate).Count() > 0))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsCreate).Count() > 0))
                            {
                                return true;
                            }
                        }
                        break;
                    case CRUDAction.Edit:
                        if (ModuleName != string.Empty)
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsEdit).Count() > 0))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsEdit).Count() > 0))
                            {
                                return true;
                            }
                        }
                        break;
                    case CRUDAction.Delete:
                        if (ModuleName != string.Empty)
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsDelete).Count() > 0))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsDelete).Count() > 0))
                            {
                                return true;
                            }
                        }
                        break;
                    case CRUDAction.Download:
                        if (ModuleName != string.Empty)
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsDownload).Count() > 0))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsDownload).Count() > 0))
                            {
                                return true;
                            }
                        }
                        break;
                    case CRUDAction.Authorized:
                        if (ModuleName != string.Empty)
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsAuthorize).Count() > 0))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if ((jsonValue.Where(x => x.ModuleName == ModuleName && x.IsAuthorize).Count() > 0))
                            {
                                return true;
                            }
                        }
                        break;
                    default:
                        return false;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return false;
        }


    }
}
