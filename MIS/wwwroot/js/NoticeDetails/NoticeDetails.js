﻿if (!window.app) {
    window.app = {}
}

app.Notice = (function ($) {
    var dataSource = app.NoticeDataSource;
    var init = function () {
        $('#btnAddNotice').click(createNotice);
        getNotice();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitNotice);
        $('.btnEdit').click(editNotice);
        $('.btnDelete').click(deleteNotice);
    }

    var createNotice = function () {
        var url = "/NoticeDetails/Create";
        dataSource.createNotice(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editNotice = function () {

        var id = $(this).attr('data-val');
        var url = "/NoticeDetails/Edit?id=" + id;
        dataSource.editNotice(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteNotice = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/NoticeDetails/Delete?id=" + id;
        dataSource.deleteNotice(url).done(function (data) {
            getNotice();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitNotice = function () {
        $('#NoticeForm').submit();
        //$('#btnCancel').click();
    };

    var getNotice = function () {
        var url = "/NoticeDetails/GetAllNotices";
        dataSource.getNotice(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            InitDataTable();
            contentAddons();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.Notice.init);