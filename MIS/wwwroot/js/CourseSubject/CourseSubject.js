﻿if (!window.app) {
    window.app = {}
}

app.CourseSubject = (function ($) {
    var dataSource = app.CourseSubjectDataSource;
    var init = function () {
        $('#btnAddNewCourse').click(createCourse);
        getCourse();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitCourse);
        $('.btnEdit').click(editCourse);
        $('.btnDelete').click(deleteCourse);
    }

    var createCourse = function () {
        var url = "/CourseSubject/Create";
        dataSource.createCourse(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editCourse = function () {

        var id = $(this).attr('data-val');
        var url = "/CourseSubject/Edit?id=" + id;
        dataSource.editCourse(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteCourse = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/CourseSubject/Delete?id=" + id;
        dataSource.deleteCourse(url).done(function (data) {
            getCourse();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitCourse = function () {
        $('#courseForm').submit();
        //$('#btnCancel').click();
    };

    var getCourse = function () {
        var url = "/CourseSubject/GetCourseIndex";
        dataSource.getCourse(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            InitDataTable();
            contentAddons();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.CourseSubject.init);