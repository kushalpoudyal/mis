﻿if (!window.app) {
    window.app = {}
}

app.Batch = (function ($) {
    var dataSource = app.BatchDataSource;
    var init = function () {
        $('#btnAddNewBatch').click(createBatch);
        getBatch();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitBatch);
        $('.btnEdit').click(editBatch);
        $('.btnDelete').click(deleteBatch);
    }

    var createBatch = function () {
        var url = "/BatchDetails/Create";
        dataSource.createBatch(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editBatch = function () {

        var id = $(this).attr('data-val');
        var url = "/BatchDetails/Edit?id=" + id;
        dataSource.editBatch(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteBatch = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/BatchDetails/Delete?id=" + id;
        dataSource.deleteBatch(url).done(function (data) {
            getBatch();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitBatch = function () {
        $('#batchForm').submit();
        //$('#btnCancel').click();
    };

    var getBatch = function () {
        var url = "/BatchDetails/GetBatchIndex";
        dataSource.getBatch(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            InitDataTable();
            contentAddons();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.Batch.init);