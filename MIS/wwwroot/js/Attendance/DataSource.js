﻿if (!window.app) {
    window.app = {}
}

app.AttendanceDataSource = (function ($) {

    var createAttendance = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var getAttendance = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };



    return {
        createAttendance: createAttendance,
        getAttendance: getAttendance
    };

})(jQuery);