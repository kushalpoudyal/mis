﻿if (!window.app) {
    window.app = {}
}

app.Attendance = (function ($) {
    var dataSource = app.AttendanceDataSource;
    var init = function () {
        $('#btnAttendance').click(createAttendance);
        getAttendance();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitAttendance);
        $('.btnEdit').click(editAttendance);
        $('.btnDelete').click(deleteAttendance);
    }

    var createAttendance = function () {
        var url = "/Common/AttendancePage";
        dataSource.createAttendance(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };




    var submitAttendance = function () {
        $('#batchForm').submit();
        //$('#btnCancel').click();
    };

    var getAttendance = function () {
        var url = "/Common/GetAttendancePage";
        dataSource.getAttendance(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            InitDataTable();
            contentAddons();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.Attendance.init);