﻿if (!window.app) {
    window.app = {}
}

app.Library = (function ($) {
    var dataSource = app.LibraryDataSource;
    var init = function () {
        $('#inputSearch').keyup(SearchBook);

    };

    var SearchBook = function () {
        var nput = $('#inputSearch').val();
        if (nput) {
            dataSource.getSearchBook(nput).done(function (data) {
                $('.card-body').empty();
                $('.card-body').html(data);
            });
        }

    };
    return { init: init };
})(jQuery);

jQuery(app.Library.init);