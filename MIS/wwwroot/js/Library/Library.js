﻿if (!window.app) {
    window.app = {}
}

app.Library = (function ($) {
    var dataSource = app.LibraryDataSource;
    var init = function () {
        $('#btnAddNewBook').click(createLibrary);
        $('#btnRequestBook').click(RequestBook);
        getLibrary();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitLibrary);
        $('.btnEdit').click(editLibrary);
        $('.btnDelete').click(deleteLibrary);

    }

    var createLibrary = function () {
        var url = "/Library/Create";
        dataSource.createLibrary(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");
            $("#libraryForm").find("select[name='BookTypes']").on('change', function () {
                if ($(this).val() != "1") {
                    $("#libraryForm").find('#bookDetails').show();
                    $("#libraryForm").find('#bookDetails').find("input[name='BookFile']").attr("required", true);
                    $("#libraryForm").find("input[name='BookCount']").parent().hide();
                }
                else {
                    $("#libraryForm").find('#bookDetails').hide();
                    $("#libraryForm").find('#bookDetails').find("input[name='BookFile']").removeAttr("required");
                    $("#libraryForm").find("input[name='BookCount']").parent().show();
                }
            })

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var uploadBook = function (e) {

        var file = e.target.files[0];
        var bookId = $("#bookIdHidden").val();
        var formData = new FormData();
        formData.append("bookFile", file);
        formData.append("BookId", bookId);



        $.ajax({
            type: "POST",
            url: "/Library/UploadBook",
            contentType: false,
            processData: false,
            data: formData,
            success: function (response) {
                alert("Succesfully Uploaded!");
            },
            error: function (error) {
                alert("Count not upload the chosen file. Please try again!");
            }
        });
    }
    var RequestBook = function () {
        var url = "/BookRequest/Create";
        dataSource.createLibrary(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");
            $('#btnSubmit').click(submitRequest);
        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editLibrary = function () {

        var id = $(this).attr('data-val');
        var url = "/Library/Edit?id=" + id;
        dataSource.editLibrary(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteLibrary = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/Library/Delete?id=" + id;
        dataSource.deleteLibrary(url).done(function (data) {
            getLibrary();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitLibrary = function () {
        $('#libraryForm').submit();
        //$('#btnCancel').click();
    };

    var submitRequest = function () {
        $('#RequestForm').submit();
        //$('#btnCancel').click();
    };

    var getLibrary = function () {
        var url = "/Library/GetLibraryIndex";
        dataSource.getLibrary(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            contentAddons();
            InitDataTable();
            $(".btnUpload").on("click", function () {
                var id = $(this).attr('data-val')
                $('#bookIdHidden').val(id);
                $("#file").trigger("click");
            });
            $("#file").on("change", uploadBook);
            $('.bookNameEvt').click(getDetails);

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };
    var getDetails = function () {
        var id = $(this).attr('data-val');
        var url = "/Library/Details?id=" + id;
        dataSource.createLibrary(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };


    return { init: init };
})(jQuery);

jQuery(app.Library.init);