﻿if (!window.app) {
    window.app = {}
}

app.Faculty = (function ($) {
    var dataSource = app.FacultyDataSource;
    var init = function () {
        $('#btnAddNewFaculty').click(createFaculty);
        getFaculty();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitFaculty);
        $('.btnEdit').click(editFaculty);
        $('.btnDelete').click(deleteFaculty);
    }

    var createFaculty = function () {
        var url = "/FacultyDetails/Create";
        dataSource.createFaculty(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editFaculty = function () {

        var id = $(this).attr('data-val');
        var url = "/FacultyDetails/Edit?id=" + id;
        dataSource.editFaculty(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteFaculty = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/FacultyDetails/Delete?id=" + id;
        dataSource.deleteFaculty(url).done(function (data) {
            getFaculty();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitFaculty = function () {
        $('#facultyForm').submit();
        //$('#btnCancel').click();
    };

    var getFaculty = function () {
        var url = "/FacultyDetails/GetFacultyIndex";
        dataSource.getFaculty(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            contentAddons();
            InitDataTable();
            

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return {
        init: init,
        contentAddons: contentAddons
    };
})(jQuery);

jQuery(app.Faculty.init);