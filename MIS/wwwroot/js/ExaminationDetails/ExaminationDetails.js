﻿if (!window.app) {
    window.app = {}
}

app.ExaminationDetails = (function ($) {
    var dataSource = app.ExaminationDetailsDataSource;
    var init = function () {
        $('#btnAddExaminationDetails').click(createExaminationDetails);
        getExaminationDetails();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitExaminationDetails);
        $('.btnEdit').click(editExaminationDetails);
        $('.btnDelete').click(deleteExaminationDetails);
    }

    var createExaminationDetails = function () {
        var url = "/ExaminationDetails/Create";
        dataSource.createExaminationDetails(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editExaminationDetails = function () {

        var id = $(this).attr('data-val');
        var url = "/ExaminationDetails/Edit?id=" + id;
        dataSource.editExaminationDetails(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteExaminationDetails = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/ExaminationDetails/Delete?id=" + id;
        dataSource.deleteExaminationDetails(url).done(function (data) {
            getExaminationDetails();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitExaminationDetails = function () {
        $('#ExaminationDetailsForm').submit();
        //$('#btnCancel').click();
    };

    var getExaminationDetails = function () {
        var url = "/ExaminationDetails/GetAllExamDetails";
        dataSource.getExaminationDetails(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            InitDataTable();
            contentAddons();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.ExaminationDetails.init);