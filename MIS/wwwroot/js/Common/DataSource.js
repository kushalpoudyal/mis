﻿if (!window.app) {
    window.app = {}
}

app.DataSource = (function ($) {

    var create = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };
    var createPost = function (url,form) {
        var $d = $.Deferred();

        $.post(url, form).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var edit = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var deleteData = function (url) {
        var $d = $.Deferred();
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token
            },
        }).done(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var get = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };



    return {
        create: create,
        edit: edit,
        deleteData: deleteData,
        get: get,
        createPost: createPost
    };

})(jQuery);
