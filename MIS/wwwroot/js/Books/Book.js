﻿if (!window.app) {
    window.app = {}
}

app.BookDetails = (function ($) {
    var dataSource = app.DataSource;
    var init = function () {
        $('#btnRequestBook').click(RequestBook);
        $('.btnRequestAction').click(RequestAction);
        getBookDetails();

    };

    //var contentAddons = function () {
    //    $('#btnSubmit').click(submit);
    //    $('.btnEdit').click(edit);
    //    $('.btnDelete').click(deleteData);
    //}

    //var create = function () {
    //    var url = "/StudentDetails/Create";
    //    dataSource.create(url).done(function (data) {
    //        var innerHTM = data;
    //        var modal = $('.modal');
    //        modal.find('.modal-body').html(innerHTM);
    //        modal.modal("toggle");


    //    }).fail(function (data) {
    //        alert("Something went wrong");
    //    });
    //};

    //var edit = function () {

    //    var id = $(this).attr('data-val');
    //    var url = "/StudentDetails/Edit?id=" + id;
    //    dataSource.edit(url).done(function (data) {
    //        var innerHTM = data;
    //        var modal = $('.modal');
    //        modal.find('.modal-body').html(innerHTM);
    //        modal.modal("toggle");


    //    }).fail(function (data) {
    //        alert("Something went wrong");
    //    });
    //};

    //var deleteData = function () {

    //    var id = $(this).attr('data-val');
    //    var r = confirm("Are you Sure?");
    //    if (r == false) {
    //        return false;
    //    }

    //    var url = "/StudentDetails/Delete?id=" + id;
    //    dataSource.deleteData(url).done(function (data) {
    //        getStudentDetails();


    //    }).fail(function (data) {
    //        alert("Something went wrong");
    //    });
    //};

    //var submit = function () {
    //    $('#editorForm').submit();
    //    //$('#btnCancel').click();
    //};

    var getBookDetails = function () {
        var url = "/Books/GetBookIndex";
        dataSource.get(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            InitDataTable();
            contentAddons();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var RequestBook = function () {
        var url = "/BookRequest/Create";
        dataSource.create(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");
            $('#btnSubmit').click(submitRequest);
        }).fail(function (data) {
            alert("Something went wrong");
        });
    };


    var RequestAction = function () {
        var UserId = $(this).attr('data-val');
        var BookId = $(this).attr('data-id');

        var url = "/BookRequest/RequestAction?UserId=" + UserId + "&&BookId=" + BookId;
        dataSource.create(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");
            $('#btnSubmit').click(submitRequest);
        }).fail(function (data) {
            alert("Something went wrong");
        });
    };


    var submitRequest = function () {
        $('#RequestForm').submit();
        //$('#btnCancel').click();
    };


    return { init: init };
})(jQuery);

jQuery(app.BookDetails.init);