﻿if (!window.app) {
    window.app = {}
}

app.BookGenresDataSource = (function ($) {

    var createBookGenres = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var editBookGenres = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var deleteBookGenres = function (url) {
        var $d = $.Deferred();
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token
            },           
        }).done(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var getBookGenres = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };



    return {
        createBookGenres: createBookGenres,
        editBookGenres: editBookGenres,
        deleteBookGenres: deleteBookGenres,
        getBookGenres: getBookGenres
    };

})(jQuery);

