﻿if (!window.app) {
    window.app = {}
}

app.BookGenres = (function ($) {
    var dataSource = app.BookGenresDataSource;
    var init = function () {
        $('#btnAddNewBookGenre').click(createBookGenres);
        getBookGenres();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitBookGenres);
        $('.btnEdit').click(editBookGenres);
        $('.btnDelete').click(deleteBookGenres);
    }

    var createBookGenres = function () {
        var url = "/BookGenres/Create";
        dataSource.createBookGenres(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editBookGenres = function () {

        var id = $(this).attr('data-val');
        var url = "/BookGenres/Edit?id=" + id;
        dataSource.editBookGenres(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteBookGenres = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/BookGenres/Delete?id=" + id;
        dataSource.deleteBookGenres(url).done(function (data) {
            getBookGenres();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitBookGenres = function () {
        $('#bookGenreForm').submit();
        //$('#btnCancel').click();
    };

    var getBookGenres = function () {
        var url = "/BookGenres/GetBookGenresIndex";
        dataSource.getBookGenres(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            contentAddons();
            InitDataTable();
            

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.BookGenres.init);