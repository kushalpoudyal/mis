﻿if (!window.app) {
    window.app = {}
}

app.Assignment = (function ($) {
    var dataSource = app.AssignmentDataSource;
    var Order = 0;
    var init = function () {
        $('#btnAddNewAssignment').click(createAssignment);
        $('.btnRequestAction').click(RequestAction); 
        contentAddons();
        //InitDataTable();
    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitAssignment);
        $('.btnEdit').click(editAssignment);
        $('.btnDelete').click(deleteAssignment);
    };
    var RequestAction = function () {
        $('#modal3').modal('show');
    };

    var createAssignment = function () {
        var url = "/FacultyAccess/CreateAssignment";
        dataSource.createAssignment(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");
            //modal.find('#selectSubjects').click(getSubjects);
            contentAddons(); 
        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editAssignment = function () {

        var id = $(this).attr('data-val');
        var url = "/AssignmentMaterials/Edit?id=" + id;
        dataSource.editAssignment(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteAssignment = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/AssignmentMaterials/Delete?id=" + id;
        dataSource.deleteAssignment(url).done(function (data) {
            getAssignment();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitAssignment = function () {
        $('#AssignmentForm').submit();
        //$('#btnCancel').click();
    };

    var getAssignment = function () {
        var url = "/FacultyAccess/Assignment";
        dataSource.getAssignment(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            contentAddons();
            InitDataTable();
           

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };
    var getSubjects = function () {
        var url = "/AssignmentSubject/GetAllSubjects";
        dataSource.getAssignment(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal2');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");
            $('#modal2 input[type="checkbox"]').on('change', GetSelectedSubject);

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };


    var GetSelectedSubject = function () {
        var id = $(this).val();
        var evt = this;
        if ($(this).is(':checked')) {

            var name = $(this).next().val();
            var code = $(this).next().next().val();
            var credit = $(this).next().next().next().next().val();
            var TeacherId = $(this).parents('.option').find('select').val();
            if (!TeacherId) {
                $(this).removeAttr('checked');
                $(this).parents('.option').find('select').css('border-color', 'red');
                setTimeout(function () {
                    // reset CSS
                    $(evt).parents('.option').find('select').css('border-color', '');
                }, 500);
                return false;
            }

            var Row = `<tr data-row="0" id="row_${id}" class="datatable-row datatable-row-hover" style="left: 0px;">
                            <td class="datatable-cell-center datatable-cell datatable-cell-check" data-field="RecordID" aria-label="1">
                                <span style="width: 20px;">
                                    <label class="checkbox checkbox-single">
                                        <input type="checkbox" value="1">&nbsp;<span>
                                        </span>
                                    </label>
                                </span>
                            </td>
                            <td data-field="OrderID" aria-label="64616-103" class="datatable-cell"><span style="width: 137px;">${code}</span></td>
                            <td data-field="Country" aria-label="Brazil" class="datatable-cell"><span style="width: 137px;">${name}</span></td>
                            <td data-field="ShipAddress" aria-label="698 Oriole Pass" class="datatable-cell"><span style="width: 137px;">${credit}</span></td>
                            <td class="datatable-cell-left datatable-cell" data-field="Actions" data-autohide-disabled="false" aria-label="null">
                                <span style="overflow: visible; position: relative; width: 125px;">
                                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">
                                        <span class="svg-icon svg-icon-md">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </a>
                                </span>
                            </td>
                        </tr>`;


            var Inputs = `<input type="hidden" value="${id}" name="SubjectAssignments[${Order}].SubjectId" />
                          <input type="hidden" value="${TeacherId}" name="SubjectAssignments[${Order}].FacultyId" />`;
            $('#tableSubjectAssignments').find('tbody').append(Row);
            $('.card-body').find('#hiddenInputs').append(Inputs);
            Order++;
        }
        else {
            $('#tableSubjectAssignments').find('tbody').find('#row_' + id).remove();
        }

    };


    //};

    return { init: init };
})(jQuery);

jQuery(app.Assignment.init);