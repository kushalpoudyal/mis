﻿if (!window.app) {
    window.app = {}
}

app.StudentDetails = (function ($) {
    var dataSource = app.DataSource;
    var init = function () {
        getStudentObservation();
        $('#studentObservation').submit(createObservation);
        $('#SuspendCheck').change(checkSuspend);
    };

    var contentAddons = function () {
        $('#btnSubmit').click(submit);
        $('.btnEdit').click(edit);
        $('.btnDelete').click(deleteData);
    }

    var checkSuspend = function (e) {
        if ($(this).is(':checked')) {
            $('input[name="HasSuspended"]').val('true');
        }
    };

    var createObservation = function (e) {
        e.preventDefault();
        var url = "/StudentDetails/CreateObservation";
        var $form = $(this).serialize();
        exampleModalLabel
        dataSource.createPost(url, $form).done(function (data) {
            getStudentObservation();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };
    var progress = function () {
        $('.btn').prop('disabled', true);

    };

    var edit = function () {

        var id = $(this).attr('data-val');
        var url = "/StudentDetails/Edit?id=" + id;
        var label = "Edit Student";
        dataSource.edit(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            $("#exampleModalLabel").text(label);

            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteData = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/StudentDetails/Delete?id=" + id;
        dataSource.deleteData(url).done(function (data) {
            getStudentDetails();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submit = function () {
        $('#editorForm').submit();
        //$('#btnCancel').click();
    };
    var getStudentObservation = function () {
        var StudentId = $('#studentObservation').find('input[name="StudentId"]').val();
        var url = "/StudentDetails/GetStudentObservation?StudentId=" + StudentId;
        dataSource.get(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#observationDisplay');
            modal.html(innerHTM);


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };
    var getStudentObservation = function () {
        var StudentId = $('#studentObservation').find('input[name="StudentId"]').val();
        var url = "/StudentDetails/GetStudentFinancialHistory?StudentId=" + StudentId;
        dataSource.get(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#financeDisplay');
            modal.html(innerHTM);


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };




    return { init: init };
})(jQuery);

jQuery(app.StudentDetails.init);