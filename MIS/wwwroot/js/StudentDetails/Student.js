﻿if (!window.app) {
    window.app = {}
}

app.StudentDetails = (function ($) {
    var dataSource = app.DataSource;
    var init = function () {
        $('#btnAddNewStudent').click(create);
        getStudentDetails();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submit);
        $('.btnEdit').click(edit);
        $('.btnDelete').click(deleteData);
    }

    var create = function () {
        var url = "/StudentDetails/Create";
        var label = "Add New Student";
        exampleModalLabel
        dataSource.create(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            $("#exampleModalLabel").text(label);

            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };
   
    var edit = function () {

        var id = $(this).attr('data-val');
        var url = "/StudentDetails/Edit?id=" + id;
        var label = "Edit Student";
        dataSource.edit(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            $("#exampleModalLabel").text(label);

            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteData = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/StudentDetails/Delete?id=" + id;
        dataSource.deleteData(url).done(function (data) {
            getStudentDetails();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submit = function () {
        $('#editorForm').submit();
        //$('#btnCancel').click();
    };

    var getStudentDetails = function () {
        var url = "/StudentDetails/GetStudentIndex";
        dataSource.get(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            contentAddons();
            InitDataTable();
            

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.StudentDetails.init);