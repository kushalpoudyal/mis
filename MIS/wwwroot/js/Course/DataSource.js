﻿if (!window.app) {
    window.app = {}
}

app.CourseDataSource = (function ($) {

    var createCourse = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var editCourse = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var deleteCourse = function (url) {
        var $d = $.Deferred();
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token
            },
        }).done(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var getCourse = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };



    return {
        createCourse: createCourse,
        editCourse: editCourse,
        deleteCourse: deleteCourse,
        getCourse: getCourse
    };

})(jQuery);