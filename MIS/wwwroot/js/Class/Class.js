﻿if (!window.app) {
    window.app = {}
}

app.Class = (function ($) {
    var dataSource = app.ClassesDataSource;
    var init = function () {
        $('#btnAddNewClass').click(createClass);
        getClasses();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitClass);
        $('.btnEdit').click(editClass);
        $('.btnDelete').click(deleteClass);
        $('#CourseID').on('change', GetSubjects);
    }
    var GetSubjects = function () {
        var Id = $(this).val();
        var list = '';
        debugger;
        url = "/ClassDetails/GetSubjectByCourse/?Id=" + Id;
        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json; charset=utf-8'
        }).done(function (response) {
            response.forEach(myFunction);
            $('#SubjectID').append(list);
        }).fail(function (response) {
            $d.reject(response);
        });
        function myFunction(item, index) {
            list += `<option value="${item.value}">${item.text}</option>`;
        }
    }

    var createClass = function () {
        var url = "/ClassDetails/Create";
        dataSource.createClasses(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle"); 
            contentAddons();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editClass = function () {

        var id = $(this).attr('data-val');
        var url = "/ClassDetails/Edit?id=" + id;
        dataSource.editClasses(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteClass = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/ClassDetails/Delete?id=" + id;
        dataSource.deleteClasses(url).done(function (data) {
            getClass();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitClass = function () {
        $('#classForm').submit();
        //$('#btnCancel').click();
    };

    var getClasses = function () {
        var url = "/ClassDetails/GetClassesIndex";
        dataSource.getClasses(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            InitDataTable();
            contentAddons();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.Class.init);