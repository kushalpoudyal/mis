﻿if (!window.app) {
    window.app = {}
}

app.RecommendationsDataSource = (function ($) {

    var createRecommendations = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var editRecommendations = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var deleteRecommendations = function (url) {
        var $d = $.Deferred();
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token
            },
        }).done(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var getRecommendations = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };



    return {
        createRecommendations: createRecommendations,
        editRecommendations: editRecommendations,
        deleteRecommendations: deleteRecommendations,
        getRecommendations: getRecommendations
    };

})(jQuery);