﻿if (!window.app) {
    window.app = {}
}

app.Recommendations = (function ($) {
    var dataSource = app.RecommendationsDataSource;
    var init = function () {
        $('#btnAddRecommendations').click(createRecommendations);
        getRecommendations();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitRecommendations);
        $('.btnEdit').click(editRecommendations);
        $('.btnDelete').click(deleteRecommendations);
    }

    var createRecommendations = function () {
        var url = "/Recommendations/Create";
        dataSource.createRecommendations(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editRecommendations = function () {

        var id = $(this).attr('data-val');
        var url = "/Recommendations/Edit?id=" + id;
        dataSource.editRecommendations(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteRecommendations = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/Recommendations/Delete?id=" + id;
        dataSource.deleteRecommendations(url).done(function (data) {
            getRecommendations();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitRecommendations = function () {
        $('#RecommendationsForm').submit();
        //$('#btnCancel').click();
    };

    var getRecommendations = function () {
        var url = "/Recommendations/GetAllRecommendationss";
        dataSource.getRecommendations(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            InitDataTable();
            contentAddons();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.Recommendations.init);