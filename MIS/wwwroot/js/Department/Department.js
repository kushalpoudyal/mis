﻿if (!window.app) {
    window.app = {}
}

app.Department = (function ($) {
    var dataSource = app.DepartmentDataSource;
    var init = function () {
        $('#btnAddNewDepartment').click(createDepartment);
        getDepartment();

    };

    var contentAddons = function () {
        $('#btnSubmit').click(submitDepartment);
        $('.btnEdit').click(editDepartment);
        $('.btnDelete').click(deleteDepartment);
    }

    var createDepartment = function () {
        var url = "/Departments/Create";
        dataSource.createDepartment(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");
            contentAddons(); 

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var editDepartment = function () {

        var id = $(this).attr('data-val');
        var url = "/Departments/Edit?id=" + id;
        dataSource.editDepartment(url).done(function (data) {
            var innerHTM = data;
            var modal = $('#modal1');
            modal.find('.modal-body').html(innerHTM);
            modal.modal("toggle");


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var deleteDepartment = function () {

        var id = $(this).attr('data-val');
        var r = confirm("Are you Sure?");
        if (r == false) {
            return false;
        }

        var url = "/Departments/Delete?id=" + id;
        dataSource.deleteDepartment(url).done(function (data) {
            getDepartment();


        }).fail(function (data) {
            alert("Something went wrong");
        });
    };

    var submitDepartment = function () {
        $('#DepartmentForm').submit();
        //$('#btnCancel').click();
    };

    var getDepartment = function () {
        var url = "/Departments/GetDepartmentIndex";
        dataSource.getDepartment(url).done(function (data) {
            var innerHTM = data;
            var modal = $('.card-body');
            modal.html(innerHTM);
            contentAddons();
            InitDataTable();

        }).fail(function (data) {
            alert("Something went wrong");
        });
    };



    return { init: init };
})(jQuery);

jQuery(app.Department.init);