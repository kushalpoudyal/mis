﻿if (!window.app) {
    window.app = {}
}

app.DepartmentDataSource = (function ($) {

    var createDepartment = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var editDepartment = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var deleteDepartment = function (url) {
        var $d = $.Deferred();
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token
            },
        }).done(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };

    var getDepartment = function (url) {
        var $d = $.Deferred();

        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };



    return {
        createDepartment: createDepartment,
        editDepartment: editDepartment,
        deleteDepartment: deleteDepartment,
        getDepartment: getDepartment
    };

})(jQuery);