﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.Services
{
    public static class CreateFile
    {
        public static async Task<bool> UploadToServer(IFormFile formFile, string folder)
        {
            try
            {
                using (var fm = new FileStream(folder, FileMode.Create))
                {
                    await formFile.CopyToAsync(fm);
                    fm.Close();

                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }


        }
    }
}
