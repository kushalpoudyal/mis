﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.Services
{
    public class DocumentServices
    {
        public async Task<bool> SaveDocToLocation(string FolderName, string FilePath, IFormFile source, string fileName)
        {
            try
            {
                double FileLength = source.Length;
                if (!Directory.Exists(Path.Combine(FilePath, FolderName)))
                {
                    Directory.CreateDirectory(Path.Combine(FilePath, FolderName));
                }
                string VideoFolder = Path.Combine(FilePath, FolderName, fileName);
                await CreateFile.UploadToServer(source, VideoFolder);

                return true;
            }
            catch
            {
                return false;
            }

        }
    }
}
