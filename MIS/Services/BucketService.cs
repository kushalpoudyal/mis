﻿using DevExpress.Data.Filtering.Helpers;
using Microsoft.AspNetCore.Http;
using MIS_Entity.DTO;
using OBS;
using OBS.Model;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace MIS.Services
{
    public class BucketService : IBucketService
    {
        public BucketService()
        {

        }

        //public FileResponseDTO UploadFTP(FileUploadRequestDTO model)
        //{
        //    var filePath = model.Endpoint + model.BucketName + "/" + model.ObjectKey;
        //    using (var stream = model.FileStream)
        //    using (var binaryReader = new BinaryReader(stream))
        //    {
        //        var fileData = binaryReader.ReadBytes((int)model.FileLength);
        //        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(filePath);
        //        request.Method = WebRequestMethods.Ftp.UploadFile;
        //        request.Credentials = new NetworkCredential(model.AccessKey, model.SecretKey);

        //        using (Stream requestStream = request.GetRequestStream())
        //        {
        //            requestStream.Write(fileData, 0, fileData.Length);
        //        }
        //        FtpWebResponse resp = (FtpWebResponse)request.GetResponse();
        //        if (resp.StatusCode == FtpStatusCode.ClosingData)
        //        {
        //            Console.WriteLine("File uploaded successfully.");
        //        }
        //        else
        //        {
        //            Console.WriteLine("Error uploading file: " + resp.StatusDescription);
        //        }

        //    }

        //    var response = new FileResponseDTO()
        //    {
        //        id = model.id,
        //        success = true,
        //        Message = "Uploaded Successfully",
        //        locationKey = filePath,
        //        fileName = model.ObjectKey
        //        //locationKey = "https://" + model.BucketName + ".obs.ap-southeast-3.myhuaweicloud.com/" + model.ObjectKey,
        //    };
        //    return response;
        //}
        public FileResponseDTO UploadFTP(FileUploadRequestDTO model)
        {
            try
            {
                var filePath = model.Endpoint + model.BucketName + "/" + model.ObjectKey;
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(filePath);
                 
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(model.AccessKey, model.SecretKey);
                using (Stream ftpStream = request.GetRequestStream())
                {
                    model.FileStream.CopyTo(ftpStream);
                }
                var response = new FileResponseDTO()
                {
                    id = model.id,
                    success = true,
                    Message = "Uploaded Successfully",
                    locationKey = filePath,
                    fileName = model.ObjectKey
                    //locationKey = "https://" + model.BucketName + ".obs.ap-southeast-3.myhuaweicloud.com/" + model.ObjectKey,
                };
                return response;
            }
            catch (Exception ex)
            {

                var response = new FileResponseDTO()
                {
                    id = model.id,
                    success = false,
                    Message = ex.ToString(),
                    locationKey = null,
                    //locationKey = "https://" + model.BucketName + ".obs.ap-southeast-3.myhuaweicloud.com/" + model.ObjectKey,
                };
                return response;
            }


        }

        public FileResponseDTO UploadBlob(FileUploadRequestDTO model)
        {

            // Create an instance of ObsClient.
            ObsClient client = new ObsClient(model.AccessKey, model.SecretKey, model.Endpoint);
            // Upload a file in asynchronous mode.
            try
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = model.BucketName,
                    ObjectKey = model.ObjectKey,
                    InputStream = model.FileStream,
                    CannedAcl = CannedAclEnum.PublicRead
                };
                PutObjectResponse resp = new PutObjectResponse();
                //await Task.Run(() => client.BeginPutObject(request, delegate (IAsyncResult ar)
                // {
                //     try
                //     {
                //         resp = client.EndPutObject(ar);
                //         return;
                //     }
                //     catch (ObsException ex)
                //     {
                //         throw ex;
                //     }
                // }, null));
                resp = client.PutObject(request);

                var response = new FileResponseDTO()
                {
                    id = model.id,
                    success = true,
                    Message = "Uploaded Successfully",
                    locationKey = resp.ObjectUrl,
                    //locationKey = "https://" + model.BucketName + ".obs.ap-southeast-3.myhuaweicloud.com/" + model.ObjectKey,
                };


                return response;

            }
            catch (ObsException ex)
            {
                return new FileResponseDTO()
                {
                    id = model.id,
                    success = false,
                    Message = $"Error:{ex.Message}",
                    locationKey = null
                };
            }
        }

        public FileResponseDTO UploadFileStream(FileUploadRequestDTO model)
        {

            // Create an instance of ObsClient.
            ObsClient client = new ObsClient(model.AccessKey, model.SecretKey, model.Endpoint);
            // Upload a file in asynchronous mode.
            try
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = model.BucketName,
                    ObjectKey = model.ObjectKey,
                    InputStream = model.FileStream,
                    CannedAcl = CannedAclEnum.PublicRead
                };
                PutObjectResponse resp = new PutObjectResponse();

                resp = client.PutObject(request);
                var response = new FileResponseDTO()
                {
                    id = model.id,
                    success = true,
                    Message = "Uploaded Successfully",
                    locationKey = resp.ObjectUrl,
                    //locationKey = "https://" + model.BucketName + ".obs.ap-southeast-3.myhuaweicloud.com/" + model.ObjectKey,
                };


                return response;

            }
            catch (ObsException ex)
            {
                return new FileResponseDTO()
                {
                    id = model.id,
                    success = false,
                    Message = $"Error:{ex.Message}",
                    locationKey = null
                };
            }
        }
        public void DeleteBlob(FileUploadRequestDTO model)
        {
            ObsClient client = new ObsClient(model.AccessKey, model.SecretKey, model.Endpoint);
            // Delete an object.
            try
            {
                DeleteObjectRequest request = new DeleteObjectRequest()
                {
                    BucketName = model.BucketName,
                    ObjectKey = model.ObjectKey,
                };
                DeleteObjectResponse response = client.DeleteObject(request);

            }
            catch (ObsException ex)
            {
                throw ex;
            }
        }
    }
    public interface IBucketService
    {
        FileResponseDTO UploadBlob(FileUploadRequestDTO model);

        void DeleteBlob(FileUploadRequestDTO model);
        FileResponseDTO UploadFileStream(FileUploadRequestDTO model);
        FileResponseDTO UploadFTP(FileUploadRequestDTO model);
    }
}
