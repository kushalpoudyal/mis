﻿

using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using MIS_Entity.ViewModel;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MIS.Services
{
    public class EmailService : IEmailService
    {
        public EmailService()
        {
            //var msg = $"Dear Test,<br/> Check your login credential for VLE: <br/><b>username</b>:kuku@gmail.com" +
            //               $"<br/><b>password</b>:123123<br/><i>Please change your password after logging in.</i>";
            // Mail_Alert("kushalpoudyal94@gmail.com", "VLE Registration", msg);
        }
        public async Task Mail_Alert(MailViewModel model)
        {

            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(model.Username));
            email.To.Add(MailboxAddress.Parse(model.ToEmail));
            email.Subject = model.Subject;
            email.Body = new TextPart(TextFormat.Html) { Text = model.Body };

            // send email
            using (var smtp = new SmtpClient())
            {
                await smtp.ConnectAsync(model.Host, 587, SecureSocketOptions.StartTlsWhenAvailable);
                await smtp.AuthenticateAsync(model.Username, model.Password);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }

        }
        public async Task<bool> Mail_Alert(string mail, string subject, string htmlmessage)
        {
            string username = "vle@ismt.edu.np";
            string password = "15mtc0ll3g3@123";
            string Host = "smtp.gmail.com";

            var model = new MailViewModel
            {
                ToEmail = mail,
                Subject = subject,
                Body = htmlmessage,
                Username = username,
                Password = password,
                Host = Host

            };

            ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };


            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            var httpClient = new HttpClient(clientHandler);
            var Uri = "https://user-svc.withgyre.com/api/Notification/SendEmail";

            var myContent = JsonConvert.SerializeObject(model);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var byteContent = new ByteArrayContent(buffer);

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("ApiKey", "te8eNcO/FOyPhmZMSP1JZA==");
            try
            {
                var response = await httpClient.PostAsync(Uri, content);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;

            }
        }
    }
    public interface IEmailService
    {
        Task<bool> Mail_Alert(string mail, string subject, string htmlmessage);
    }
}
