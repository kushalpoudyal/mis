﻿using System;

namespace MIS.ViewModel
{
    public class FileUploadRequestViewModel
    {
        public FileUploadRequestViewModel()
        {
        }

        public Guid id { get; set; }
        public object AccessKey { get; set; }
        public object SecretKey { get; set; }
        public string ObjectKey { get; set; }
        public object BucketName { get; set; }
        public string FilePath { get; set; }
        public long FileLength { get; set; }
        public object EndPoint { get; set; }
        public object FileStream { get; set; }

        public string URL { get; set; }
    }
}