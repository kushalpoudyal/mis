﻿using DevExpress.Printing.Native;
using System;

namespace MIS.ViewModel
{
    public class BookDetailsViewModel
    {
        public Guid  Id { get; set; }
        public string Name { get; set; }

        public string Author { get; set; }
        public string Genre { get; set; }

        public string Course { get; set; }
        public string BookLocationKey { get; set; }

        public string ImageLocationKey { get; set; }
    }
}
