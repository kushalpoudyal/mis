﻿using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class FacultyDetailsViewModel
    {
        public Guid ID { get; set; }
        public string FacultyName { get; set; }
        public Guid DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public bool HeadOfDepartment { get; set; }
        public FacultyType FacultyType { get; set; }
        public string FacultyImgLocation { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
