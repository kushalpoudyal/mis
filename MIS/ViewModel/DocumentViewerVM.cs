﻿namespace MIS.ViewModel
{
    public class DocumentViewerVM
    {
        public string LocationKey { get; set; }

        public string BookName { get; set; }

        public bool IsDownloadable { get; set; }
        public string ImageLocation { get; internal set; }
    }
}
