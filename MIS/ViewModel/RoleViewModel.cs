﻿using MIS_Entity.Entity.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class RoleViewModel
    {
        public RoleViewModel()
        {
            this.roleDetailsList = new List<RoleDetails>();
        }
        public Guid Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "RoleName")]
        public string Name { get; set; }

        public List<RoleDetails> roleDetailsList { get; set; }
    }
}
