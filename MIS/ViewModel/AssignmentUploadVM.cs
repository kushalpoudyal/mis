﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class AssignmentUploadVM
    {
        [Required]
        public string filePath { get; set; }
        [Required]
        public string fileFormat { get; set; }
        [Required]
        public string Remarks { get; set; }
        [Required]
        public Guid AssignmentId { get; set; }
    }
}
