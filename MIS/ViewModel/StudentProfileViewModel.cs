﻿using MIS_Entity.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class StudentProfileViewModel
    {
        public Guid Id { get; set; }
        public Guid BatchId { get; set; }
        public string BatchName { get; set; }
        public Guid CourseId { get; set; }

        public string CourseName { get; set; }

        public string CourseCode { get; set; }

        public string StudentName { get; set; }

        public DateTime DOB { get; set; }

        public string ProfileImgLocation { get; set; }

        public BloodGroups BloodGroup { get; set; }

        public string Religion { get; set; }

        public string MobileNumber { get; set; }

        public string AlternateNumber { get; set; }

        public string Intake { get; set; }

        public string Fathername { get; set; }

        public string Mothername { get; set; }

        public string StudentEmail { get; set; }

        public string Nationality { get; set; }

        public string PermanentCountry { get; set; }

        public string PermanentState { get; set; }

        public string PermanentCity { get; set; }

        public string PermanentLocality { get; set; }

        public string TemporaryCountry { get; set; }

        public string TemporaryState { get; set; }

        public string TemporaryCity { get; set; }

        public string TemporaryLocality { get; set; }

        public string StudentID { get; set; }

        public string RegistrationNo { get; set; }

        public string Identification { get; set; }

        public string Id_Number { get; set; }

        public byte[] QrImage { get; set; }

        public DateTime Id_Issued_Date { get; set; }

        public string Id_Issued_Place { get; set; }

        public string Id_Issued_Country { get; set; }
        public List<StudentObservationViewModel> StudentObservation { get; set; }
    }
    public class StudentObservationViewModel
    {
        public Guid StudentId { get; set; }

        public string Remarks { get; set; }

        public int Criticality { get; set; }

        public DateTime GivenOn { get; set; }

        public Guid GivenById { get; set; }

        public string GivenByName { get; set; }
    }
}
