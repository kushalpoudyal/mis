﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class GetAssignmentViewModel
    {

        public Guid SubjectId { get; set; }

        public string SubjectName { get; set; }
    }

}
