﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class FileUploadAjaxModel
    {
        public string Message { get; set; }

        public bool Status { get; set; }

        public string FilePath { get; set; }

        public string FileFormat { get; set; }
    }
}
