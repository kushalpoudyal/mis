﻿using MIS_Entity.Entity.Course;
using MIS_Entity.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class CourseDetailViewModel
    {
        public Guid Id { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        public int CreditHours { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedTimes { get; set; }
        public DateTime ModifiedDate { get; set; }
        public RecordStatus RecordStatus { get; set; }

        public List<SubjectForCourseViewModel> SubjectCourses { get; set; }
    }
    public class SubjectForCourseViewModel
    {
        public Guid SubjectId { get; set; }

        public Guid FacultyId { get; set; }
    }
}
