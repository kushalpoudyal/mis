﻿using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class BookRequestActionVM
    {
        public Guid BookId { get; set; }

        public Guid UserId { get; set; }

        public int AvailableStock { get; set; }

        public RequestStatus RequestStatus { get; set; }
        public string ActionRemarks { get; internal set; }
    }
}
