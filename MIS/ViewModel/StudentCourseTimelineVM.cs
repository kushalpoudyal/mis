﻿using System.ComponentModel.DataAnnotations;
using System;

namespace MIS.ViewModel
{
    public class StudentCourseTimelineVM
    {
        
        public long Id { get; set; }

        public Guid StudentId { get; set; }

        public Guid CourseId { get; set; }

        public DateTime FromDate { get; set; }

        public bool WasEscalated { get; set; }

        public Guid GivenById { get; set; }

        public String CourseName { get; set; }

        public String CourseCode { get; set; }
        public DateTime GivenOnDate { get; set; }
    }
}
