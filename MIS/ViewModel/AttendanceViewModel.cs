﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class AttendanceViewModel
    {
        public bool CheckIn { get; set; }

        public bool CheckOut { get; set; }

    }
}
