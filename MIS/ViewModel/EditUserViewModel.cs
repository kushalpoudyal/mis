﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace MIS.ViewModel
{
    public class EditUserViewModel
    {
        public Guid Id   { get; set; }
        public string FullName { get; set; }
        public string Contact { get; set; }
        public string Country { get; set; }
        public string WorkTitle { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Username { get; set; }
       

        public List<string> Role { get; set; }
        public string BuyId { get; set; }
        public IFormFile Picture { get; set; }

        public string CurrentRole { get; set; }
    }
}
