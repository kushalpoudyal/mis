﻿using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class AssignmentDetailViewModel
    {
        public Guid Id { get; set; }
        public Guid SubjectId { get; set; }

        public Guid UploadedById { get; set; }

        public string AssignmentName { get; set; }

        public Guid BatchId { get; set; }

        public string BatchName { get; set; }

        public string AssignmentDetail { get; set; }

        public DateTime DeadlineDate { get; set; }

        public string FileLocation { get; set; }

        public string FileFormat { get; set; }

        public ICollection<AssignmentByStudentVM> AssignmentByStudentVM { get; set; }

        public ICollection<AssignmentByStudentVM> AssignmentByStudentReviewed { get; set; }

        public ICollection<AssignmentByStudentVM> AssignmentByStudentRejected { get; set; }

        public ICollection<AssignmentByStudentVM> AssignmentByStudentReturned { get; set; }
    }
    public class AssignmentByStudentVM
    {
        public Guid AssignmentId { get; set; }

        public Guid StudentId { get; set; }

        public string StudentName { get; set; }

         
        public string AssignmentContent { get; set; }

        public string Remarks { get; set; }

        public string FileLocation { get; set; }

        public DateTime CreatedOn { get; set; }

        public AssignmentStatus AssignmentStatus { get; set; }
    }
}
