﻿using GLib;
using MIS_Entity.Enum;
using System;
using System.Collections.Generic;

namespace MIS.ViewModel
{
    public class AssignmentTimelineDetailsViewModel
    {
        public Guid AssignmentId { get; set; }
         

        public string Remarks { get; set; }

        public string FileLocation { get; set; }

        public string AssignmentName { get; set; }

        public string SubmittedByName { get; set; }

        public AssignmentStatus AssignmentStatus { get; set; }

        public List<AssignmentTimelineViewModel> AssignmentTimelineViewModel { get; set; }
        public Guid AssignmentByStudentId { get; internal set; }
    }

    public class AssignmentTimelineViewModel
    {
        public long Id { get; set; }
        public Guid AssignmentByStudentId { get; set; }

        public string Remarks { get; set; }

        public AssignmentStatus AssignmentStatus { get; set; }

        public System.DateTime GivenOn { get; set; }

        public Guid GivenById { get; set; }

        public string GivenByName { get; set; }

      
    }
}
