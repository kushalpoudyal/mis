﻿using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class BookRequestViewModel
    {
        public Guid BookId { get; set; }

        public Guid UserId { get; set; }

        public string Fullname { get; set; }

        public string BookName { get; set; }
        public DateTime RequestedFor { get; set; }

        public DateTime RequestedTil { get; set; }

        public string AdditionalRemarks { get; set; }

        public RequestStatus RequestStatus { get; set; }

        public DateTime RequestedOn { get; set; }


    }
}
