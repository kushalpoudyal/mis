﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class AdminDashboardViewModel
    {
        public int TotalStudents { get; set; }

        public int TotalStaffs { get; set; }

        public int TotalBooks { get; set; }

        public int TotalSubjects { get; set; }

        public int TotalCourse { get; set; }

        public int TotalBatches { get; set; }

        public int TotalDepartment { get; set; }
    }
}
