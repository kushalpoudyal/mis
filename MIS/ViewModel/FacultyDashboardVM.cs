﻿using MIS_Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class FacultyDashboardVM
    {
        public List<NoticeDetails> Notices { get; set; }
    }
}
