﻿using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class NoticeDetailViewModel
    {
       
        public string Title { get; set; }
        [Required] 
        public string Description { get; set; }

        public Priority Priority { get; set; }

        public DateTime ExpiryDate { get; set; }
    }
}
