﻿using MIS_Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.ViewModel
{
    public class StudentDashboardVM
    {
        public List<NoticeDetails> Notices { get; set; }

        public List<BookDetails> TrendingBook { get; set; }

        public List<BookDetails> RecentBook { get; set; }

        public string UpcomingClass { get; set; }

        public string UpcomingClassLink { get; set; }
    }
}
