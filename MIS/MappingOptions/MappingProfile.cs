﻿using AutoMapper;
using MIS.ViewModel;
using MIS_Entity.DTO;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Course;
using MIS_Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.MappingOptions
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CourseDetailViewModel, CourseDetails>().ReverseMap();
            CreateMap<AdminDashboardViewModel, AdminDashboardDTO>().ReverseMap();
            CreateMap<StudentProfileViewModel, StudentDetails>().ReverseMap();
            CreateMap<FileResponseViewModel, FileUploadRequestDTO>().ReverseMap();
            CreateMap<FileUploadRequestViewModel, FileUploadRequestDTO>().ReverseMap();
            CreateMap<FileResponseViewModel, FileResponseDTO>().ReverseMap(); 


            //.ForMember(dest => dest.Compensation, source => source.MapFrom(source => source.Salary)); //For Specific Mapping
            //Map from BadgeInfo Object to BadgeViewModel Object
        }
    }
}
