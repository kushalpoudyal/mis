﻿using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MIS.DBContext;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MIS.Customs
{
    public class AdditionalUserClaimsPrincipalFactory
        : UserClaimsPrincipalFactory<ApplicationUser, ApplicationRole>
    {
        private readonly ISMTDbContext _context;

        public AdditionalUserClaimsPrincipalFactory(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IOptions<IdentityOptions> optionsAccessor,
            ISMTDbContext context)
            : base(userManager, roleManager, optionsAccessor)
        {
            _context = context;
        }

        public async override Task<ClaimsPrincipal> CreateAsync(ApplicationUser user)
        {
            var principal = await base.CreateAsync(user);
            var identity = (ClaimsIdentity)principal.Identity;
            if (user != null)
            {
                var usrRole = await this.UserManager.GetRolesAsync(user);

                var claims = new List<Claim>
                     {
                               new Claim(ClaimTypes.Role,usrRole.FirstOrDefault()),
                               new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                               new Claim("Name",user.Fullname.ToString()),
                               new Claim(ClaimTypes.Email,user.Email.ToString()),
                    };
                if (usrRole.Contains(Roles.Student.ToString()))
                {
                    var StudentDetails = await _context.StudentDetails.FirstOrDefaultAsync(x => x.UserId == user.Id);
                    if (StudentDetails == null)
                    {
                        throw new Exception("Student details not found");
                    }
                    claims.Add(new Claim("CourseId", StudentDetails.CourseId.ToString()));
                    claims.Add(new Claim("BatchId", StudentDetails.BatchId.ToString()));
                    claims.Add(new Claim("StudentId", StudentDetails.ID.ToString()));
                    claims.Add(new Claim("ProfileImagePath", string.IsNullOrEmpty(StudentDetails.ProfileImgLocation) ? "https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png" : StudentDetails.ProfileImgLocation.ToString()));

                }
                if (usrRole.Contains("Faculty") || usrRole.Contains("Librarian"))
                {
                    var facDetails = await _context.FacultyDetails.FirstOrDefaultAsync(x => x.ID == user.Id);
                    if (usrRole.Contains("Librarian") && facDetails == null) { }
                    else
                    {
                        var SubjectIDs = JsonConvert.SerializeObject(await _context.SubjectByCourse.Where(x => x.FacultyId == user.Id).Select(x => x.SubjectId).ToListAsync());
                        if (facDetails == null)
                        {
                            throw new Exception("Teacher details not found");
                        }
                        claims.Add(new Claim("DepartmentID", facDetails.DepartmentID.ToString()));
                        claims.Add(new Claim("SubjectIDs", SubjectIDs));
                    }

                }
                var appIdentity = new ClaimsIdentity(claims);
                identity.AddClaims(claims);
            }

            return principal;
        }
    }
}
