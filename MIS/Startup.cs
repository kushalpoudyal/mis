using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MIS.Customs;
using MIS.DBContext;
using MIS.Helper;
using MIS.Interfaces;
using MIS.MappingOptions;
using MIS.Services;
using MIS.ViewModel;
using MIS_Entity.Entity.Identity;
using MIS_Repository;
using MIS_Repository.Interfaces;

namespace MIS
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.Configure<FileUploadRequestViewModel>(Configuration.GetSection("FTP"));
            //services.AddTransient<IRepository<>, Repository>();
            services.AddDbContext<ISMTDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DevConnection")));
            services.AddIdentity<ApplicationUser, ApplicationRole>()
        .AddEntityFrameworkStores<ISMTDbContext>().AddDefaultTokenProviders();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<ILibraryRepository, LibraryRepository>();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddTransient<IBookGenreRepository, BookGenreRepository>();
            services.AddTransient<ICourseRepository, CourseRepository>();
            services.AddTransient<ICourseSubjectRepository, CourseSubjectRepository>();
            services.AddTransient<IDepartmentRepository, DeparmentRepository>();
            services.AddTransient<IFacultyRepository, FacultyRepository>();
            services.AddTransient<IStudentRepository, StudentRepository>();
            services.AddTransient<IBatchRepository, BatchRepository>();
            services.AddTransient<IClassRepository, ClassRepository>();
            services.AddTransient<IAdminRepository, AdminRepository>();
            services.AddTransient<IAssignmentRepository, AssignmentRepository>();
            services.AddTransient<IStudentAssignmentRepository, StudentAssignmentRepository>();
            services.AddTransient<IRoleDetailsRepository, RoleDetailsRepository>();
            services.AddScoped<SessionHelper>();
            services.AddScoped<IBucketService, BucketService>();
            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>,
    AdditionalUserClaimsPrincipalFactory>();
            //Automapper
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddSingleton<IEmailService, EmailService>();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseCors(options => options
          .AllowAnyHeader().AllowAnyMethod());
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}
