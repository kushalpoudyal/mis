﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using MIS_Entity.ViewModel;

namespace MIS_API.Service
{
    public class EmailServices : IEmailServices
    {
        public EmailServices()
        {

        }
        public void Mail_Alert(MailViewModel model)
        {

            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(model.Username));
            email.To.Add(MailboxAddress.Parse(model.ToEmail));
            email.Subject = model.Subject;
            email.Body = new TextPart(TextFormat.Html) { Text = model.Body };

            // send email
            using (var smtp = new SmtpClient())
            {
                smtp.Connect(model.Host, 587, SecureSocketOptions.StartTlsWhenAvailable);
                smtp.Authenticate(model.Username, model.Password);
                smtp.Send(email);
                smtp.Disconnect(true);
            }

        }
    }
    public interface IEmailServices
    {
        void Mail_Alert(MailViewModel model);
    }
}
