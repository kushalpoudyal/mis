﻿using Microsoft.AspNetCore.Mvc;
using MIS_API.Service;
using MIS_Entity.ViewModel;

namespace MIS_API.Controllers
{
    public class NotificationController : Controller
    {
        private readonly IEmailServices emailService;
        public NotificationController(IEmailServices emailService)
        {
            this.emailService = emailService;
        }
        [HttpPost]
        public IActionResult SendEmail([FromBody] MailViewModel model)
        {
            try
            {
                emailService.Mail_Alert(model);
                return Ok();
            }
            catch (Exception ex)
            {


                return BadRequest(ex.ToString());
            }

        }
    }
}
