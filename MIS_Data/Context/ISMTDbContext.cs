﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Batches;
using MIS_Entity.Entity.Classes;
using MIS_Entity.Entity.Course;
using MIS_Entity.Entity.Department;
using MIS_Entity.Entity.Documents;
using MIS_Entity.Entity.Examination;
using MIS_Entity.Entity.FacultyDetails;
using MIS_Entity.Entity.Identity;
using MIS_Entity.Entity.Library;
using MIS_Entity.Entity.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.DBContext
{
    public class ISMTDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public ISMTDbContext(DbContextOptions<ISMTDbContext> options) : base(options)
        {

        }

        public ISMTDbContext()
        {
        }

        public virtual DbSet<AssignmentByStudent> AssignmentByStudent { get; set; }
        public virtual DbSet<AssignmentMaterial> AssignmentMaterial { get; set; }
        public virtual DbSet<BookDetails> BookDetails { get; set; }
        public virtual DbSet<BookGenre> BookGenre { get; set; }
        public virtual DbSet<BookTimeline> BookTimeline { get; set; }

        public virtual DbSet<CourseDetails> CourseDetails { get; set; }

        public virtual DbSet<Department> Department { get; set; }

        public virtual DbSet<FacultyDetails> FacultyDetails { get; set; }
        public virtual DbSet<StudentDetails> StudentDetails { get; set; }

        public virtual DbSet<BatchDetails> BatchDetails { get; set; }

        public virtual DbSet<ClassDetails> ClassDetails { get; set; }

        public virtual DbSet<UserDocuments> UserDocuments { get; set; }

        public virtual DbSet<SubjectByCourse> SubjectByCourse { get; set; }
        public virtual DbSet<CourseSubject> CourseSubject { get; set; }
        public virtual DbSet<UserAttendanceDetails> UserAttendanceDetails { get; set; }
        public virtual DbSet<NoticeDetails> NoticeDetails { get; set; }

        public virtual DbSet<BookRequest> BookRequest { get; set; }

        public virtual DbSet<LoginActivity> LoginActivity { get; set; }

        public virtual DbSet<BookActivity> BookActivity { get; set; }
        public virtual DbSet<StudentObservation> StudentObservation { get; set; }
        public virtual DbSet<StudentPayment> StudentPayment { get; set; }
        public virtual DbSet<StudentSuspension> StudentSuspension { get; set; }
        public virtual DbSet<StudentCourseTimeline> StudentCourseTimeline { get; set; }

        public virtual DbSet<RoleDetails> RoleDetails { get; set; }

        public virtual DbSet<AssignmentTimeline> AssignmentTimeline { get; set; }

        public virtual DbSet<Recommendations> Recommendations { get; set; }

        public virtual DbSet<ExaminationGrading> ExaminationGrading { get; set; }

        public virtual DbSet<ExaminationDetails> ExaminationDetails { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().ToTable("ApplicationUser");
            modelBuilder.Entity<ApplicationRole>().ToTable("ApplicationRole");

            modelBuilder.Entity<BookDetails>().HasQueryFilter(x => !x.Deleted);

            modelBuilder.Entity<SubjectByCourse>()
             .HasKey(x => new { x.CourseId, x.SubjectId });

            modelBuilder.Entity<BookRequest>()
            .HasKey(x => new { x.BookId, x.UserId });

            modelBuilder.Entity<BookTimeline>()
           .HasKey(x => new { x.BookId, x.UserId });

        }
        public async Task<int> CommitAsync()
        {
            //no change log table so commented
            //var modifiedEntities = this.ChangeTracker.Entries()
            //    .Where(s => s.State == EntityState.Added || s.State == EntityState.Modified)
            //    .ToList();
            //var now = DateTime.UtcNow;
            //foreach (var modified in modifiedEntities)
            //{
            //    if (modified.State == EntityState.Added)
            //    {
            //        if (modified.Metadata.FindProperty("Id") == null)
            //            continue;// to do fix 
            //        //for newly added 
            //        foreach (var prop in modified.OriginalValues.Properties)
            //        {
            //            if (modified.CurrentValues[prop] != null)
            //            {
            //                var originalValue = modified.OriginalValues[prop]?.ToString() ?? null;
            //                var currentValue = modified.CurrentValues[prop].ToString() ?? null;
            //                //var chnage = modified.Property("Id");

            //                var Changelog = new ChangeLogs()
            //                {
            //                    PKId = Guid.Parse(modified.Property("Id")?.CurrentValue?.ToString() ?? Guid.NewGuid().ToString()),
            //                    EntityName = modified.Entity.GetType().Name,
            //                    PropertyName = prop.GetColumnName(),
            //                    OldValue = currentValue,
            //                    NewValue = currentValue,
            //                    DateChanged = now,
            //                    EntityState = modified.State
            //                };
            //                Add(Changelog);

            //            }

            //        }
            //    }
            //    else
            //    {

            //        //for mdoified added 
            //        foreach (var prop in modified.OriginalValues.Properties)
            //        {
            //            if (modified.CurrentValues[prop] != null)
            //            {
            //                var originalValue = modified.OriginalValues[prop]?.ToString() ?? null;
            //                var currentValue = modified.CurrentValues[prop].ToString() ?? null;
            //                if (originalValue == currentValue)
            //                {
            //                    continue;
            //                }


            //                var Changelog = new ChangeLogs()
            //                {
            //                    PKId = Guid.Parse(modified.Property("Id").CurrentValue?.ToString() ?? Guid.NewGuid().ToString()),
            //                    EntityName = modified.Entity.GetType().Name,
            //                    PropertyName = prop.GetColumnName(),
            //                    OldValue = originalValue,
            //                    NewValue = currentValue,
            //                    DateChanged = now,
            //                    EntityState = modified.State
            //                };
            //                Add(Changelog);

            //            }

            //        }
            //    }
            //}
            return await this.SaveChangesAsync();
        }
    }


}
