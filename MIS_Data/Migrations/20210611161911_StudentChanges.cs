﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class StudentChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GrandFather",
                table: "StudentDetails");

            migrationBuilder.AddColumn<string>(
                name: "Intake",
                table: "StudentDetails",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RegistrationNo",
                table: "StudentDetails",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StudentID",
                table: "StudentDetails",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Intake",
                table: "StudentDetails");

            migrationBuilder.DropColumn(
                name: "RegistrationNo",
                table: "StudentDetails");

            migrationBuilder.DropColumn(
                name: "StudentID",
                table: "StudentDetails");

            migrationBuilder.AddColumn<string>(
                name: "GrandFather",
                table: "StudentDetails",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);
        }
    }
}
