﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class TimelINe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AssignmentTimeline",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AssignmentByStudentId = table.Column<Guid>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    AssignmentStatus = table.Column<int>(nullable: false),
                    GivenOn = table.Column<DateTime>(nullable: false),
                    GivenById = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignmentTimeline", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssignmentTimeline_AssignmentByStudent_AssignmentByStudentId",
                        column: x => x.AssignmentByStudentId,
                        principalTable: "AssignmentByStudent",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssignmentTimeline_ApplicationUser_GivenById",
                        column: x => x.GivenById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentTimeline_AssignmentByStudentId",
                table: "AssignmentTimeline",
                column: "AssignmentByStudentId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentTimeline_GivenById",
                table: "AssignmentTimeline",
                column: "GivenById");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssignmentTimeline");
        }
    }
}
