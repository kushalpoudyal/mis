﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class ChangesforStudent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSuspended",
                table: "StudentDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "StudentCourseTimeline",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StudentId = table.Column<Guid>(nullable: false),
                    CourseId = table.Column<Guid>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    WasEscalated = table.Column<bool>(nullable: false),
                    GivenById = table.Column<Guid>(nullable: false),
                    GivenOnDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCourseTimeline", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StudentSuspension",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StudentId = table.Column<Guid>(nullable: false),
                    GivenById = table.Column<Guid>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    GivenOnDate = table.Column<DateTime>(nullable: false),
                    TillDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentSuspension", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentCourseTimeline");

            migrationBuilder.DropTable(
                name: "StudentSuspension");

            migrationBuilder.DropColumn(
                name: "IsSuspended",
                table: "StudentDetails");
        }
    }
}
