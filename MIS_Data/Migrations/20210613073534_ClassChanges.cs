﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class ClassChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "SubjectID",
                table: "ClassDetails",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_ClassDetails_SubjectID",
                table: "ClassDetails",
                column: "SubjectID");

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassDetails_CourseSubject_SubjectID",
                table: "ClassDetails");

            migrationBuilder.DropIndex(
                name: "IX_ClassDetails_SubjectID",
                table: "ClassDetails");

            migrationBuilder.DropColumn(
                name: "SubjectID",
                table: "ClassDetails");
        }
    }
}
