﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class RequestForBook : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_BookTimeline",
                table: "BookTimeline");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BookTimeline",
                table: "BookTimeline",
                columns: new[] { "BookId", "UserId" });

            migrationBuilder.CreateTable(
                name: "BookRequest",
                columns: table => new
                {
                    BookId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    RequestedFor = table.Column<DateTime>(nullable: false),
                    RequestedTil = table.Column<DateTime>(nullable: false),
                    AdditionalRemarks = table.Column<string>(nullable: true),
                    RequestStatus = table.Column<int>(nullable: false),
                    RequestedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookRequest", x => new { x.BookId, x.UserId });
                    table.ForeignKey(
                        name: "FK_BookRequest_StudentDetails_BookId",
                        column: x => x.BookId,
                        principalTable: "StudentDetails",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BookRequest_BookDetails_UserId",
                        column: x => x.UserId,
                        principalTable: "BookDetails",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BookTimeline_UserId",
                table: "BookTimeline",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BookRequest_UserId",
                table: "BookRequest",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_BookTimeline_StudentDetails_BookId",
                table: "BookTimeline",
                column: "BookId",
                principalTable: "StudentDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BookTimeline_BookDetails_UserId",
                table: "BookTimeline",
                column: "UserId",
                principalTable: "BookDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookTimeline_StudentDetails_BookId",
                table: "BookTimeline");

            migrationBuilder.DropForeignKey(
                name: "FK_BookTimeline_BookDetails_UserId",
                table: "BookTimeline");

            migrationBuilder.DropTable(
                name: "BookRequest");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BookTimeline",
                table: "BookTimeline");

            migrationBuilder.DropIndex(
                name: "IX_BookTimeline_UserId",
                table: "BookTimeline");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BookTimeline",
                table: "BookTimeline",
                column: "BookId");
        }
    }
}
