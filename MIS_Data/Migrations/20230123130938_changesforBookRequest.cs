﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class changesforBookRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ActionRemarks",
                table: "BookRequest",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ActionTakenBy",
                table: "BookRequest",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionRemarks",
                table: "BookRequest");

            migrationBuilder.DropColumn(
                name: "ActionTakenBy",
                table: "BookRequest");
        }
    }
}
