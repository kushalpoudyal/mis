﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class RequestForBooks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookRequest_StudentDetails_BookId",
                table: "BookRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_BookRequest_BookDetails_UserId",
                table: "BookRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_BookTimeline_StudentDetails_BookId",
                table: "BookTimeline");

            migrationBuilder.DropForeignKey(
                name: "FK_BookTimeline_BookDetails_UserId",
                table: "BookTimeline");

            migrationBuilder.AddForeignKey(
                name: "FK_BookRequest_BookDetails_BookId",
                table: "BookRequest",
                column: "BookId",
                principalTable: "BookDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BookRequest_StudentDetails_UserId",
                table: "BookRequest",
                column: "UserId",
                principalTable: "StudentDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_BookTimeline_BookDetails_BookId",
                table: "BookTimeline",
                column: "BookId",
                principalTable: "BookDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BookTimeline_StudentDetails_UserId",
                table: "BookTimeline",
                column: "UserId",
                principalTable: "StudentDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookRequest_BookDetails_BookId",
                table: "BookRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_BookRequest_StudentDetails_UserId",
                table: "BookRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_BookTimeline_BookDetails_BookId",
                table: "BookTimeline");

            migrationBuilder.DropForeignKey(
                name: "FK_BookTimeline_StudentDetails_UserId",
                table: "BookTimeline");

            migrationBuilder.AddForeignKey(
                name: "FK_BookRequest_StudentDetails_BookId",
                table: "BookRequest",
                column: "BookId",
                principalTable: "StudentDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BookRequest_BookDetails_UserId",
                table: "BookRequest",
                column: "UserId",
                principalTable: "BookDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BookTimeline_StudentDetails_BookId",
                table: "BookTimeline",
                column: "BookId",
                principalTable: "StudentDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BookTimeline_BookDetails_UserId",
                table: "BookTimeline",
                column: "UserId",
                principalTable: "BookDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
