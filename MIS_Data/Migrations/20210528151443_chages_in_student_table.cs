﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class chages_in_student_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Id_Issued_Date",
                table: "StudentDetails",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BloodGroup",
                table: "StudentDetails",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(5)",
                oldMaxLength: 5,
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Status",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.CreateIndex(
                name: "IX_StudentDetails_BatchId",
                table: "StudentDetails",
                column: "BatchId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDetails_CourseId",
                table: "StudentDetails",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDetails_UserId",
                table: "StudentDetails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassDetails_BatchID",
                table: "ClassDetails",
                column: "BatchID");

            migrationBuilder.CreateIndex(
                name: "IX_ClassDetails_CourseID",
                table: "ClassDetails",
                column: "CourseID");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDetails_BatchDetails_BatchID",
                table: "ClassDetails",
                column: "BatchID",
                principalTable: "BatchDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDetails_CourseDetails_CourseID",
                table: "ClassDetails",
                column: "CourseID",
                principalTable: "CourseDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentDetails_BatchDetails_BatchId",
                table: "StudentDetails",
                column: "BatchId",
                principalTable: "BatchDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentDetails_CourseDetails_CourseId",
                table: "StudentDetails",
                column: "CourseId",
                principalTable: "CourseDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentDetails_ApplicationUser_UserId",
                table: "StudentDetails",
                column: "UserId",
                principalTable: "ApplicationUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassDetails_BatchDetails_BatchID",
                table: "ClassDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassDetails_CourseDetails_CourseID",
                table: "ClassDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentDetails_BatchDetails_BatchId",
                table: "StudentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentDetails_CourseDetails_CourseId",
                table: "StudentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentDetails_ApplicationUser_UserId",
                table: "StudentDetails");

            migrationBuilder.DropIndex(
                name: "IX_StudentDetails_BatchId",
                table: "StudentDetails");

            migrationBuilder.DropIndex(
                name: "IX_StudentDetails_CourseId",
                table: "StudentDetails");

            migrationBuilder.DropIndex(
                name: "IX_StudentDetails_UserId",
                table: "StudentDetails");

            migrationBuilder.DropIndex(
                name: "IX_ClassDetails_BatchID",
                table: "ClassDetails");

            migrationBuilder.DropIndex(
                name: "IX_ClassDetails_CourseID",
                table: "ClassDetails");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "ApplicationUser");

            migrationBuilder.AlterColumn<string>(
                name: "Id_Issued_Date",
                table: "StudentDetails",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "BloodGroup",
                table: "StudentDetails",
                type: "nvarchar(5)",
                maxLength: 5,
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
