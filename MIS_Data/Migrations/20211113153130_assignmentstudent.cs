﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class assignmentstudent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
              
            migrationBuilder.CreateTable(
               name: "AssignmentByStudent",
               columns: table => new
               {
                   ID = table.Column<Guid>(nullable: false),
                   CreatedBy = table.Column<Guid>(nullable: false),
                   ModifiedBy = table.Column<Guid>(nullable: false),
                   CreatedDate = table.Column<DateTime>(nullable: false),
                   ModifiedTimes = table.Column<int>(nullable: false),
                   ModifiedDate = table.Column<DateTime>(nullable: false),
                   RecordStatus = table.Column<int>(nullable: false),
                   AssignmentId = table.Column<Guid>(nullable: false), 
                   AssignmentContent = table.Column<string>(nullable: true),
                   Remarks = table.Column<string>(nullable: true),
                   FileLocation = table.Column<string>(nullable: true),
                   AssignmentStatus = table.Column<int>(nullable: false)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_AssignmentByStudent", x => x.ID);
                   table.ForeignKey(
                       name: "FK_AssignmentByStudent_AssignmentMaterial_AssignmentId",
                       column: x => x.AssignmentId,
                       principalTable: "AssignmentMaterial",
                       principalColumn: "ID",
                       onDelete: ReferentialAction.NoAction);
                   table.ForeignKey(
                       name: "FK_AssignmentByStudent_ApplicationUser_CreatedBy",
                       column: x => x.CreatedBy,
                       principalTable: "ApplicationUser",
                       principalColumn: "Id",
                       onDelete: ReferentialAction.NoAction);
               });

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentByStudent_AssignmentId",
                table: "AssignmentByStudent",
                column: "AssignmentId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentByStudent_CreatedBy",
                table: "AssignmentByStudent",
                column: "CreatedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentMaterial_BatchDetails_BatchId",
                table: "AssignmentMaterial");

            migrationBuilder.DropIndex(
                name: "IX_AssignmentMaterial_BatchId",
                table: "AssignmentMaterial");

            migrationBuilder.AddColumn<Guid>(
                name: "StudentId",
                table: "AssignmentByStudent",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
