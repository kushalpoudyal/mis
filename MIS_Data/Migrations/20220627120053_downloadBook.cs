﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class downloadBook : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentMaterial_FacultyDetails_UploadedById",
                table: "AssignmentMaterial");

            migrationBuilder.AddColumn<bool>(
                name: "IsDownloadable",
                table: "BookDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_AssignmentMaterial_ApplicationUser_UploadedById",
                table: "AssignmentMaterial",
                column: "UploadedById",
                principalTable: "ApplicationUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentMaterial_ApplicationUser_UploadedById",
                table: "AssignmentMaterial");

            migrationBuilder.DropColumn(
                name: "IsDownloadable",
                table: "BookDetails");

            migrationBuilder.AddForeignKey(
                name: "FK_AssignmentMaterial_FacultyDetails_UploadedById",
                table: "AssignmentMaterial",
                column: "UploadedById",
                principalTable: "FacultyDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
