﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class NoticeDEtails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AssignmentMaterial",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    RecordStatus = table.Column<int>(nullable: false),
                    SubjectId = table.Column<Guid>(nullable: false),
                    UploadedById = table.Column<Guid>(nullable: false),
                    AssignmentName = table.Column<string>(nullable: true),
                    AssignmentDetail = table.Column<string>(nullable: true),
                    DeadlineDate = table.Column<DateTime>(nullable: false),
                    FileLocation = table.Column<string>(nullable: true),
                    FileFormat = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignmentMaterial", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AssignmentMaterial_ApplicationUser_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_AssignmentMaterial_CourseSubject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "CourseSubject",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_AssignmentMaterial_FacultyDetails_UploadedById",
                        column: x => x.UploadedById,
                        principalTable: "FacultyDetails",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NoticeDetails",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    RecordStatus = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    ExpiryDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NoticeDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_NoticeDetails_ApplicationUser_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserAttendanceDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserID = table.Column<Guid>(nullable: false),
                    CheckIn = table.Column<DateTime>(nullable: false),
                    CheckOut = table.Column<DateTime>(nullable: true),
                    Remarks = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatedById = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAttendanceDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserAttendanceDetails_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_UserAttendanceDetails_ApplicationUser_UserID",
                        column: x => x.UserID,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentMaterial_CreatedBy",
                table: "AssignmentMaterial",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentMaterial_SubjectId",
                table: "AssignmentMaterial",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentMaterial_UploadedById",
                table: "AssignmentMaterial",
                column: "UploadedById");

            migrationBuilder.CreateIndex(
                name: "IX_NoticeDetails_CreatedBy",
                table: "NoticeDetails",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_UserAttendanceDetails_CreatedById",
                table: "UserAttendanceDetails",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UserAttendanceDetails_UserID",
                table: "UserAttendanceDetails",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssignmentMaterial");

            migrationBuilder.DropTable(
                name: "NoticeDetails");

            migrationBuilder.DropTable(
                name: "UserAttendanceDetails");
        }
    }
}
