﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class CourseSubjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CourseTeacher",
                table: "CourseDetails");

            migrationBuilder.AddColumn<string>(
                name: "FacultyContact",
                table: "FacultyDetails",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FacultyEmail",
                table: "FacultyDetails",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CourseSubject",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    RecordStatus = table.Column<int>(nullable: false),
                    SubjectCode = table.Column<string>(maxLength: 50, nullable: true),
                    SubjectName = table.Column<string>(maxLength: 50, nullable: true),
                    SubjectDescription = table.Column<string>(maxLength: 150, nullable: true),
                    CreditHours = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseSubject", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CourseSubject_ApplicationUser_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubjectByCourse",
                columns: table => new
                {
                    SubjectId = table.Column<Guid>(nullable: false),
                    CourseId = table.Column<Guid>(nullable: false),
                    FacultyId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectByCourse", x => new { x.CourseId, x.SubjectId });
                });

            migrationBuilder.CreateTable(
                name: "UserDocuments",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    RecordStatus = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    DocumentType = table.Column<int>(nullable: false),
                    DocLocation = table.Column<string>(nullable: true),
                    DocExtension = table.Column<string>(nullable: true),
                    DocumentSize = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDocuments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserDocuments_ApplicationUser_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CourseSubject_CreatedBy",
                table: "CourseSubject",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_UserDocuments_CreatedBy",
                table: "UserDocuments",
                column: "CreatedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CourseSubject");

            migrationBuilder.DropTable(
                name: "SubjectByCourse");

            migrationBuilder.DropTable(
                name: "UserDocuments");

            migrationBuilder.DropColumn(
                name: "FacultyContact",
                table: "FacultyDetails");

            migrationBuilder.DropColumn(
                name: "FacultyEmail",
                table: "FacultyDetails");

            migrationBuilder.AddColumn<Guid>(
                name: "CourseTeacher",
                table: "CourseDetails",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
