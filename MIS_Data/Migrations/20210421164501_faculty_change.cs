﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class faculty_change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DepartmentID",
                table: "Faculty",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepartmentID",
                table: "Faculty");
        }
    }
}
