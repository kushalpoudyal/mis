﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class studentDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StudentDetails",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    RecordStatus = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    BatchId = table.Column<Guid>(nullable: false),
                    CourseId = table.Column<Guid>(nullable: false),
                    StudentName = table.Column<string>(maxLength: 50, nullable: false),
                    DOB = table.Column<DateTime>(nullable: false),
                    ProfileImgLocation = table.Column<string>(maxLength: 100, nullable: true),
                    BloodGroup = table.Column<string>(maxLength: 5, nullable: true),
                    Religion = table.Column<string>(maxLength: 20, nullable: true),
                    MobileNumber = table.Column<string>(maxLength: 20, nullable: true),
                    AlternateNumber = table.Column<string>(maxLength: 20, nullable: true),
                    GrandFather = table.Column<string>(maxLength: 50, nullable: true),
                    Fathername = table.Column<string>(maxLength: 50, nullable: true),
                    Mothername = table.Column<string>(maxLength: 50, nullable: true),
                    StudentEmail = table.Column<string>(maxLength: 50, nullable: true),
                    Nationality = table.Column<string>(maxLength: 50, nullable: true),
                    PermanentCountry = table.Column<string>(maxLength: 50, nullable: true),
                    PermanentState = table.Column<string>(maxLength: 50, nullable: true),
                    PermanentCity = table.Column<string>(maxLength: 50, nullable: true),
                    PermanentLocality = table.Column<string>(maxLength: 50, nullable: true),
                    TemporaryCountry = table.Column<string>(maxLength: 50, nullable: true),
                    TemporaryState = table.Column<string>(maxLength: 50, nullable: true),
                    TemporaryCity = table.Column<string>(maxLength: 50, nullable: true),
                    TemporaryLocality = table.Column<string>(maxLength: 50, nullable: true),
                    Identification = table.Column<string>(maxLength: 50, nullable: true),
                    Id_Number = table.Column<string>(maxLength: 50, nullable: true),
                    Id_Issued_Date = table.Column<string>(maxLength: 50, nullable: true),
                    Id_Issued_Place = table.Column<string>(maxLength: 50, nullable: true),
                    Id_Issued_Country = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_StudentDetails_ApplicationUser_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentDetails_CreatedBy",
                table: "StudentDetails",
                column: "CreatedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentDetails");
        }
    }
}
