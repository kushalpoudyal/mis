﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class assignments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AssignmentMaterial",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedTimes = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    RecordStatus = table.Column<int>(nullable: false),
                    SubjectId = table.Column<Guid>(nullable: false),
                    UploadedById = table.Column<Guid>(nullable: false),
                    AssignmentName = table.Column<string>(nullable: true),
                    AssignmentDetail = table.Column<string>(nullable: true),
                    DeadlineDate = table.Column<DateTime>(nullable: false),
                    FileLocation = table.Column<string>(nullable: true),
                    FileFormat = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignmentMaterial", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AssignmentMaterial_ApplicationUser_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssignmentMaterial_CourseSubject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "CourseSubject",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_AssignmentMaterial_FacultyDetails_UploadedById",
                        column: x => x.UploadedById,
                        principalTable: "FacultyDetails",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentMaterial_CreatedBy",
                table: "AssignmentMaterial",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentMaterial_SubjectId",
                table: "AssignmentMaterial",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentMaterial_UploadedById",
                table: "AssignmentMaterial",
                column: "UploadedById");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssignmentMaterial");
        }
    }
}
