﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class changesDeletedStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "UserDocuments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "StudentDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "RoleDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "NoticeDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "FacultyDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "Department",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "CourseSubject",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "CourseDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "ClassDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "BookDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "BatchDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "AssignmentMaterial",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "AssignmentByStudent",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDate",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "ModifiedTimes",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RecordStatus",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "UserDocuments");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "StudentDetails");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "RoleDetails");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "NoticeDetails");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "FacultyDetails");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "CourseSubject");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "CourseDetails");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "ClassDetails");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "BookDetails");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "BatchDetails");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "AssignmentMaterial");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "AssignmentByStudent");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "ModifiedDate",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "ModifiedTimes",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "RecordStatus",
                table: "ApplicationUser");
        }
    }
}
