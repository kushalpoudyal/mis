﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class Observation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StudentObservation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StudentId = table.Column<Guid>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    Criticality = table.Column<int>(nullable: false),
                    GivenOn = table.Column<DateTime>(nullable: false),
                    GivenById = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentObservation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StudentPayment",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StudentId = table.Column<Guid>(nullable: false),
                    TotalCourseAmount = table.Column<decimal>(nullable: false),
                    HasPaid = table.Column<bool>(nullable: false),
                    PaymentDueDate = table.Column<DateTime>(nullable: false),
                    RecordedDate = table.Column<DateTime>(nullable: false),
                    GivenById = table.Column<Guid>(nullable: false),
                    GivenOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentPayment", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentObservation");

            migrationBuilder.DropTable(
                name: "StudentPayment");
        }
    }
}
