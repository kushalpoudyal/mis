﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class Examandotherchang : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Recommendations_StudentId",
                table: "Recommendations",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_ExaminationDetails_SubjectId",
                table: "ExaminationDetails",
                column: "SubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExaminationDetails_CourseSubject_SubjectId",
                table: "ExaminationDetails",
                column: "SubjectId",
                principalTable: "CourseSubject",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Recommendations_StudentDetails_StudentId",
                table: "Recommendations",
                column: "StudentId",
                principalTable: "StudentDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExaminationDetails_CourseSubject_SubjectId",
                table: "ExaminationDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Recommendations_StudentDetails_StudentId",
                table: "Recommendations");

            migrationBuilder.DropIndex(
                name: "IX_Recommendations_StudentId",
                table: "Recommendations");

            migrationBuilder.DropIndex(
                name: "IX_ExaminationDetails_SubjectId",
                table: "ExaminationDetails");
        }
    }
}
