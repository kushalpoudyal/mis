﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class faculty_change_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Faculty_ApplicationUser_CreatedBy",
                table: "Faculty");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Faculty",
                table: "Faculty");

            migrationBuilder.RenameTable(
                name: "Faculty",
                newName: "FacultyDetails");

            migrationBuilder.RenameIndex(
                name: "IX_Faculty_CreatedBy",
                table: "FacultyDetails",
                newName: "IX_FacultyDetails_CreatedBy");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FacultyDetails",
                table: "FacultyDetails",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_FacultyDetails_ApplicationUser_CreatedBy",
                table: "FacultyDetails",
                column: "CreatedBy",
                principalTable: "ApplicationUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FacultyDetails_ApplicationUser_CreatedBy",
                table: "FacultyDetails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FacultyDetails",
                table: "FacultyDetails");

            migrationBuilder.RenameTable(
                name: "FacultyDetails",
                newName: "Faculty");

            migrationBuilder.RenameIndex(
                name: "IX_FacultyDetails_CreatedBy",
                table: "Faculty",
                newName: "IX_Faculty_CreatedBy");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Faculty",
                table: "Faculty",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Faculty_ApplicationUser_CreatedBy",
                table: "Faculty",
                column: "CreatedBy",
                principalTable: "ApplicationUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
