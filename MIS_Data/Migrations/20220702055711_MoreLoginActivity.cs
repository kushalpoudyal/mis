﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MIS_Data.Migrations
{
    public partial class MoreLoginActivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LoginIP",
                table: "LoginActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LoginLatitude",
                table: "LoginActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LoginLongitude",
                table: "LoginActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserAgent",
                table: "LoginActivity",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "BookDetails",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(200)",
                oldMaxLength: 200,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LoginIP",
                table: "LoginActivity");

            migrationBuilder.DropColumn(
                name: "LoginLatitude",
                table: "LoginActivity");

            migrationBuilder.DropColumn(
                name: "LoginLongitude",
                table: "LoginActivity");

            migrationBuilder.DropColumn(
                name: "UserAgent",
                table: "LoginActivity");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "BookDetails",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);
        }
    }
}
