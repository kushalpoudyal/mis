﻿using MIS_Entity.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository.DTO
{
    public class RoleDetailsDTO : BaseEntity
    {

        public Guid RoleId { get; set; }

        public string ModuleName { get; set; }
        public bool IsView { get; set; }
        public bool IsCreate { get; set; }
        public bool IsEdit { get; set; }
        public bool IsDelete { get; set; }
        public bool IsAuthorize { get; set; }
        public bool IsDownload { get; set; }



    }
}
