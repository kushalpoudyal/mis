﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository.DTO
{
    public class AdminDashboardDTO
    {
        public int TotalStudents { get; set; }

        public int TotalStaffs { get; set; }

        public int TotalBooks { get; set; }

        public int TotalSubjects { get; set; }

        public int TotalCourse { get; set; }

        public int TotalBatches { get; set; }

        public int TotalDepartment { get; set; }
    }
}
