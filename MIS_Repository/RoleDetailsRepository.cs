﻿using MIS.DBContext;
using MIS.Interfaces;
using MIS_Entity.Entity.Identity;
using MIS_Repository.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MIS_Repository
{
    public class RoleDetailsRepository : Repository<RoleDetails>, IRoleDetailsRepository
    {
        private readonly ISMTDbContext _context;
        public RoleDetailsRepository(ISMTDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<RoleDetailsDTO>> GetUserRoleDetails(Guid UserId)
        {
            try
            {

                var roledetails = await (from usr in _context.Users
                                         join rol in _context.UserRoles on usr.Id equals rol.UserId
                                         join roldtl in _context.RoleDetails on rol.RoleId equals roldtl.RoleId
                                         where usr.Id == UserId
                                         select new RoleDetailsDTO
                                         {
                                             RoleId = roldtl.RoleId,
                                             ModuleName = roldtl.ModuleName,
                                             IsView = roldtl.IsView,
                                             IsCreate = roldtl.IsCreate,
                                             IsEdit = roldtl.IsEdit,
                                             IsDelete = roldtl.IsDelete,
                                             IsAuthorize = roldtl.IsAuthorize,
                                             IsDownload = roldtl.IsDownload,
                                         }).ToListAsync();


                return roledetails.ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

    }
    public interface IRoleDetailsRepository : IRepository<RoleDetails>
    {
        Task<List<RoleDetailsDTO>> GetUserRoleDetails(Guid UserId);
    }
}
