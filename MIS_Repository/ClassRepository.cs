﻿using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS_Entity.Entity.Classes;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MIS_Repository
{
    public class ClassRepository : Repository<ClassDetails>, IClassRepository
    {
        private readonly ISMTDbContext _entities;
        public ClassRepository(ISMTDbContext context) : base(context)
        {
            _entities = context;
        }

        public IQueryable<ClassDetails> GetCourseBatchName()
        {
            var classDetails = _entities.ClassDetails.Include(e => e.CourseDetails)
                .Include(e => e.BatchDetails);
            return classDetails;
        }
        public IQueryable<ClassDetails> GetCourseByStudent()
        {
            var classDetails = _entities.ClassDetails.Include(e => e.CourseDetails)
                .Include(e => e.BatchDetails).Include(e => e.CourseSubject);
            return classDetails;
        }
    }
}
