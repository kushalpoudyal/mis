﻿using MIS.DBContext;
using MIS_Entity.Entity;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository
{
    public class BookGenreRepository : Repository<BookGenre>, IBookGenreRepository
    {
        private readonly ISMTDbContext _entities;
        public BookGenreRepository(ISMTDbContext context) : base(context)
        {
            _entities = context;
        }
    }
}
