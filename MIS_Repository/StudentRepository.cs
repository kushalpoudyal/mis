﻿using MIS.DBContext;
using MIS_Entity.Entity;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository
{
    public class StudentRepository : Repository<StudentDetails>, IStudentRepository
    {
        private readonly ISMTDbContext _entities;
        public StudentRepository(ISMTDbContext context) : base(context)
        {
            _entities = context;
        }
        

    }
}
