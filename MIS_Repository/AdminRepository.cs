﻿using MIS.DBContext;
using MIS_Entity.Entity;
using MIS_Entity.Entity.Classes;
using MIS_Repository.DTO;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIS_Repository
{
    public class AdminRepository : Repository<StudentDetails>, IAdminRepository
    {
        private readonly ISMTDbContext _entities;
        private readonly IUnitOfWork unitOfWork;
        public AdminRepository(ISMTDbContext context, IUnitOfWork unitOfWork) : base(context)
        {
            _entities = context;
            this.unitOfWork = unitOfWork;
        }

        public AdminDashboardDTO GetAdminDashboardInfo()
        {
            var books = unitOfWork.LibraryRepository.GetAll();
            var students = unitOfWork.StudentRepository.GetAll();
            var staffs = unitOfWork.FacultyRepository.GetAll();
            var subjects = unitOfWork.CourseSubjectRepository.GetAll();
            var courses = unitOfWork.CourseRepository.GetAll();
            var batch = unitOfWork.BatchRepository.GetAll();
            var department = unitOfWork.DepartmentRepository.GetAll();

            var model = new AdminDashboardDTO()
            {
                TotalStudents = students.Where(x => x.RecordStatus == MIS_Entity.Entity.Enum.RecordStatus.Active).Count(),
                TotalBooks = books.Count(),
                TotalStaffs = staffs.Where(x => x.RecordStatus == MIS_Entity.Entity.Enum.RecordStatus.Active).Count(),
                TotalSubjects = subjects.Where(x => x.RecordStatus == MIS_Entity.Entity.Enum.RecordStatus.Active).Count(),
                TotalBatches = batch.Where(x => x.RecordStatus == MIS_Entity.Entity.Enum.RecordStatus.Active).Count(),
                TotalCourse = courses.Where(x => x.RecordStatus == MIS_Entity.Entity.Enum.RecordStatus.Active).Count(),
                TotalDepartment = department.Where(x => x.RecordStatus == MIS_Entity.Entity.Enum.RecordStatus.Active).Count()
            };

            return model;
        }
    }
}
