﻿using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS_Entity.Entity;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIS_Repository
{
    public class AssignmentRepository : Repository<AssignmentMaterial>, IAssignmentRepository
    {
        private readonly ISMTDbContext _context;
        public AssignmentRepository(ISMTDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<AssignmentMaterial> GetAssignmentBySubject(Guid SubjectId)
        {
            return await _context.AssignmentMaterial.Where(x => x.SubjectId == SubjectId).Include(x => x.CourseSubject).FirstOrDefaultAsync();

        }
    }
}
