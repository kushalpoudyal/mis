﻿using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MIS_Repository
{
    public class Repository<T> : IRepository<T> where T: class
    {
        private readonly DbSet<T> _entities;

        public Repository(ISMTDbContext context)
        {
            _entities = context.Set<T>();
        }

        public void Add(T Entity)
        {
            _entities.Add(Entity);
        }

        public void AddRange(IEnumerable<T> entities)
        {
            _entities.AddRange(entities);
        }

        public void Update(T Entity)
        {
            _entities.Update(Entity);
        }

        public void UpdateRange(IEnumerable<T> entities)
        {
            _entities.UpdateRange(entities);
        }

        public T Get(Guid? id)
        {
            return _entities.Find(id);
        }

        public IQueryable<T> GetAll()
        {
            return _entities.AsQueryable();
        }

        public void Remove(T Entity)
        {
            _entities.Remove(Entity);
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            _entities.RemoveRange(entities);
        }
       
    }
}
