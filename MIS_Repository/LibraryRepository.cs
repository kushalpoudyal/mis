﻿using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS_Entity.Entity;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIS_Repository
{
    public class LibraryRepository : Repository<BookDetails>, ILibraryRepository
    {
        private readonly ISMTDbContext _entities;
        public LibraryRepository(ISMTDbContext context) : base(context)
        {
            _entities = context;
        }
        public IQueryable<BookDetails> GetPhysicalBooksByCourseId(Guid CourseId)
        {
            return _entities.BookDetails.Where(x => x.BookTypes == MIS_Entity.Entity.Enum.BookType.PhysicalBook && x.CourseId == CourseId);
        }
        public IQueryable<BookDetails> GetNonPhysicalBooksByCourseId(Guid CourseId)
        {
            return _entities.BookDetails.Where(x => x.BookTypes != MIS_Entity.Entity.Enum.BookType.PhysicalBook && x.CourseId == CourseId);
        }
        public async Task<List<BookDetails>> GetbyKeyUp(string str)
        {
            return await _entities.BookDetails.Where(x => x.Name.Contains(str)).ToListAsync();
        }
        public async Task<List<BookDetails>> GetbyGenre(string Genre)
        {
            return await _entities.BookDetails.Where(x => x.Genre == Genre).ToListAsync();
        }
    }
}
