﻿using MIS.DBContext;
using MIS_Entity.Entity.FacultyDetails;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository.Interfaces
{
    public class FacultyRepository : Repository<FacultyDetails>, IFacultyRepository
    {
        private readonly ISMTDbContext _entities;
        public FacultyRepository(ISMTDbContext context) : base(context)
        {
            _entities = context;
        }
    }
}
