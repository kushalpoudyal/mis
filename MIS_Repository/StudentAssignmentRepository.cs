﻿using MIS.DBContext;
using MIS_Entity.Entity.Student;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository
{
    public class StudentAssignmentRepository : Repository<AssignmentByStudent>, IStudentAssignmentRepository
    {
        private readonly ISMTDbContext _context;
        public StudentAssignmentRepository(ISMTDbContext context) : base(context)
        {
            _context = context;
        }
        
    }
}
