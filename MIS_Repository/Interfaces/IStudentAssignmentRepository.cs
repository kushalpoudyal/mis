﻿using MIS.Interfaces;
using MIS_Entity.Entity.Student;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository.Interfaces
{
    public interface IStudentAssignmentRepository : IRepository<AssignmentByStudent>
    {
    }
}
