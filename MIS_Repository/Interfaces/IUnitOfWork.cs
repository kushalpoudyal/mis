﻿using MIS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MIS_Repository.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        public IStudentRepository StudentRepository { get; }

        public IBookGenreRepository BookGenreRepository { get; }

        public IBatchRepository BatchRepository { get; }
        public ILibraryRepository LibraryRepository { get; }

        public IClassRepository ClassRepository { get; }

        public ICourseRepository CourseRepository { get; }

        public ICourseSubjectRepository CourseSubjectRepository { get; }

        public IDepartmentRepository DepartmentRepository { get; }

        public IFacultyRepository FacultyRepository { get; }

        Task<int> SaveAsync();


    }
}
