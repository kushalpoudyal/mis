﻿using MIS.Interfaces;
using MIS_Entity.Entity.FacultyDetails;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository.Interfaces
{
    public interface IFacultyRepository : IRepository<FacultyDetails>
    {
    }
}
