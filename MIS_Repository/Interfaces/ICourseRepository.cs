﻿using MIS.Interfaces;
using MIS_Entity.Entity.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository.Interfaces
{
    public interface ICourseRepository : IRepository<CourseDetails>
    {
    }
}
