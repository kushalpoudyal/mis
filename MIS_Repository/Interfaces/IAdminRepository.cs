﻿using MIS.Interfaces;
using MIS_Entity.Entity;
using MIS_Repository.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository.Interfaces
{
    public interface IAdminRepository : IRepository<StudentDetails>
    {
        AdminDashboardDTO GetAdminDashboardInfo();
    }
}
