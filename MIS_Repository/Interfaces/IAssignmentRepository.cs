﻿using MIS.Interfaces;
using MIS_Entity.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MIS_Repository.Interfaces
{
    public interface IAssignmentRepository : IRepository<AssignmentMaterial>
    {
        Task<AssignmentMaterial> GetAssignmentBySubject(Guid SubjectId);
    }
}
