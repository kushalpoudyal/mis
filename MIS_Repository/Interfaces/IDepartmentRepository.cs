﻿using MIS.Interfaces;
using MIS_Entity.Entity.Department;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository.Interfaces
{
    public interface IDepartmentRepository : IRepository<Department>
    {

    }
}
