﻿using MIS.Interfaces;
using MIS_Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIS_Repository.Interfaces
{
    public interface ILibraryRepository : IRepository<BookDetails>
    {
        IQueryable<BookDetails> GetNonPhysicalBooksByCourseId(Guid CourseId);
        IQueryable<BookDetails> GetPhysicalBooksByCourseId(Guid CourseId);
        Task<List<BookDetails>> GetbyKeyUp(string str);
        Task<List<BookDetails>> GetbyGenre(string Genre);
    }
}
