﻿using MIS.Interfaces;
using MIS_Entity.Entity.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MIS_Repository.Interfaces
{
    public interface IClassRepository: IRepository<ClassDetails>
    {
        //IEnumerable<ClassDetails> GetCourseName();

        IQueryable<ClassDetails> GetCourseBatchName();

        IQueryable<ClassDetails> GetCourseByStudent();
    }
}
