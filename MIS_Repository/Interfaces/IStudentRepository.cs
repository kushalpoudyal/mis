﻿using MIS.Interfaces;
using MIS_Entity.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository.Interfaces
{
    public interface IStudentRepository : IRepository<StudentDetails>
    {

    }
}
