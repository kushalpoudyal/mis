﻿using MIS.Interfaces;
using MIS_Entity.Entity.Batches;
using MIS_Entity.Entity.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MIS_Repository.Interfaces
{
    public interface IBatchRepository : IRepository<BatchDetails>
    {
       
    }
}
