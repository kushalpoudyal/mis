﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIS.Interfaces
{
    public interface IRepository<T> where T: class
    {
        T Get(Guid? id);

        IQueryable<T> GetAll();

        void Add(T Entity);

        void AddRange(IEnumerable<T> entities);

        void Update(T Entity);

        void UpdateRange(IEnumerable<T> entities);

        void Remove(T Entity);

        void RemoveRange(IEnumerable<T> entities);
                
    }
}
