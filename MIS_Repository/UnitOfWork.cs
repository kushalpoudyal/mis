﻿using MIS.DBContext;
using MIS.Interfaces;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIS_Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ISMTDbContext _entities;
        public UnitOfWork(ISMTDbContext context)
        {
            _entities = context;
            StudentRepository = new StudentRepository(_entities);
            BookGenreRepository = new BookGenreRepository(_entities);
            BatchRepository = new BatchRepository(_entities);
            LibraryRepository = new LibraryRepository(_entities);
            ClassRepository = new ClassRepository(_entities);
            CourseRepository = new CourseRepository(_entities);
            CourseSubjectRepository = new CourseSubjectRepository(_entities);
            DepartmentRepository = new DeparmentRepository(_entities);
            FacultyRepository = new FacultyRepository(_entities); 
        }
        public IStudentRepository StudentRepository { get; private set; }

        public IBookGenreRepository BookGenreRepository { get; private set; }

        public IBatchRepository BatchRepository { get; private set; }
        public ILibraryRepository LibraryRepository { get; private set; }

        public IClassRepository ClassRepository { get; private set; }

        public ICourseRepository CourseRepository { get; private set; }

        public ICourseSubjectRepository CourseSubjectRepository { get; private set; }

        public IDepartmentRepository DepartmentRepository { get; private set; }

        public IFacultyRepository FacultyRepository { get; private set; } 

        public void Dispose()
        {
            _entities.Dispose();
        }

        public async Task<int> SaveAsync()
        {
            return await _entities.CommitAsync();
        }

    }
}
