﻿using Microsoft.EntityFrameworkCore;
using MIS.DBContext;
using MIS_Entity.Entity.Batches;
using MIS_Entity.Entity.Classes;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MIS_Repository
{
    public class BatchRepository : Repository<BatchDetails>, IBatchRepository
    {
        private readonly ISMTDbContext _entities;
        public BatchRepository(ISMTDbContext context) : base(context)
        {
            _entities = context;
        }

        
    }
}
