﻿using MIS.DBContext;
using MIS_Entity.Entity.Course;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository
{
    public class CourseSubjectRepository : Repository<CourseSubject>, ICourseSubjectRepository
    {
        public CourseSubjectRepository(ISMTDbContext context) : base(context)
        {
        }
    }
}
