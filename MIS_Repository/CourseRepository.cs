﻿using MIS.DBContext;
using MIS_Entity.Entity.Course;
using MIS_Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Repository
{
    public class CourseRepository : Repository<CourseDetails>, ICourseRepository
    {
        private readonly ISMTDbContext _entities;
        public CourseRepository(ISMTDbContext context) : base(context)
        {
            _entities = context;
        }
    }
}
