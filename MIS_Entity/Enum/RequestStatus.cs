﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.Enum
{
    public enum RequestStatus
    {
        Pending=1,
        Return,
        Rejected,
        Approved
    }
}
