﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Enum
{
    public enum WeekDay
    {
        [Display(Name = "Sunday")]
        Sunday = 1,

        [Display(Name = "Monday")]
        Monday,

        [Display(Name = "Tuesday")]
        Tuesday,

        [Display(Name = "Wednesday")]
        Wednesday,

        [Display(Name = "Thursday")]
        Thursday,

        [Display(Name = "Friday")]
        Friday


    }
}
