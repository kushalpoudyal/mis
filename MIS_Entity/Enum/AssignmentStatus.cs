﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Enum
{
    public enum AssignmentStatus
    {
        [Display(Name = "Pending")]
        Pending = 1,

        [Display(Name = "Returned")]
        Returned ,

        [Display(Name = "Rejected")]
        Rejected,

        [Display(Name = "Approved")]
        Approved
    }
}
