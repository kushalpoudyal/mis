﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Entity.Enum
{
    public enum BookType
    {
        [Display(Name = "Physical Book")]
        PhysicalBook = 1,

        [Display(Name = "E-Book")]
        Ebook ,

        [Display(Name = "Journal")]
        Journal,

        [Display(Name = "Research Paper")]
        ResearchPaper

    }
}
