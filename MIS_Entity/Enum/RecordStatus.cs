﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.Entity.Enum
{
    public enum RecordStatus
    {
        Active = 1,
        Inactive,
        Deleted,
        Unauthorized,
        Suspended
    }
}
