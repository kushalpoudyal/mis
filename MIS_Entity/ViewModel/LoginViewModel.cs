﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.ViewModel
{
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string LoginLatitude { get; set; }
        public string LoginLongitude { get; set; }
        public string UserAgent { get; set; }
        public bool RememberMe { get; set; }
    }
    public class GetLoginViewModel
    {
        public string Email { get; set; }
        public string LoginLatitude { get; set; }
        public string LoginLongitude { get; set; }
        public string UserAgent { get; set; }
        public string LoginIP { get; set; }
        public bool RememberMe { get; set; }
        public bool Success { get; set; }
        public DateTime AuditDate { get; set; }
    }
}
