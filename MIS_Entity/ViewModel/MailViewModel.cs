﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.ViewModel
{
    public class MailViewModel
    {
        public string ToEmail { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Host { get; set; }
    }
}
