﻿using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity
{
    public class Recommendations : BaseEntity
    {
        [StringLength(50)]
        public string Title { get; set; }

        public string Description { get; set; }

        public Guid StudentId { get; set; }
        [ForeignKey("StudentId")]

        public virtual StudentDetails StudentDetails { get; set; }
    }
}
