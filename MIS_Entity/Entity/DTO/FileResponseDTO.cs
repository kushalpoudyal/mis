﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIS_Entity.DTO
{
    public class FileResponseDTO
    {
        public Guid id { get; set; }

        public string mediaType { get; set; }

        public string locationKey { get; set; }

        public string mediaExt { get; set; }
        public long mediaSize { get; set; }

        public string Message { get; set; }

        public bool success { get; set; }
        public string fileName { get; set; }


    }
}
