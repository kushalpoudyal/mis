﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIS_Entity.DTO
{
    public class FileUploadRequestDTO
    {
        public Guid id { get; set; }
        public string BucketName { get; set; }

        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string ObjectKey { get; set; }

        public string FilePath { get; set; }

        public long FileLength { get; set; }

        public string Endpoint { get; set; }

        public Stream FileStream { get; set; }
    }
}
