﻿using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.Entity.Documents
{
    public class UserDocuments : BaseEntity
    {
        public Guid UserId { get; set; }

        public DocumentType DocumentType { get; set; }

        public string DocLocation { get; set; }

        public string DocExtension { get; set; }

        public long DocumentSize { get; set; }
    }
}
