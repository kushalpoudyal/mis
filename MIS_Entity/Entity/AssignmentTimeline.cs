﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using MIS_Entity.Entity.Identity;
using MIS_Entity.Entity.Student;
using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity
{
    public class AssignmentTimeline
    {
        public AssignmentTimeline()
        {
            this.GivenOn = DateTime.UtcNow;
        }
        [Key]
        public long Id { get; set; }
        public Guid AssignmentByStudentId { get; set; }

        public string Remarks { get; set; }

        public AssignmentStatus AssignmentStatus { get; set; }

        public DateTime GivenOn { get; set; }

        public Guid GivenById { get; set; }

        [ForeignKey("AssignmentByStudentId")]
        public virtual AssignmentByStudent AssignmentByStudent { get; set; }
        [ForeignKey("GivenById")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
