﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Entity
{
    public class BookGenre 
    {
        public Guid ID { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(300)]
        public string Description { get; set; }
    }
}
