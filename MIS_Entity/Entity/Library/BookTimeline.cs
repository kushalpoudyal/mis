﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity
{
    public class BookTimeline
    {

        public Guid BookId { get; set; }

        public Guid UserId { get; set; }

        public bool CurrentStatus { get; set; }

        public DateTime IssuedDate { get; set; }

        public DateTime ReturnDate { get; set; }
        [ForeignKey("UserId")]
        public virtual StudentDetails StudentDetails { get; set; }
        [ForeignKey("BookId")]
        public virtual BookDetails BookDetails { get; set; }
    }
}
