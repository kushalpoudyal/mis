﻿using Microsoft.AspNetCore.Http;
using MIS_Entity.Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity
{
    public class BookDetails : BaseEntity
    {


        [StringLength(200)]
        [Required]
        public string Name { get; set; }

        [StringLength(200)]
        public string Author { get; set; }

        [StringLength(100)]
        public string Genre { get; set; }

        public BookType BookTypes { get; set; }

        public bool IsDownloadable { get; set; }

        public int BookCount { get; set; }

        public Guid? CourseId { get; set; }

        public bool AvailabilityStatus { get; set; }

        public string ReleasedDate { get; set; }

        [StringLength(400)]
        public string ImageLocation { get; set; }
        [StringLength(400)]
        public string BookLocation { get; set; }

        [NotMapped]
        public IFormFile ImageDetails { get; set; }

        [NotMapped]
        public IFormFile BookFile { get; set; }






    }
}
