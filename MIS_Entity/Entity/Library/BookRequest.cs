﻿using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity.Library
{
    public class BookRequest
    {
        public Guid BookId { get; set; }

        public Guid UserId { get; set; }

        public DateTime RequestedFor { get; set; }

        public DateTime RequestedTil { get; set; }

        public string AdditionalRemarks { get; set; }

        public RequestStatus RequestStatus { get; set; }

        public DateTime RequestedOn { get; set; }

        public DateTime? ActionTakenOn { get; set; }

        public Guid? ActionTakenBy { get; set; }

        public string ActionRemarks { get; set; }

        [ForeignKey("UserId")]
        public virtual StudentDetails StudentDetails { get; set; }
        [ForeignKey("BookId")]
        public virtual BookDetails BookDetails { get; set; }
    }
}
