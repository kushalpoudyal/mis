﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.Entity.Library
{
    public class BookActivity
    {
        public BookActivity()
        {
            this.AuditDate = DateTime.Now;
        }
        public long Id { get; set; }

        public Guid UserId { get; set; }

        public string Username { get; set; }


        public Guid BookId { get; set; }
        public string BookName { get; set; }

        public DateTime AuditDate { get; set; }
    }
}
