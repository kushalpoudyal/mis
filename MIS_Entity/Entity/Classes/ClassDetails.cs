﻿using MIS_Entity.Entity.Batches;
using MIS_Entity.Entity.Course;
using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity.Classes
{
    public class ClassDetails : BaseEntity
    {
        public Guid CourseID { get; set; }

        public Guid BatchID { get; set; }

        public Guid SubjectID { get; set; }

        public int BatchYear { get; set; }

        public WeekDay WeekDays { get; set; }

        public DateTime ClassStartTime { get; set; }

        public DateTime ClassEndTime { get; set; }
        [StringLength(200)]
        public string MeetingLink { get; set; }

        [ForeignKey("CourseID")]
        public virtual CourseDetails CourseDetails { get; set; }
        [ForeignKey("SubjectID")]
        public virtual CourseSubject CourseSubject { get; set; }

        [ForeignKey("BatchID")]
        public virtual BatchDetails BatchDetails { get; set; }
    }
}
