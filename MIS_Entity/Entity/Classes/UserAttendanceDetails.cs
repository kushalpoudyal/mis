﻿using MIS_Entity.Entity.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity.Classes
{
    public class UserAttendanceDetails
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public Guid UserID { get; set; }

        public DateTime CheckIn { get; set; }

        public DateTime? CheckOut { get; set; }

        public Guid? ClassId { get; set; }

        [StringLength(1000)]
        public string Remarks { get; set; }
        [Required]
        public Guid CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        [ForeignKey("UserID")]
        public ApplicationUser EmployeeInformation { get; set; }
        [ForeignKey("CreatedById")]
        public ApplicationUser ApplicationUser { get; set; }
    }
}
