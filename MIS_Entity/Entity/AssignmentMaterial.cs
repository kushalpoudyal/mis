﻿using Microsoft.AspNetCore.Http;
using MIS_Entity.Entity.Batches;
using MIS_Entity.Entity.Course;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity
{
    public class AssignmentMaterial : BaseEntity
    {

        public Guid SubjectId { get; set; }

        public Guid UploadedById { get; set; }

        public string AssignmentName { get; set; }

        public Guid BatchId { get; set; }


        public string AssignmentDetail { get; set; }

        public DateTime DeadlineDate { get; set; }

        public string FileLocation { get; set; }

        public string FileFormat { get; set; }

        [NotMapped]
        public IFormFile AssignmentFile { get; set; }

        [ForeignKey("SubjectId")]
        public virtual CourseSubject CourseSubject { get; set; }
        [ForeignKey("UploadedById")]
        public virtual ApplicationUser ApplicationUsers { get; set; }

        [ForeignKey("BatchId")]
        public virtual BatchDetails BatchDetails { get; set; }
    }
}
