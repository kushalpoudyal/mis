﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Entity.Identity
{
    public class RoleDetails : BaseEntity
    { 
        [Required]
        public Guid RoleId { get; set; }
        [Required]
        public string ModuleName { get; set; }
        public bool IsView { get; set; }
        public bool IsCreate { get; set; }
        public bool IsEdit { get; set; }
        public bool IsDelete { get; set; }
        public bool IsAuthorize { get; set; }
        public bool IsDownload { get; set; }

        public ApplicationRole Role { get; set; }



    }
}
