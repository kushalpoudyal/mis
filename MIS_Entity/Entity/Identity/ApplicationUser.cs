﻿using Microsoft.AspNetCore.Identity;
using MIS_Entity.Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MIS_Entity.Entity.Identity
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public ApplicationUser()
        {
            this.StudentDetails = new HashSet<StudentDetails>();
        }
        [StringLength(350)]
        public string Fullname { get; set; }
        [StringLength(150)]
        public string Contact { get; set; }
        [StringLength(150)]
        public string Country { get; set; }

        [StringLength(150)]
        public string Organization { get; set; }

        [StringLength(150)]
        public string WorkTitle { get; set; }
        public bool IsPasswordUpdated { get; set; }
        public bool Status{ get; set; }

        public Guid CreatedBy { get; set; }

        public Guid ModifiedBy { get; set; }


        public DateTime CreatedDate { get; set; }

        public int ModifiedTimes { get; set; }

        public DateTime ModifiedDate { get; set; }

        public RecordStatus RecordStatus { get; set; }
        public bool Deleted{ get; set; }
        //public UserTypes UserType{ get; set; }
        [InverseProperty("ApplicationUser2")]
        public  ICollection<StudentDetails> StudentDetails { get; set; }


    }
    //public enum UserTypes
    //{
    //    SuperAdmin = 1,
    //    Admin=2,
    //    Student = 3
    //}
}
