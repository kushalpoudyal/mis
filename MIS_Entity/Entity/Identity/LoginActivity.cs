﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.Entity.Identity
{
    public class LoginActivity
    {
        public LoginActivity()
        {
            this.AuditDate = DateTime.Now;
        }
        public long Id { get; set; }
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string LoginType { get; set; }
        public bool Success { get; set; }
        public string LoginLatitude { get; set; }
        public string LoginLongitude { get; set; }
        public string UserAgent { get; set; }
        public string LoginIP { get; set; }
        public string Remarks { get; set; }
        public DateTime AuditDate { get; set; }
    }
}
