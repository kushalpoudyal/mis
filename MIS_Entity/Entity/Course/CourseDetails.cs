﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Entity.Course
{
    public class CourseDetails : BaseEntity
    {
        [StringLength(50)]
        public string CourseCode { get; set; }
        [StringLength(50)]
        public string CourseName { get; set; }
        [StringLength(150)]
        public string CourseDescription { get; set; }

        public int CreditHours { get; set; }


    }
}
