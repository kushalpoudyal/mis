﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Entity.Course
{
    public class CourseSubject : BaseEntity
    {
        [StringLength(50)]
        public string SubjectCode { get; set; }
        [StringLength(50)]
        public string SubjectName { get; set; }
        [StringLength(150)]
        public string SubjectDescription { get; set; }

        public int CreditHours { get; set; }

    }
}
