﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.Entity.Course
{
    public class SubjectByCourse
    {
        public Guid SubjectId { get; set; }

        public Guid CourseId { get; set; }

        public Guid FacultyId { get; set; }
    }
}
