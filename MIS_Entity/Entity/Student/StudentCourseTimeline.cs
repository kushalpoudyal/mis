﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;
using System.Text;

namespace MIS_Entity.Entity.Student
{
    public class StudentCourseTimeline
    {
        [Key]
        public long Id { get; set; }

        public Guid StudentId { get; set; }

        public Guid CourseId { get; set; }

        public DateTime FromDate { get; set; }

        public bool WasEscalated { get; set; }

        public Guid GivenById { get; set; }

        public DateTime GivenOnDate { get; set; }
    }
}
