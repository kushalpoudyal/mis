﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Entity.Student
{
    public class StudentPayment
    {
        [Key]
        public long Id { get; set; }
        public Guid StudentId { get; set; }

        public decimal TotalCourseAmount { get; set; }

        public bool HasPaid { get; set; }

        public DateTime PaymentDueDate { get; set; }

        public DateTime RecordedDate { get; set; }

        public Guid GivenById { get; set; }

        public DateTime GivenOn { get; set; }
    }
}
