﻿using Microsoft.AspNetCore.Http;
using MIS_Entity.Entity.Batches;
using MIS_Entity.Entity.Course;
using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Text;

namespace MIS_Entity.Entity
{
    public class StudentDetails : BaseEntity
    {
        //[Required]
        public Guid UserId { get; set; }
        [Display(Name = "Batch")]
        [Required]
        public Guid BatchId { get; set; }
        [Display(Name = "Course")]

        [Required]
        public Guid CourseId { get; set; }
        [Display(Name = "Name")]

        [StringLength(50)]
        [Required]
        public string StudentName { get; set; }
        [Display(Name = "DOB")]

        public DateTime DOB { get; set; }
        [StringLength(1000)]
        public string ProfileImgLocation { get; set; }
        [Display(Name = "Blood Group")]

        public BloodGroups BloodGroup { get; set; }
        [StringLength(20)]
        [Display(Name = "Religion")]

        public string Religion { get; set; }
        [Display(Name = "Mobile Number")]

        [StringLength(20)]
        public string MobileNumber { get; set; }
        [Display(Name = "Alternate Number")]
        [StringLength(20)]
        public string AlternateNumber { get; set; }
        [Display(Name = "GrandFather Name")]

        [StringLength(50)]
        public string Intake { get; set; }
        [Display(Name = "Father Name")]

        [StringLength(50)]
        public string Fathername { get; set; }
        [Display(Name = "Mother Name")]

        [StringLength(50)]
        public string Mothername { get; set; }
        [Display(Name = "Email")]

        [StringLength(50)]
        public string GuardianEmail { get; set; }
        [Display(Name = "GuardianEmail")]

        [StringLength(50)]
        public string GuardianContact { get; set; }
        [Display(Name = "GuardianContact")]


        [StringLength(50)]
        public string StudentEmail { get; set; }
        [Display(Name = "Nationality")]

        [StringLength(50)]
        public string Nationality { get; set; }
        [Display(Name = "Permanent Country")]

        [StringLength(50)]

        public string PermanentCountry { get; set; }
        [Display(Name = "Permanent State")]

        [StringLength(50)]
        public string PermanentState { get; set; }
        [Display(Name = "Permanent City")]

        [StringLength(50)]
        public string PermanentCity { get; set; }
        [Display(Name = "Permanent Locality")]

        [StringLength(50)]
        public string PermanentLocality { get; set; }
        [Display(Name = "Temporary Country")]

        [StringLength(50)]
        public string TemporaryCountry { get; set; }
        [Display(Name = "Temporary State")]

        [StringLength(50)]
        public string TemporaryState { get; set; }
        [Display(Name = "Temporary City")]

        [StringLength(50)]
        public string TemporaryCity { get; set; }
        [Display(Name = "Temporary Locality")]

        [StringLength(50)]
        public string TemporaryLocality { get; set; }
        [StringLength(50)]
        public string StudentID { get; set; }
        [StringLength(50)]
        public string RegistrationNo { get; set; }
        [StringLength(50)]
        public string Identification { get; set; }
        [Display(Name = "Id Number")]

        [StringLength(50)]
        public string Id_Number { get; set; }
        [Display(Name = "Id Issued Date")]

        public DateTime Id_Issued_Date { get; set; }
        [Display(Name = "Id Issued Place")]

        [StringLength(50)]
        public string Id_Issued_Place { get; set; }
        [Display(Name = "Id Issued Country")]

        [StringLength(50)]
        public string Id_Issued_Country { get; set; }

        public bool IsSuspended { get; set; }
        [Display(Name = "Profile Picture")]
        [NotMapped]

        public IFormFile ProfilePic { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser2 { get; set; }
        [ForeignKey("BatchId")]
        public BatchDetails BatchDetails { get; set; }
        [ForeignKey("CourseId")]
        public CourseDetails CourseDetails { get; set; }
    }
}
