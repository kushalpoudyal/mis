﻿using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Text;

namespace MIS_Entity.Entity.Student
{
    public class StudentSuspension
    {
        public int Id { get; set; }

        public Guid StudentId { get; set; }

        public Guid GivenById { get; set; }

        public string Remarks { get; set; }
        public DateTime GivenOnDate { get; set; }

        public DateTime? TillDate { get; set; }
    }
}
