﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.Entity.Student
{
    public class StudentObservation
    {
        public int Id { get; set; }
        public Guid StudentId { get; set; }

        public string Remarks { get; set; }

        public int Criticality { get; set; }

        public DateTime GivenOn { get; set; }

        public Guid GivenById { get; set; }


    }
}
