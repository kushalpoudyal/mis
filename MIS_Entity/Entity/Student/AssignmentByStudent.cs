﻿using Microsoft.AspNetCore.Http;
using MIS_Entity.Entity.Identity;
using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity.Student
{
    public class AssignmentByStudent : BaseEntity
    {
       
        public Guid AssignmentId { get; set; }      

        public string AssignmentContent { get; set; }

        public string Remarks { get; set; }

        public string FileLocation { get; set; }

        [NotMapped]
        public IFormFile AssignmentFile { get; set; }

        public AssignmentStatus AssignmentStatus { get; set; }

        [ForeignKey("AssignmentId")]
        public virtual AssignmentMaterial AssignmentMaterial { get; set; }
         
    }
}
