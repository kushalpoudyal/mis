﻿using DataAnnotationsExtensions;
using Microsoft.AspNetCore.Http;
using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity.FacultyDetails
{
    public class FacultyDetails : BaseEntity
    {
        [StringLength(50)]
        public string FacultyName { get; set; }
        public Guid DepartmentID { get; set; }
        public bool HeadOfDepartment { get; set; }

        public FacultyType FacultyType { get; set; }
        [Email]
        public string FacultyEmail { get; set; }
        [StringLength(150)]
        public string FacultyContact { get; set; }

        [StringLength(150)]
        public string FacultyImgLocation { get; set; }

        [NotMapped]
        public IFormFile ImageDetails { get; set; }


    }


}
