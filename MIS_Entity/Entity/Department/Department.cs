﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Entity.Department
{
    public class Department : BaseEntity
    {
        [StringLength(50)]
        public string DepartmentName { get; set; }

        [StringLength(50)]
        public string DepartmentDesc { get; set; }

        [StringLength(50)]
        public string DepartmentBlock { get; set; }


    }
}
