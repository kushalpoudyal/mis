﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MIS_Entity.Entity.Examination
{
    public class ExaminationGrading : BaseEntity
    {
        public Guid ExaminationId { get; set; }

        public Guid StudentId { get; set; }

        public string Remarks { get; set; }

        public Grade Grade { get; set; }
    }
    public enum Grade
    {
        Low,
        Medium,
        High
    }
}
