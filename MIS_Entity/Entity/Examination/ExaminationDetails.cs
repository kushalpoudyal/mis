﻿using MIS_Entity.Entity.Course;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity.Examination
{
    public class ExaminationDetails : BaseEntity
    {
        public string ExaminationSubject { get; set; }

        public Guid? SubjectId { get; set; }

        public DateTime ExaminationDate { get; set; }

        public DateTime ExaminationEndDate { get; set; }

        public string Remarks { get; set; }

        public bool IsOnline { get; set; }
        [ForeignKey("SubjectId")]

        public virtual CourseSubject CourseSubject { get; set; }
    }
}
