﻿using MIS_Entity.Entity.Enum;
using MIS_Entity.Entity.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MIS_Entity.Entity
{
    public class BaseEntity
    {

        public Guid ID { get; set; }

        public Guid CreatedBy { get; set; }

        public Guid ModifiedBy { get; set; }


        public DateTime CreatedDate { get; set; }

        public int ModifiedTimes { get; set; }

        public DateTime ModifiedDate { get; set; }
        public bool Deleted { get; set; }
        public RecordStatus RecordStatus { get; set; }
        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser ApplicationUser { get; set; }


    }
}
