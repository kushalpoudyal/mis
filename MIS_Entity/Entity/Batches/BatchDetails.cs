﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Entity.Batches
{
    public class BatchDetails : BaseEntity
    {

        [StringLength(50)]
        public string BatchName { get; set; }
        [StringLength(50)]
        public string BatchCode { get; set; }
        [StringLength(50)]
        public string BatchType { get; set; }

        public int BatchYear { get; set; }

    }
}
