﻿using MIS_Entity.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MIS_Entity.Entity
{
    public class NoticeDetails : BaseEntity
    {
        [StringLength(50)]
        public string Title { get; set; }

        public string Description { get; set; }

        public Priority Priority { get; set; }

        public DateTime ExpiryDate { get; set; }
    }
}
